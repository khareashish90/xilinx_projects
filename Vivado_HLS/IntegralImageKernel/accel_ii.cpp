#include "hls_stream.h"
#include "ap_int.h"
#include "ap_axi_sdata.h"


#include "accel_ii.hpp"


template<int ROWS, int COLS>
void integral_image_kernel_templated(
		Input_stream& _in, Output_stream& _out)
{
#pragma hls inlines

	ap_uint<12> i, j;
	Source_double_pixel readValue;
	Output_double_pixel axistream;

	Integral_simple_pixel curSumEven, curSumOdd, prev;
	Integral_simple_pixel rowBuffer[COLS];

#pragma HLS ARRAY_PARTITION variable=rowBuffer cyclic factor=2

	axistream.dest = 0;
	axistream.id = 0;
	axistream.keep = -1;
	axistream.strb = -1;

	RowLoop:
	for(i = 0; i < ROWS; i++)
	{
#pragma HLS LOOP_TRIPCOUNT min=ROWS max=ROWS

		curSumEven = 0, curSumOdd = 0;
		prev = 0;

		ColLoop:
		for(j = 0; j < COLS/2; j++)
		{
#pragma HLS LOOP_TRIPCOUNT min=COLS max=COLS
#pragma HLS LOOP_FLATTEN OFF
#pragma HLS PIPELINE

			//Load the new value
			_in >> readValue;
#ifdef VERBISE_AD
			printf("%0.4x : %0.2x %0.2x\n", (int)readValue, (int)readValue(15,8), (int)readValue(7,0));
#endif

			//prev: the accumulation of the currant row
			curSumEven = prev + readValue(7,0);
			curSumOdd  = prev + readValue(7,0) + readValue(15,8);

#ifdef VERBISE_AD
			printf("%0.8x  %0.8x %0.8x\n", (int)prev, (int)curSumEven, (int)curSumOdd);
#endif

			//rowBuffer: For adding the above value
			if(i != 0){
				curSumEven += rowBuffer[2*j];
				curSumOdd  += rowBuffer[2*j + 1];
			}

			//Bufferize the previous row
			rowBuffer[2*j] 	  = curSumEven;
			rowBuffer[2*j + 1] = curSumOdd;

			prev += readValue(7,0) + readValue(15,8);

			axistream.user = ((i==0)&&(j==0))?1:0;						//DC
			axistream.last = ((i==(ROWS-1))&&(j==(COLS/2-1)))?1:0;		//Very important for the DMA!
			axistream.data = (curSumOdd, curSumEven);					//Concat data (MSB,LSB)

#ifdef VERBISE_AD
			printf("%0.16llx : %0.8x %0.8x\n", (long int)axistream.data, (int)axistream.data(63,32), (int)axistream.data(31,0));
#endif

			//Store the new value
			_out << axistream;

		}//ColLoop

	}//rowLoop

}

void integral_image_kernel(
		Input_stream& _in, Output_stream& _out)
{
#pragma HLS INTERFACE s_axilite port=return bundle=control
#pragma HLS INTERFACE axis port=_in
#pragma HLS INTERFACE axis port=_out
	assert((WIDTH%2)==0 && "ERROR: Width must be even!");
	integral_image_kernel_templated<HEIGHT,WIDTH>(_in, _out);
}
