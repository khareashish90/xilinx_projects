#ifndef __accel_ii_hpp__
#define __accel_ii_hpp__

#include <assert.h>
#include "hls_stream.h"
#include "ap_axi_sdata.h"
#include <stdint.h>

#define VERBOSE_AD
#undef VERBOSE_AD


#ifndef VERBOSE_AD
	#define HEIGHT 		1080
	#define WIDTH  		1920
#else
	#define HEIGHT 		4
	#define WIDTH  		6
#endif

typedef ap_uint<8> 								Source_simple_pixel;
typedef ap_uint<16> 							Source_double_pixel;
typedef ap_uint<16>								Input_double_pixel;
typedef hls::stream<Input_double_pixel> 		Input_stream;

typedef ap_uint<32> 							Integral_simple_pixel;
typedef ap_uint<64> 							Integral_double_pixel;
typedef ap_axiu<64,1,1,1> 						Output_double_pixel;
typedef hls::stream<Output_double_pixel>		Output_stream;





template<int ROWS, int COLS, int NPC>
void integral_image_kernel_templated(Input_stream& _in, Output_stream& _out);

void integral_image_kernel(Input_stream& _in, Output_stream& _out);

#endif
