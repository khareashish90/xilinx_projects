#include <stdio.h>
#include <stdlib.h>

#include "accel_ii.hpp"

//TB functions specified after the main function.
void compute_ref(uint8_t* in, uint32_t* out);
int compare_ref(uint32_t* ref, uint32_t* out);

int main(void){
	Input_stream inS;
	Source_double_pixel inD;
	Output_stream outS;

	/*
	 * Vivado HLS doesn't manage the big static array. (ERROR SIGSEGV)
	 * We can use a dynamic allocation but, in our case,
	 * we only need the bellow AXI Stream array for displaying it.
	 * We just need an 1-element buffer.
	 */
#ifdef VERBOSE_AD
	Output_double_pixel outPs[HEIGHT*WIDTH/2];
#else
	Output_double_pixel outP;
#endif

	char* tabIn = (char*)malloc(HEIGHT*WIDTH*2);
	char* tabOut = (char*)malloc(HEIGHT*WIDTH*2*4);
	uint16_t* tShort = (uint16_t*)tabIn;
	uint64_t* tLong = (uint64_t*)tabOut;

	int nerr;
	uint32_t* ref = (uint32_t*)malloc(HEIGHT*WIDTH*sizeof(uint32_t));

	//Set Input Data -- Optional (we can use random data)
	for(int i=0; i<HEIGHT*WIDTH; i++)
		tabIn[i] = i;

	//Compute reference
	compute_ref((uint8_t*)tabIn, ref);

	printf("\n");
#ifdef VERBISE_AD
	printf("\nData to send\n");
	for(int i=0; i<HEIGHT*WIDTH; i++)
		printf("%0.2x%s", tabIn[i],((i+1)%(WIDTH))?" ":"\n");
#endif


#ifdef VERBISE_AD
	printf("\nInput Data in memory\n");
	for(int i=0; i<HEIGHT*WIDTH; i++)
		printf("%0.4x%s", tShort[i],((i+1)%(WIDTH/2))?"  ":"\n");
#endif

	//Send Data
	for(int i=0; i<HEIGHT*WIDTH/2; i++){
		inD = tShort[i];
		inS << inD;
	}

	printf("\nProcess...\n");
	integral_image_kernel(inS, outS);

	//Receive Data
	for(int i=0; i<HEIGHT*WIDTH/2; i++){
#ifdef VERBOSE_AD
		outS >> outPs[i];
		tLong[i] = (uint64_t)outPs[i].data;
#else
		outS >> outP;
		tLong[i] = (uint64_t)outP.data;
#endif
	}

#ifdef VERBISE_AD
	printf("\nReceived Data\n");
	for(int i=0; i<HEIGHT*WIDTH/2; i++){
		printf("%0.8x%0.8x%s", (int)outPs[i].data(63,32), (int)outPs[i].data(31,0), ((i+1)%(WIDTH/2))?"  ":"\n");
	}
#endif

#ifdef VERBISE_AD
	printf("\nData in memory\n");
	for(int i=0; i<HEIGHT*WIDTH*4; i++){
		printf("%0.2x%s%s", tabOut[i], ((i+1)%4)?"":" ", ((i+1)%(WIDTH*4))?"":"\n");
	}
#endif

	nerr = compare_ref(ref, (uint32_t*)tLong);
	printf("Nomber of errors: %d\n\n", nerr);

	return nerr;
}

void compute_ref(uint8_t* in, uint32_t* out){
	for(int i=0; i<HEIGHT*WIDTH; i++)
		out[i]  = in[i]
				+ ((i>=WIDTH)?	out[i-WIDTH]				:0)
				+ ((i%WIDTH) ?	out[i-1]					:0)
				-(((i>=WIDTH)&&(i%WIDTH))?  out[i-WIDTH-1]	:0);
}

int compare_ref(uint32_t* ref, uint32_t* out){
	int nerr = 0;
	for(int i=0; i<HEIGHT*WIDTH; i++){
		if(ref[i] != out[i])
			nerr++;
	}

	return nerr;
}
