############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project IntegralImageKernel
set_top integral_image_kernel
add_files IntegralImageKernel/accel_ii.cpp
add_files IntegralImageKernel/accel_ii.hpp
add_files -tb IntegralImageKernel/main.cpp
open_solution "solution1"
set_part {xc7z030sbg485-1} -tool vivado
create_clock -period 10 -name default
#source "./IntegralImageKernel/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog -description "Integral image computation; 2 computation per cycle;" -vendor "Trimble" -version "3.0.0" -display_name "Integral Image Double"
