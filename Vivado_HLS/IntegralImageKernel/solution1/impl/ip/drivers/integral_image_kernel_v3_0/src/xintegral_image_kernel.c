// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2016.4
// Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
// 
// ==============================================================

/***************************** Include Files *********************************/
#include "xintegral_image_kernel.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XIntegral_image_kernel_CfgInitialize(XIntegral_image_kernel *InstancePtr, XIntegral_image_kernel_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XIntegral_image_kernel_Start(XIntegral_image_kernel *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL) & 0x80;
    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u32 XIntegral_image_kernel_IsDone(XIntegral_image_kernel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XIntegral_image_kernel_IsIdle(XIntegral_image_kernel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XIntegral_image_kernel_IsReady(XIntegral_image_kernel *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XIntegral_image_kernel_EnableAutoRestart(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XIntegral_image_kernel_DisableAutoRestart(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_AP_CTRL, 0);
}

void XIntegral_image_kernel_InterruptGlobalEnable(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_GIE, 1);
}

void XIntegral_image_kernel_InterruptGlobalDisable(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_GIE, 0);
}

void XIntegral_image_kernel_InterruptEnable(XIntegral_image_kernel *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_IER);
    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_IER, Register | Mask);
}

void XIntegral_image_kernel_InterruptDisable(XIntegral_image_kernel *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_IER);
    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_IER, Register & (~Mask));
}

void XIntegral_image_kernel_InterruptClear(XIntegral_image_kernel *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XIntegral_image_kernel_WriteReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_ISR, Mask);
}

u32 XIntegral_image_kernel_InterruptGetEnabled(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_IER);
}

u32 XIntegral_image_kernel_InterruptGetStatus(XIntegral_image_kernel *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XIntegral_image_kernel_ReadReg(InstancePtr->Control_BaseAddress, XINTEGRAL_IMAGE_KERNEL_CONTROL_ADDR_ISR);
}

