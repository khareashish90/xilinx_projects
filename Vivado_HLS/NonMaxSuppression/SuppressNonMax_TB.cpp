/*
 * File:	SuppressNonMax.cpp
 * Date:	October 22, 2017
 * By:		Ashish Khare

 * Description:	Test Bench

*/



#include"SuppressNonMax_TB.hpp"
#include "TestImage.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <ap_fixed.h>
#include <string>
#include "opencv2/core/core.hpp"
#include <hls_opencv.h>

int main(void)
{

	int iImagesToTest = 1 ;

	for(int i = 0 ; i< iImagesToTest; i++)
	{
	//8 bit source image
		int_8_Pixel* SrcImage = (int_8_Pixel*)malloc(ImageRows*ImageCols*sizeof(int_8_Pixel));

		// 32 bit integral image
		int_32_Pixel* IntegralImage = (int_32_Pixel*)malloc(ImageRows*ImageCols*sizeof(int_32_Pixel));

		// Response and size
		Response_And_Size_SideChannel* Response = (Response_And_Size_SideChannel*)malloc(ImageRows*ImageCols*sizeof(Response_And_Size_SideChannel));



		hls::stream<Response_And_Size_SideChannel> ResponseStream;
		hls::stream<Keypoint_SideChannel> KeypointStream;
		std::vector<cv::KeyPoint> vKeypoints;

		int iImageIndex= i ;

		// Test image path //
//		std::string strImageFolder = "D:\\Documents\\Trimble\\Repositories\\VideoNavigation\\videonavigation\\testcase\\runloop_725_1200\\" ;
//		std::string strImageName = "GroundCamera1_";
//		std::stringstream ss;
//		ss<< i ;
//		std::string strImageIndex = ss.str();
//		std::string strImageExtension = ".jpg";
//		std::string strFullImagePath = strImageFolder+ strImageName + strImageIndex + strImageExtension ;

		std::string strFullImagePath = "D:\\Documents\\akhare\\TestImages\\wheat.jpg" ;

		cv::Mat Image ;
		Image = cv::imread(strFullImagePath, CV_LOAD_IMAGE_GRAYSCALE);


		for(int iRow = 0 ; iRow < ImageRows; iRow++)
			for(int iCol = 0 ; iCol < ImageCols; iCol++)
				SrcImage[iRow*ImageCols+iCol] = Image.at<unsigned char>(iRow, iCol);

		// Compute Integral Image
		ComputeIntegralImage(SrcImage, IntegralImage);

		// Compute Response for the integral image
		ComputeResponse(IntegralImage, Response);


		int nKPCount ;

		// Write data to the response stream
		for(int i = 0 ; i < ImageRows*ImageCols; i++)
		{
			ResponseStream.write(Response[i]);
		}


		// Get keypoints from hls function.
		SuppressNonMax(ResponseStream, KeypointStream, &nKPCount);
		printf("Image %d : Number of Keypoints = %d\n", i ,nKPCount);

		cv::KeyPoint cvKeypoint ;
		Keypoint_SideChannel val;

		for(int i = 0 ; i< nKPCount-1 ; i ++)
		{
			KeypointStream.read(val);
			cvKeypoint.pt.x = (float)val.data.x;
			cvKeypoint.pt.y = (float)val.data.y;
			cvKeypoint.size = (float)val.data.size;
			cvKeypoint.response = (float)val.data.resp;
			vKeypoints.push_back(cvKeypoint);
		}
		KeypointStream.read(val); //an extra read to get rid of the last value on stream. This is not a valid keypoint. It just indicates the end of keypoint stream.


		// Write keypoints to log file //

//		std::string strKeypointFilePath = "C:\\Users\\akhare\\Desktop\\testcase\\runloop_725_1200\\" ;
//		std::string strFileExtension = ".yml";
		//Note: we want Keypoint file name same as the input image name
//		std::string strFullFilePath = strKeypointFilePath+  strImageName + strImageIndex + strFileExtension ;


		std::string strFullFilePath = "D:\\Documents\\akhare\\TestImages\\wheat.yml";
		cv::FileStorage fs(strFullFilePath, cv::FileStorage::WRITE);
		fs <<"keypoint"<< vKeypoints ;
		fs.release();


		//Write Response  to log file //

		FILE* fResponse	= fopen("Response.log", "w");

		for(int i= 0 ; i< ImageRows*ImageCols; i++)
		{
			fprintf(fResponse, "%f, ", (float)Response[i].data.resp);

		}
		fclose(fResponse);


		free(IntegralImage);
		free(SrcImage);
		free(Response);

	}

return 0 ;
}



void ComputeIntegralImage(int_8_Pixel srcImage[ImageRows*ImageCols], int_32_Pixel integralImage[ImageRows*ImageCols])
{
			for(int i=0; i<ImageRows; i++)
			{
				for(int j=0; j<ImageCols; j++)
				{
					int k = ImageCols*i+j;
					//xil_printf("Id %d:\tSrc %d\n\r", k, (u8)refSrc[k]);
					integralImage[k] = srcImage[k];
					if(i!=0)
						integralImage[k] += integralImage[k-ImageCols];
					if(j!=0)
						integralImage[k] += integralImage[k-1];
					if((i!=0)&&(j!=0))
						integralImage[k] -= integralImage[k-ImageCols-1];
				}
			}
}


void ComputeResponse(int_32_Pixel in[ImageRows*ImageCols], Response_And_Size_SideChannel out[ImageRows*ImageCols])
{
	Index_t quadSizes[NQUADS] = QUADS_SIZES;
	Size_id_t pairs[NPAIRS][2] = PAIRS_QIDS;
	Inv_size_t invSizes[NPAIRS][2] = PAIRS_COEF;

    memset(out, 0, ImageRows*ImageCols*sizeof(Response_And_Size_SideChannel));

    int border = QUADS_MSIZE+2;
    for (int j = border; j < (ImageRows-border); ++j){
    	for (int i = border; i < (ImageCols-border); ++i){
    		int ofs = j*ImageCols + i;
    		int vals[NQUADS];
    		float bestResponse = 0;
    		int bestSize = 1;

    		for(int k=0; k<NQUADS; k++){
    			vals[k] = in[ofs+A_OFS(k)] - in[ofs+B_OFS(k)] - in[ofs+C_OFS(k)] + in[ofs+D_OFS(k)];
    		}
    		for(int k=0; k<NPAIRS; k++){
    			int inner_sum = vals[pairs[k][1]];
                int outer_sum = vals[pairs[k][0]] - inner_sum;
                float response = inner_sum*invSizes[k][1] - outer_sum*invSizes[k][0];


                if( fabs(response) > fabs(bestResponse) )
                {
                    bestResponse = response;
                    bestSize = quadSizes[pairs[k][0]];
                }
    		}

    		out[ofs].data.resp = bestResponse;
    		out[ofs].data.size = bestSize;
    		out[ofs].data.null = 0 ;
    	}
    }
    return;
}
