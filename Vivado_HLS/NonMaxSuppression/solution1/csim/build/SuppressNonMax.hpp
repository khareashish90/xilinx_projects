/*

 * File:	SuppressNonMax.hpp
 * Date:	October 22, 2017
 * By:		Ashish Khare

 * Description:

 */

#ifndef _SUPPRESS_NON_MAX_
#define _SUPPRESS_NON_MAX_

#include <assert.h>
#include <stdint.h>
#include "ap_fixed.h"
#include "ap_axi_sdata.h"
#include "hls_stream.h"

#include"hls_video.h"


#define ImageRows 1080
#define ImageCols 1920
#define Kernel_Size 5
#define Filter_Offs Kernel_Size/2
#define Max_Size 64
#define Min_Size 2

#define Response_Threshold 30

typedef ap_fixed<16,9,AP_RND_ZERO> Response_t;
typedef ap_uint<12> Index_t;



/* Response Structure 32 bits */
struct Response_And_Size
{
	Response_t resp;
	ap_uint<4> null;
	Index_t size;
};

// Keypoint Structure
struct Keypoint_t
{
	uint16_t x;
	uint16_t y;
	uint16_t resp;
	uint16_t size;
};

//Response and size with side channel information
struct Response_And_Size_SideChannel
{
	Response_And_Size	data;
    ap_uint<4> 			keep;
    ap_uint<4> 			strb;
    ap_uint<1>  		user;
    ap_uint<1>  		last;
    ap_uint<1>  		id;
    ap_uint<1>  		dest;
};


//Keypoint with side channel information
struct Keypoint_SideChannel
{
	Keypoint_t			data;
    ap_uint<4> 			keep;
    ap_uint<4> 			strb;
    ap_uint<1>  		user;
    ap_uint<1>  		last;
    ap_uint<1>  		id;
    ap_uint<1>  		dest;
};





void SuppressNonMax(hls::stream<Response_And_Size_SideChannel> &responseStream, hls::stream<Keypoint_SideChannel> &keypointStream, int* nKPCount );


void VerticalShiftResponseBuf(hls::stream<Response_And_Size_SideChannel> &responseStream,Response_And_Size ResponseBuf[Kernel_Size][ImageCols] );

void ShiftImageBuffer( Response_And_Size ResponseBuf[Kernel_Size][ImageCols], Response_And_Size  pix
						,Response_And_Size rightCol[Kernel_Size]
						, int row
						, int col
						);
//void SuppressNonMax(hls::stream<Response_And_Size> &responseStream, hls::stream<axi_32_side_channel> &keypointStream, int *nKP);
bool IsLocalExtremum(Response_And_Size Window[Kernel_Size][Kernel_Size]);

#endif
