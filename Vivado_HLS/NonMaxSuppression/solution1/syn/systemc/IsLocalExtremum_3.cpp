#include "IsLocalExtremum.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void IsLocalExtremum::thread_ap_return() {
    ap_return = (or_cond2_fu_1893_p2.read() | or_cond4_reg_3103.read());
}

void IsLocalExtremum::thread_not_tmp_17_0_3_fu_538_p2() {
    not_tmp_17_0_3_fu_538_p2 = (ap_pipeline_reg_pp0_iter6_tmp_17_0_3_reg_2233.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_1_3_fu_882_p2() {
    not_tmp_17_1_3_fu_882_p2 = (ap_pipeline_reg_pp0_iter12_tmp_17_1_3_reg_2419.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_2_3_fu_918_p2() {
    not_tmp_17_2_3_fu_918_p2 = (tmp_17_2_3_fu_862_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_2_4_fu_1358_p2() {
    not_tmp_17_2_4_fu_1358_p2 = (ap_pipeline_reg_pp0_iter18_tmp_17_2_4_reg_2655.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_3_2_fu_1873_p2() {
    not_tmp_17_3_2_fu_1873_p2 = (ap_pipeline_reg_pp0_iter25_tmp_17_3_2_reg_2763.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_3_3_fu_1378_p2() {
    not_tmp_17_3_3_fu_1378_p2 = (tmp_17_3_3_reg_2821.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_3_4_fu_1705_p2() {
    not_tmp_17_3_4_fu_1705_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_3_4_reg_2857.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_4_1_fu_1715_p2() {
    not_tmp_17_4_1_fu_1715_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_4_1_reg_2947.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_4_2_fu_1742_p2() {
    not_tmp_17_4_2_fu_1742_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_4_2_reg_2983.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_17_4_fu_1710_p2() {
    not_tmp_17_4_fu_1710_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_4_reg_2913.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_18_0_2_fu_296_p2() {
    not_tmp_18_0_2_fu_296_p2 = (!p_0_2_0_1_reg_2168.read().is_01() || !ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_0_1_reg_2168.read() != ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read());
}

void IsLocalExtremum::thread_not_tmp_18_0_3_fu_343_p2() {
    not_tmp_18_0_3_fu_343_p2 = (!p_0_2_0_2_reg_2203.read().is_01() || !ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_0_2_reg_2203.read() != ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read());
}

void IsLocalExtremum::thread_not_tmp_18_0_4_fu_402_p2() {
    not_tmp_18_0_4_fu_402_p2 = (!p_0_2_0_3_reg_2239.read().is_01() || !ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_0_3_reg_2239.read() != ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read());
}

void IsLocalExtremum::thread_not_tmp_18_1_1_fu_510_p2() {
    not_tmp_18_1_1_fu_510_p2 = (!p_0_2_1_reg_2310.read().is_01() || !ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_1_reg_2310.read() != ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read());
}

void IsLocalExtremum::thread_not_tmp_18_1_2_fu_575_p2() {
    not_tmp_18_1_2_fu_575_p2 = (!p_0_2_1_1_reg_2345.read().is_01() || !ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_1_1_reg_2345.read() != ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read());
}

void IsLocalExtremum::thread_not_tmp_18_1_3_fu_659_p2() {
    not_tmp_18_1_3_fu_659_p2 = (!p_0_2_1_2_reg_2380.read().is_01() || !ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_1_2_reg_2380.read() != ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read());
}

void IsLocalExtremum::thread_not_tmp_18_1_4_fu_718_p2() {
    not_tmp_18_1_4_fu_718_p2 = (!p_0_2_1_3_reg_2425.read().is_01() || !ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_1_3_reg_2425.read() != ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read());
}

void IsLocalExtremum::thread_not_tmp_18_1_fu_444_p2() {
    not_tmp_18_1_fu_444_p2 = (!p_0_2_0_4_reg_2275.read().is_01() || !ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_0_4_reg_2275.read() != ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read());
}

void IsLocalExtremum::thread_not_tmp_18_2_1_fu_826_p2() {
    not_tmp_18_2_1_fu_826_p2 = (!p_0_2_2_reg_2496.read().is_01() || !ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_2_reg_2496.read() != ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read());
}

void IsLocalExtremum::thread_not_tmp_18_2_2_fu_854_p2() {
    not_tmp_18_2_2_fu_854_p2 = (!p_0_2_2_1_reg_2531.read().is_01() || !ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_2_1_reg_2531.read() != ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read());
}

void IsLocalExtremum::thread_not_tmp_18_2_3_fu_949_p2() {
    not_tmp_18_2_3_fu_949_p2 = (!p_0_2_2_2_reg_2568.read().is_01() || !ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_2_2_reg_2568.read() != ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read());
}

void IsLocalExtremum::thread_not_tmp_18_2_4_fu_1056_p2() {
    not_tmp_18_2_4_fu_1056_p2 = (!p_0_2_2_3_reg_2605.read().is_01() || !ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_2_3_reg_2605.read() != ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read());
}

void IsLocalExtremum::thread_not_tmp_18_2_fu_760_p2() {
    not_tmp_18_2_fu_760_p2 = (!p_0_2_1_4_reg_2461.read().is_01() || !ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_1_4_reg_2461.read() != ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read());
}

void IsLocalExtremum::thread_not_tmp_18_3_1_fu_1164_p2() {
    not_tmp_18_3_1_fu_1164_p2 = (!p_0_2_3_reg_2698.read().is_01() || !ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_3_reg_2698.read() != ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read());
}

void IsLocalExtremum::thread_not_tmp_18_3_2_fu_1216_p2() {
    not_tmp_18_3_2_fu_1216_p2 = (!p_0_2_3_1_reg_2733.read().is_01() || !ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_3_1_reg_2733.read() != ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read());
}

void IsLocalExtremum::thread_not_tmp_18_3_3_fu_1299_p2() {
    not_tmp_18_3_3_fu_1299_p2 = (!p_0_2_3_2_reg_2770.read().is_01() || !ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_3_2_reg_2770.read() != ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read());
}

void IsLocalExtremum::thread_not_tmp_18_3_4_fu_1389_p2() {
    not_tmp_18_3_4_fu_1389_p2 = (!p_0_2_3_3_reg_2827.read().is_01() || !ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_3_3_reg_2827.read() != ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read());
}

void IsLocalExtremum::thread_not_tmp_18_3_fu_1098_p2() {
    not_tmp_18_3_fu_1098_p2 = (!p_0_2_2_4_reg_2662.read().is_01() || !ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_2_4_reg_2662.read() != ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read());
}

void IsLocalExtremum::thread_not_tmp_18_4_1_fu_1528_p2() {
    not_tmp_18_4_1_fu_1528_p2 = (!p_0_2_4_reg_2918.read().is_01() || !ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_4_reg_2918.read() != ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read());
}

void IsLocalExtremum::thread_not_tmp_18_4_2_fu_1556_p2() {
    not_tmp_18_4_2_fu_1556_p2 = (!p_0_2_4_1_reg_2953.read().is_01() || !ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_4_1_reg_2953.read() != ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read());
}

void IsLocalExtremum::thread_not_tmp_18_4_3_fu_1603_p2() {
    not_tmp_18_4_3_fu_1603_p2 = (!p_0_2_4_2_reg_2990.read().is_01() || !ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_4_2_reg_2990.read() != ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read());
}

void IsLocalExtremum::thread_not_tmp_18_4_4_fu_1654_p2() {
    not_tmp_18_4_4_fu_1654_p2 = (!p_0_2_4_3_reg_3027.read().is_01() || !ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_4_3_reg_3027.read() != ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read());
}

void IsLocalExtremum::thread_not_tmp_18_4_fu_1462_p2() {
    not_tmp_18_4_fu_1462_p2 = (!p_0_2_3_4_reg_2863.read().is_01() || !ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_3_4_reg_2863.read() != ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read());
}

void IsLocalExtremum::thread_not_tmp_19_0_3_fu_579_p2() {
    not_tmp_19_0_3_fu_579_p2 = (ap_pipeline_reg_pp0_iter6_tmp_19_0_3_reg_2246.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_1_3_fu_959_p2() {
    not_tmp_19_1_3_fu_959_p2 = (ap_pipeline_reg_pp0_iter12_tmp_19_1_3_reg_2432.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_2_3_fu_995_p2() {
    not_tmp_19_2_3_fu_995_p2 = (tmp_19_2_3_fu_872_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_2_4_fu_1393_p2() {
    not_tmp_19_2_4_fu_1393_p2 = (ap_pipeline_reg_pp0_iter18_tmp_19_2_4_reg_2669.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_3_2_fu_1805_p2() {
    not_tmp_19_3_2_fu_1805_p2 = (ap_pipeline_reg_pp0_iter24_tmp_19_3_2_reg_2777.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_3_3_fu_1413_p2() {
    not_tmp_19_3_3_fu_1413_p2 = (tmp_19_3_3_reg_2834.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_3_4_fu_1670_p2() {
    not_tmp_19_3_4_fu_1670_p2 = (ap_pipeline_reg_pp0_iter23_tmp_19_3_4_reg_2870.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_4_1_fu_1680_p2() {
    not_tmp_19_4_1_fu_1680_p2 = (ap_pipeline_reg_pp0_iter23_tmp_19_4_1_reg_2960.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_4_2_fu_1810_p2() {
    not_tmp_19_4_2_fu_1810_p2 = (ap_pipeline_reg_pp0_iter24_tmp_19_4_2_reg_2997.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_19_4_fu_1675_p2() {
    not_tmp_19_4_fu_1675_p2 = (ap_pipeline_reg_pp0_iter23_tmp_19_4_reg_2925.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_not_tmp_20_0_2_fu_300_p2() {
    not_tmp_20_0_2_fu_300_p2 = (!p_038_2_0_1_reg_2180.read().is_01() || !ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_0_1_reg_2180.read() != ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read());
}

void IsLocalExtremum::thread_not_tmp_20_0_3_fu_372_p2() {
    not_tmp_20_0_3_fu_372_p2 = (!p_038_2_0_2_reg_2216.read().is_01() || !ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_0_2_reg_2216.read() != ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read());
}

void IsLocalExtremum::thread_not_tmp_20_0_4_fu_406_p2() {
    not_tmp_20_0_4_fu_406_p2 = (!p_038_2_0_3_reg_2252.read().is_01() || !ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_0_3_reg_2252.read() != ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read());
}

void IsLocalExtremum::thread_not_tmp_20_1_1_fu_514_p2() {
    not_tmp_20_1_1_fu_514_p2 = (!p_038_2_1_reg_2322.read().is_01() || !ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_1_reg_2322.read() != ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read());
}

void IsLocalExtremum::thread_not_tmp_20_1_2_fu_616_p2() {
    not_tmp_20_1_2_fu_616_p2 = (!p_038_2_1_1_reg_2358.read().is_01() || !ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_1_1_reg_2358.read() != ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read());
}

void IsLocalExtremum::thread_not_tmp_20_1_3_fu_688_p2() {
    not_tmp_20_1_3_fu_688_p2 = (!p_038_2_1_2_reg_2392.read().is_01() || !ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_1_2_reg_2392.read() != ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read());
}

void IsLocalExtremum::thread_not_tmp_20_1_4_fu_722_p2() {
    not_tmp_20_1_4_fu_722_p2 = (!p_038_2_1_3_reg_2438.read().is_01() || !ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_1_3_reg_2438.read() != ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read());
}

void IsLocalExtremum::thread_not_tmp_20_1_fu_474_p2() {
    not_tmp_20_1_fu_474_p2 = (!p_038_2_0_4_reg_2288.read().is_01() || !ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_0_4_reg_2288.read() != ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read());
}

void IsLocalExtremum::thread_not_tmp_20_2_1_fu_830_p2() {
    not_tmp_20_2_1_fu_830_p2 = (!p_038_2_2_reg_2508.read().is_01() || !ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_2_reg_2508.read() != ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read());
}

void IsLocalExtremum::thread_not_tmp_20_2_2_fu_858_p2() {
    not_tmp_20_2_2_fu_858_p2 = (!p_038_2_2_1_reg_2544.read().is_01() || !ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_2_1_reg_2544.read() != ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read());
}

void IsLocalExtremum::thread_not_tmp_20_2_3_fu_1026_p2() {
    not_tmp_20_2_3_fu_1026_p2 = (!p_038_2_2_2_reg_2582.read().is_01() || !ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_2_2_reg_2582.read() != ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read());
}

void IsLocalExtremum::thread_not_tmp_20_2_4_fu_1060_p2() {
    not_tmp_20_2_4_fu_1060_p2 = (!p_038_2_2_3_reg_2618.read().is_01() || !ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_2_3_reg_2618.read() != ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read());
}

void IsLocalExtremum::thread_not_tmp_20_2_fu_790_p2() {
    not_tmp_20_2_fu_790_p2 = (!p_038_2_1_4_reg_2474.read().is_01() || !ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_1_4_reg_2474.read() != ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read());
}

void IsLocalExtremum::thread_not_tmp_20_3_1_fu_1168_p2() {
    not_tmp_20_3_1_fu_1168_p2 = (!p_038_2_3_reg_2710.read().is_01() || !ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_3_reg_2710.read() != ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read());
}

void IsLocalExtremum::thread_not_tmp_20_3_2_fu_1250_p2() {
    not_tmp_20_3_2_fu_1250_p2 = (!p_038_2_3_1_reg_2746.read().is_01() || !ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_3_1_reg_2746.read() != ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read());
}

void IsLocalExtremum::thread_not_tmp_20_3_3_fu_1328_p2() {
    not_tmp_20_3_3_fu_1328_p2 = (!p_038_2_3_2_reg_2784.read().is_01() || !ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_3_2_reg_2784.read() != ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read());
}

void IsLocalExtremum::thread_not_tmp_20_3_4_fu_1424_p2() {
    not_tmp_20_3_4_fu_1424_p2 = (!p_038_2_3_3_reg_2840.read().is_01() || !ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_3_3_reg_2840.read() != ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read());
}

void IsLocalExtremum::thread_not_tmp_20_3_fu_1128_p2() {
    not_tmp_20_3_fu_1128_p2 = (!p_038_2_2_4_reg_2676.read().is_01() || !ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_2_4_reg_2676.read() != ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read());
}

void IsLocalExtremum::thread_not_tmp_20_4_1_fu_1532_p2() {
    not_tmp_20_4_1_fu_1532_p2 = (!p_038_2_4_reg_2930.read().is_01() || !ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_4_reg_2930.read() != ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read());
}

void IsLocalExtremum::thread_not_tmp_20_4_2_fu_1560_p2() {
    not_tmp_20_4_2_fu_1560_p2 = (!p_038_2_4_1_reg_2966.read().is_01() || !ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_4_1_reg_2966.read() != ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read());
}

void IsLocalExtremum::thread_not_tmp_20_4_3_fu_1632_p2() {
    not_tmp_20_4_3_fu_1632_p2 = (!p_038_2_4_2_reg_3004.read().is_01() || !ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_4_2_reg_3004.read() != ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read());
}

void IsLocalExtremum::thread_not_tmp_20_4_4_fu_1689_p2() {
    not_tmp_20_4_4_fu_1689_p2 = (!p_038_2_4_3_reg_3039.read().is_01() || !ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_4_3_reg_3039.read() != ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read());
}

void IsLocalExtremum::thread_not_tmp_20_4_fu_1492_p2() {
    not_tmp_20_4_fu_1492_p2 = (!p_038_2_3_4_reg_2876.read().is_01() || !ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_3_4_reg_2876.read() != ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read());
}

void IsLocalExtremum::thread_or_cond2_fu_1893_p2() {
    or_cond2_fu_1893_p2 = (tmp29_reg_3098.read() & tmp23_fu_1888_p2.read());
}

void IsLocalExtremum::thread_or_cond4_fu_1867_p2() {
    or_cond4_fu_1867_p2 = (tmp51_fu_1861_p2.read() & tmp45_fu_1825_p2.read());
}

void IsLocalExtremum::thread_p_038_2_0_1_fu_261_p3() {
    p_038_2_0_1_fu_261_p3 = (!tmp_19_0_1_fu_257_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_0_1_fu_257_p2.read()[0].to_bool())? Window_0_1_V_read_1_reg_2131.read(): p_038_2_reg_2156.read());
}

void IsLocalExtremum::thread_p_038_2_0_2_fu_290_p3() {
    p_038_2_0_2_fu_290_p3 = (!tmp_19_0_2_fu_286_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_0_2_fu_286_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read(): p_038_2_0_1_reg_2180.read());
}

void IsLocalExtremum::thread_p_038_2_0_3_fu_318_p3() {
    p_038_2_0_3_fu_318_p3 = (!tmp_19_0_3_fu_314_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_0_3_fu_314_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read(): p_038_2_0_2_reg_2216.read());
}

void IsLocalExtremum::thread_p_038_2_0_4_fu_396_p3() {
    p_038_2_0_4_fu_396_p3 = (!tmp_19_0_4_fu_392_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_0_4_fu_392_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read(): p_038_2_0_3_reg_2252.read());
}

void IsLocalExtremum::thread_p_038_2_1_1_fu_504_p3() {
    p_038_2_1_1_fu_504_p3 = (!tmp_19_1_1_fu_500_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_1_1_fu_500_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read(): p_038_2_1_reg_2322.read());
}

void IsLocalExtremum::thread_p_038_2_1_2_fu_532_p3() {
    p_038_2_1_2_fu_532_p3 = (!tmp_19_1_2_fu_528_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_1_2_fu_528_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read(): p_038_2_1_1_reg_2358.read());
}

void IsLocalExtremum::thread_p_038_2_1_3_fu_634_p3() {
    p_038_2_1_3_fu_634_p3 = (!tmp_19_1_3_fu_630_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_1_3_fu_630_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read(): p_038_2_1_2_reg_2392.read());
}

void IsLocalExtremum::thread_p_038_2_1_4_fu_712_p3() {
    p_038_2_1_4_fu_712_p3 = (!tmp_19_1_4_fu_708_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_1_4_fu_708_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read(): p_038_2_1_3_reg_2438.read());
}

void IsLocalExtremum::thread_p_038_2_1_fu_424_p3() {
    p_038_2_1_fu_424_p3 = (!tmp_19_1_fu_420_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_1_fu_420_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read(): p_038_2_0_4_reg_2288.read());
}

void IsLocalExtremum::thread_p_038_2_2_1_fu_820_p3() {
    p_038_2_2_1_fu_820_p3 = (!tmp_19_2_1_fu_816_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_2_1_fu_816_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read(): p_038_2_2_reg_2508.read());
}

void IsLocalExtremum::thread_p_038_2_2_2_fu_848_p3() {
    p_038_2_2_2_fu_848_p3 = (!tmp_19_2_2_fu_844_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_2_2_fu_844_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read(): p_038_2_2_1_reg_2544.read());
}

void IsLocalExtremum::thread_p_038_2_2_3_fu_876_p3() {
    p_038_2_2_3_fu_876_p3 = (!tmp_19_2_3_fu_872_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_2_3_fu_872_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read(): p_038_2_2_2_reg_2582.read());
}

void IsLocalExtremum::thread_p_038_2_2_4_fu_1050_p3() {
    p_038_2_2_4_fu_1050_p3 = (!tmp_19_2_4_fu_1046_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_2_4_fu_1046_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read(): p_038_2_2_3_reg_2618.read());
}

void IsLocalExtremum::thread_p_038_2_2_fu_740_p3() {
    p_038_2_2_fu_740_p3 = (!tmp_19_2_fu_736_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_2_fu_736_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read(): p_038_2_1_4_reg_2474.read());
}

void IsLocalExtremum::thread_p_038_2_3_1_fu_1158_p3() {
    p_038_2_3_1_fu_1158_p3 = (!tmp_19_3_1_fu_1154_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_3_1_fu_1154_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read(): p_038_2_3_reg_2710.read());
}

void IsLocalExtremum::thread_p_038_2_3_2_fu_1186_p3() {
    p_038_2_3_2_fu_1186_p3 = (!tmp_19_3_2_fu_1182_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_3_2_fu_1182_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read(): p_038_2_3_1_reg_2746.read());
}

void IsLocalExtremum::thread_p_038_2_3_3_fu_1274_p3() {
    p_038_2_3_3_fu_1274_p3 = (!tmp_19_3_3_fu_1270_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_3_3_fu_1270_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read(): p_038_2_3_2_reg_2784.read());
}

void IsLocalExtremum::thread_p_038_2_3_4_fu_1352_p3() {
    p_038_2_3_4_fu_1352_p3 = (!tmp_19_3_4_fu_1348_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_3_4_fu_1348_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read(): p_038_2_3_3_reg_2840.read());
}

void IsLocalExtremum::thread_p_038_2_3_fu_1078_p3() {
    p_038_2_3_fu_1078_p3 = (!tmp_19_3_fu_1074_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_3_fu_1074_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read(): p_038_2_2_4_reg_2676.read());
}

void IsLocalExtremum::thread_p_038_2_4_1_fu_1522_p3() {
    p_038_2_4_1_fu_1522_p3 = (!tmp_19_4_1_fu_1518_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_4_1_fu_1518_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read(): p_038_2_4_reg_2930.read());
}

void IsLocalExtremum::thread_p_038_2_4_2_fu_1550_p3() {
    p_038_2_4_2_fu_1550_p3 = (!tmp_19_4_2_fu_1546_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_4_2_fu_1546_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read(): p_038_2_4_1_reg_2966.read());
}

void IsLocalExtremum::thread_p_038_2_4_3_fu_1578_p3() {
    p_038_2_4_3_fu_1578_p3 = (!tmp_19_4_3_fu_1574_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_4_3_fu_1574_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read(): p_038_2_4_2_reg_3004.read());
}

void IsLocalExtremum::thread_p_038_2_4_fu_1442_p3() {
    p_038_2_4_fu_1442_p3 = (!tmp_19_4_fu_1438_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_19_4_fu_1438_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read(): p_038_2_3_4_reg_2876.read());
}

void IsLocalExtremum::thread_p_038_2_fu_234_p3() {
    p_038_2_fu_234_p3 = (!tmp_1_fu_228_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_1_fu_228_p2.read()[0].to_bool())? Window_0_0_V_read.read(): ap_const_lv16_F100);
}

void IsLocalExtremum::thread_p_0_2_0_1_fu_250_p3() {
    p_0_2_0_1_fu_250_p3 = (!tmp_17_0_1_fu_245_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_0_1_fu_245_p2.read()[0].to_bool())? Window_0_1_V_read_1_reg_2131.read(): p_0_2_cast_fu_242_p1.read());
}

void IsLocalExtremum::thread_p_0_2_0_2_fu_280_p3() {
    p_0_2_0_2_fu_280_p3 = (!tmp_17_0_2_fu_276_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_0_2_fu_276_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read(): p_0_2_0_1_reg_2168.read());
}

void IsLocalExtremum::thread_p_0_2_0_3_fu_308_p3() {
    p_0_2_0_3_fu_308_p3 = (!tmp_17_0_3_fu_304_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_0_3_fu_304_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read(): p_0_2_0_2_reg_2203.read());
}

void IsLocalExtremum::thread_p_0_2_0_4_fu_386_p3() {
    p_0_2_0_4_fu_386_p3 = (!tmp_17_0_4_fu_382_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_0_4_fu_382_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read(): p_0_2_0_3_reg_2239.read());
}

void IsLocalExtremum::thread_p_0_2_1_1_fu_494_p3() {
    p_0_2_1_1_fu_494_p3 = (!tmp_17_1_1_fu_490_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_1_1_fu_490_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read(): p_0_2_1_reg_2310.read());
}

void IsLocalExtremum::thread_p_0_2_1_2_fu_522_p3() {
    p_0_2_1_2_fu_522_p3 = (!tmp_17_1_2_fu_518_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_1_2_fu_518_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read(): p_0_2_1_1_reg_2345.read());
}

void IsLocalExtremum::thread_p_0_2_1_3_fu_624_p3() {
    p_0_2_1_3_fu_624_p3 = (!tmp_17_1_3_fu_620_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_1_3_fu_620_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read(): p_0_2_1_2_reg_2380.read());
}

void IsLocalExtremum::thread_p_0_2_1_4_fu_702_p3() {
    p_0_2_1_4_fu_702_p3 = (!tmp_17_1_4_fu_698_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_1_4_fu_698_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read(): p_0_2_1_3_reg_2425.read());
}

void IsLocalExtremum::thread_p_0_2_1_fu_414_p3() {
    p_0_2_1_fu_414_p3 = (!tmp_17_1_fu_410_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_1_fu_410_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read(): p_0_2_0_4_reg_2275.read());
}

void IsLocalExtremum::thread_p_0_2_2_1_fu_810_p3() {
    p_0_2_2_1_fu_810_p3 = (!tmp_17_2_1_fu_806_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_2_1_fu_806_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read(): p_0_2_2_reg_2496.read());
}

void IsLocalExtremum::thread_p_0_2_2_2_fu_838_p3() {
    p_0_2_2_2_fu_838_p3 = (!tmp_17_2_2_fu_834_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_2_2_fu_834_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read(): p_0_2_2_1_reg_2531.read());
}

void IsLocalExtremum::thread_p_0_2_2_3_fu_866_p3() {
    p_0_2_2_3_fu_866_p3 = (!tmp_17_2_3_fu_862_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_2_3_fu_862_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read(): p_0_2_2_2_reg_2568.read());
}

void IsLocalExtremum::thread_p_0_2_2_4_fu_1040_p3() {
    p_0_2_2_4_fu_1040_p3 = (!tmp_17_2_4_fu_1036_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_2_4_fu_1036_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read(): p_0_2_2_3_reg_2605.read());
}

void IsLocalExtremum::thread_p_0_2_2_fu_730_p3() {
    p_0_2_2_fu_730_p3 = (!tmp_17_2_fu_726_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_2_fu_726_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read(): p_0_2_1_4_reg_2461.read());
}

void IsLocalExtremum::thread_p_0_2_3_1_fu_1148_p3() {
    p_0_2_3_1_fu_1148_p3 = (!tmp_17_3_1_fu_1144_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_3_1_fu_1144_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read(): p_0_2_3_reg_2698.read());
}

void IsLocalExtremum::thread_p_0_2_3_2_fu_1176_p3() {
    p_0_2_3_2_fu_1176_p3 = (!tmp_17_3_2_fu_1172_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_3_2_fu_1172_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read(): p_0_2_3_1_reg_2733.read());
}

void IsLocalExtremum::thread_p_0_2_3_3_fu_1264_p3() {
    p_0_2_3_3_fu_1264_p3 = (!tmp_17_3_3_fu_1260_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_3_3_fu_1260_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read(): p_0_2_3_2_reg_2770.read());
}

void IsLocalExtremum::thread_p_0_2_3_4_fu_1342_p3() {
    p_0_2_3_4_fu_1342_p3 = (!tmp_17_3_4_fu_1338_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_3_4_fu_1338_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read(): p_0_2_3_3_reg_2827.read());
}

void IsLocalExtremum::thread_p_0_2_3_fu_1068_p3() {
    p_0_2_3_fu_1068_p3 = (!tmp_17_3_fu_1064_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_3_fu_1064_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read(): p_0_2_2_4_reg_2662.read());
}

void IsLocalExtremum::thread_p_0_2_4_1_fu_1512_p3() {
    p_0_2_4_1_fu_1512_p3 = (!tmp_17_4_1_fu_1508_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_4_1_fu_1508_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read(): p_0_2_4_reg_2918.read());
}

void IsLocalExtremum::thread_p_0_2_4_2_fu_1540_p3() {
    p_0_2_4_2_fu_1540_p3 = (!tmp_17_4_2_fu_1536_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_4_2_fu_1536_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read(): p_0_2_4_1_reg_2953.read());
}

void IsLocalExtremum::thread_p_0_2_4_3_fu_1568_p3() {
    p_0_2_4_3_fu_1568_p3 = (!tmp_17_4_3_fu_1564_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_4_3_fu_1564_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read(): p_0_2_4_2_reg_2990.read());
}

void IsLocalExtremum::thread_p_0_2_4_fu_1432_p3() {
    p_0_2_4_fu_1432_p3 = (!tmp_17_4_fu_1428_p2.read()[0].is_01())? sc_lv<16>(): ((tmp_17_4_fu_1428_p2.read()[0].to_bool())? ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read(): p_0_2_3_4_reg_2863.read());
}

void IsLocalExtremum::thread_p_0_2_cast_fu_242_p1() {
    p_0_2_cast_fu_242_p1 = esl_zext<16,15>(p_0_2_reg_2146.read());
}

void IsLocalExtremum::thread_p_0_2_fu_220_p3() {
    p_0_2_fu_220_p3 = (!tmp_s_fu_210_p2.read()[0].is_01())? sc_lv<15>(): ((tmp_s_fu_210_p2.read()[0].to_bool())? tmp_110_fu_216_p1.read(): ap_const_lv15_F00);
}

void IsLocalExtremum::thread_tmp10_demorgan_fu_1192_p2() {
    tmp10_demorgan_fu_1192_p2 = (ap_pipeline_reg_pp0_iter16_tmp_17_3_reg_2693.read() | tmp_17_3_1_reg_2727.read());
}

void IsLocalExtremum::thread_tmp10_fu_1363_p2() {
    tmp10_fu_1363_p2 = (ap_pipeline_reg_pp0_iter18_tmp1_reg_2791.read() & not_tmp_17_2_4_fu_1358_p2.read());
}

void IsLocalExtremum::thread_tmp11_fu_1368_p2() {
    tmp11_fu_1368_p2 = (tmp10_fu_1363_p2.read() & ap_pipeline_reg_pp0_iter18_tmp9_reg_2630.read());
}

void IsLocalExtremum::thread_tmp12_fu_1737_p2() {
    tmp12_fu_1737_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_4_2_reg_2983.read() | tmp17_fu_1732_p2.read());
}

void IsLocalExtremum::thread_tmp13_fu_1373_p2() {
    tmp13_fu_1373_p2 = (ap_pipeline_reg_pp0_iter18_tmp_17_3_2_reg_2763.read() | tmp11_fu_1368_p2.read());
}

void IsLocalExtremum::thread_tmp14_fu_1383_p2() {
    tmp14_fu_1383_p2 = (tmp13_fu_1373_p2.read() & not_tmp_17_3_3_fu_1378_p2.read());
}

void IsLocalExtremum::thread_tmp15_fu_1720_p2() {
    tmp15_fu_1720_p2 = (not_tmp_17_4_fu_1710_p2.read() & not_tmp_17_4_1_fu_1715_p2.read());
}

void IsLocalExtremum::thread_tmp16_fu_1726_p2() {
    tmp16_fu_1726_p2 = (tmp15_fu_1720_p2.read() & not_tmp_17_3_4_fu_1705_p2.read());
}

void IsLocalExtremum::thread_tmp17_fu_1732_p2() {
    tmp17_fu_1732_p2 = (tmp16_fu_1726_p2.read() & ap_pipeline_reg_pp0_iter24_tmp14_reg_2888.read());
}

void IsLocalExtremum::thread_tmp187_demorgan_fu_552_p2() {
    tmp187_demorgan_fu_552_p2 = (tmp188_demorgan_fu_548_p2.read() | ap_pipeline_reg_pp0_iter6_tmp_17_0_4_reg_2269.read());
}

void IsLocalExtremum::thread_tmp188_demorgan_fu_548_p2() {
    tmp188_demorgan_fu_548_p2 = (ap_pipeline_reg_pp0_iter6_tmp_17_1_reg_2305.read() | tmp_17_1_1_reg_2339.read());
}

void IsLocalExtremum::thread_tmp18_fu_1202_p2() {
    tmp18_fu_1202_p2 = (ap_pipeline_reg_pp0_iter16_tmp_17_2_3_reg_2599.read() | ap_pipeline_reg_pp0_iter16_tmp_17_2_4_reg_2655.read());
}

void IsLocalExtremum::thread_tmp190_demorgan_fu_896_p2() {
    tmp190_demorgan_fu_896_p2 = (tmp191_demorgan_fu_892_p2.read() | ap_pipeline_reg_pp0_iter12_tmp_17_1_4_reg_2455.read());
}

void IsLocalExtremum::thread_tmp191_demorgan_fu_892_p2() {
    tmp191_demorgan_fu_892_p2 = (ap_pipeline_reg_pp0_iter12_tmp_17_2_reg_2491.read() | ap_pipeline_reg_pp0_iter12_tmp_17_2_1_reg_2525.read());
}

void IsLocalExtremum::thread_tmp19_fu_1206_p2() {
    tmp19_fu_1206_p2 = (tmp18_fu_1202_p2.read() | ap_pipeline_reg_pp0_iter16_tmp_17_2_2_reg_2561.read());
}

void IsLocalExtremum::thread_tmp1_fu_1196_p2() {
    tmp1_fu_1196_p2 = (tmp10_demorgan_fu_1192_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp20_fu_1220_p2() {
    tmp20_fu_1220_p2 = (tmp_3_fu_1211_p2.read() & tmp1_fu_1196_p2.read());
}

void IsLocalExtremum::thread_tmp210_demorgan_fu_1759_p2() {
    tmp210_demorgan_fu_1759_p2 = (ap_pipeline_reg_pp0_iter24_tmp_17_4_3_reg_3021.read() | tmp_17_4_4_reg_3055.read());
}

void IsLocalExtremum::thread_tmp213_demorgan_fu_593_p2() {
    tmp213_demorgan_fu_593_p2 = (tmp214_demorgan_fu_589_p2.read() | ap_pipeline_reg_pp0_iter6_tmp_19_0_4_reg_2282.read());
}

void IsLocalExtremum::thread_tmp214_demorgan_fu_589_p2() {
    tmp214_demorgan_fu_589_p2 = (ap_pipeline_reg_pp0_iter6_tmp_19_1_reg_2317.read() | tmp_19_1_1_reg_2352.read());
}

void IsLocalExtremum::thread_tmp216_demorgan_fu_973_p2() {
    tmp216_demorgan_fu_973_p2 = (tmp217_demorgan_fu_969_p2.read() | ap_pipeline_reg_pp0_iter12_tmp_19_1_4_reg_2468.read());
}

void IsLocalExtremum::thread_tmp217_demorgan_fu_969_p2() {
    tmp217_demorgan_fu_969_p2 = (ap_pipeline_reg_pp0_iter12_tmp_19_2_reg_2503.read() | ap_pipeline_reg_pp0_iter12_tmp_19_2_1_reg_2538.read());
}

void IsLocalExtremum::thread_tmp21_fu_1878_p2() {
    tmp21_fu_1878_p2 = (ap_pipeline_reg_pp0_iter25_not_tmp_17_3_3_reg_2883.read() & not_tmp_17_3_4_reg_3093.read());
}

void IsLocalExtremum::thread_tmp22_fu_1882_p2() {
    tmp22_fu_1882_p2 = (tmp21_fu_1878_p2.read() & not_tmp_17_3_2_fu_1873_p2.read());
}

void IsLocalExtremum::thread_tmp236_demorgan_fu_1840_p2() {
    tmp236_demorgan_fu_1840_p2 = (ap_pipeline_reg_pp0_iter24_tmp_19_4_3_reg_3033.read() | tmp_19_4_4_reg_3060.read());
}

void IsLocalExtremum::thread_tmp23_fu_1888_p2() {
    tmp23_fu_1888_p2 = (tmp22_fu_1882_p2.read() & ap_pipeline_reg_pp0_iter25_tmp20_reg_2801.read());
}

void IsLocalExtremum::thread_tmp24_fu_1747_p2() {
    tmp24_fu_1747_p2 = (not_tmp_17_4_1_fu_1715_p2.read() & not_tmp_17_4_2_fu_1742_p2.read());
}

void IsLocalExtremum::thread_tmp25_fu_1753_p2() {
    tmp25_fu_1753_p2 = (tmp24_fu_1747_p2.read() & not_tmp_17_4_fu_1710_p2.read());
}

void IsLocalExtremum::thread_tmp26_fu_1763_p2() {
    tmp26_fu_1763_p2 = (tmp210_demorgan_fu_1759_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp27_fu_1769_p2() {
    tmp27_fu_1769_p2 = (tmp12_fu_1737_p2.read() & tmp_2_reg_3065.read());
}

void IsLocalExtremum::thread_tmp28_fu_1774_p2() {
    tmp28_fu_1774_p2 = (tmp27_fu_1769_p2.read() & tmp26_fu_1763_p2.read());
}

void IsLocalExtremum::thread_tmp29_fu_1780_p2() {
    tmp29_fu_1780_p2 = (tmp28_fu_1774_p2.read() & tmp25_fu_1753_p2.read());
}

void IsLocalExtremum::thread_tmp2_fu_557_p2() {
    tmp2_fu_557_p2 = (tmp187_demorgan_fu_552_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp30_fu_584_p2() {
    tmp30_fu_584_p2 = (ap_pipeline_reg_pp0_iter6_tmp_19_0_2_reg_2210.read() & not_tmp_19_0_3_fu_579_p2.read());
}

void IsLocalExtremum::thread_tmp31_fu_598_p2() {
    tmp31_fu_598_p2 = (tmp213_demorgan_fu_593_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp32_fu_964_p2() {
    tmp32_fu_964_p2 = (ap_pipeline_reg_pp0_iter12_tmp_54_reg_2409.read() & not_tmp_19_1_3_fu_959_p2.read());
}

void IsLocalExtremum::thread_tmp33_fu_978_p2() {
    tmp33_fu_978_p2 = (tmp216_demorgan_fu_973_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp34_fu_1001_p2() {
    tmp34_fu_1001_p2 = (tmp_56_fu_990_p2.read() & not_tmp_19_2_3_fu_995_p2.read());
}

void IsLocalExtremum::thread_tmp35_fu_1230_p2() {
    tmp35_fu_1230_p2 = (tmp39_demorgan_fu_1226_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp36_fu_1398_p2() {
    tmp36_fu_1398_p2 = (ap_pipeline_reg_pp0_iter18_tmp35_reg_2806.read() & not_tmp_19_2_4_fu_1393_p2.read());
}

void IsLocalExtremum::thread_tmp37_fu_1418_p2() {
    tmp37_fu_1418_p2 = (tmp_58_fu_1408_p2.read() & not_tmp_19_3_3_fu_1413_p2.read());
}

void IsLocalExtremum::thread_tmp38_fu_1786_p2() {
    tmp38_fu_1786_p2 = (not_tmp_19_4_reg_3076.read() & not_tmp_19_4_1_reg_3082.read());
}

void IsLocalExtremum::thread_tmp39_demorgan_fu_1226_p2() {
    tmp39_demorgan_fu_1226_p2 = (ap_pipeline_reg_pp0_iter16_tmp_19_3_reg_2705.read() | tmp_19_3_1_reg_2740.read());
}

void IsLocalExtremum::thread_tmp39_fu_1790_p2() {
    tmp39_fu_1790_p2 = (tmp38_fu_1786_p2.read() & not_tmp_19_3_4_reg_3070.read());
}

void IsLocalExtremum::thread_tmp3_fu_563_p2() {
    tmp3_fu_563_p2 = (tmp_fu_543_p2.read() & tmp2_fu_557_p2.read());
}

void IsLocalExtremum::thread_tmp40_fu_1236_p2() {
    tmp40_fu_1236_p2 = (ap_pipeline_reg_pp0_iter16_tmp_19_2_3_reg_2612.read() | ap_pipeline_reg_pp0_iter16_tmp_19_2_4_reg_2669.read());
}

void IsLocalExtremum::thread_tmp41_fu_1240_p2() {
    tmp41_fu_1240_p2 = (tmp40_fu_1236_p2.read() | ap_pipeline_reg_pp0_iter16_tmp_19_2_2_reg_2575.read());
}

void IsLocalExtremum::thread_tmp42_fu_1254_p2() {
    tmp42_fu_1254_p2 = (tmp_61_fu_1245_p2.read() & tmp35_fu_1230_p2.read());
}

void IsLocalExtremum::thread_tmp43_fu_1815_p2() {
    tmp43_fu_1815_p2 = (ap_pipeline_reg_pp0_iter24_not_tmp_19_3_3_reg_2898.read() & not_tmp_19_3_4_reg_3070.read());
}

void IsLocalExtremum::thread_tmp44_fu_1819_p2() {
    tmp44_fu_1819_p2 = (tmp43_fu_1815_p2.read() & not_tmp_19_3_2_fu_1805_p2.read());
}

void IsLocalExtremum::thread_tmp45_fu_1825_p2() {
    tmp45_fu_1825_p2 = (tmp44_fu_1819_p2.read() & ap_pipeline_reg_pp0_iter24_tmp42_reg_2816.read());
}

void IsLocalExtremum::thread_tmp46_fu_1830_p2() {
    tmp46_fu_1830_p2 = (not_tmp_19_4_1_reg_3082.read() & not_tmp_19_4_2_fu_1810_p2.read());
}

void IsLocalExtremum::thread_tmp47_fu_1835_p2() {
    tmp47_fu_1835_p2 = (tmp46_fu_1830_p2.read() & not_tmp_19_4_reg_3076.read());
}

void IsLocalExtremum::thread_tmp48_fu_1844_p2() {
    tmp48_fu_1844_p2 = (tmp236_demorgan_fu_1840_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp49_fu_1850_p2() {
    tmp49_fu_1850_p2 = (tmp_60_fu_1800_p2.read() & tmp_5_reg_3088.read());
}

void IsLocalExtremum::thread_tmp4_fu_569_p2() {
    tmp4_fu_569_p2 = (tmp_17_1_2_fu_518_p2.read() | tmp3_fu_563_p2.read());
}

void IsLocalExtremum::thread_tmp50_fu_1855_p2() {
    tmp50_fu_1855_p2 = (tmp49_fu_1850_p2.read() & tmp48_fu_1844_p2.read());
}

void IsLocalExtremum::thread_tmp51_fu_1861_p2() {
    tmp51_fu_1861_p2 = (tmp50_fu_1855_p2.read() & tmp47_fu_1835_p2.read());
}

void IsLocalExtremum::thread_tmp5_fu_887_p2() {
    tmp5_fu_887_p2 = (ap_pipeline_reg_pp0_iter12_tmp4_reg_2399.read() & not_tmp_17_1_3_fu_882_p2.read());
}

void IsLocalExtremum::thread_tmp6_fu_901_p2() {
    tmp6_fu_901_p2 = (tmp190_demorgan_fu_896_p2.read() ^ ap_const_lv1_1);
}

void IsLocalExtremum::thread_tmp7_fu_907_p2() {
    tmp7_fu_907_p2 = (tmp5_fu_887_p2.read() & tmp6_fu_901_p2.read());
}

void IsLocalExtremum::thread_tmp8_fu_913_p2() {
    tmp8_fu_913_p2 = (tmp_17_2_2_reg_2561.read() | tmp7_fu_907_p2.read());
}

void IsLocalExtremum::thread_tmp9_fu_924_p2() {
    tmp9_fu_924_p2 = (tmp8_fu_913_p2.read() & not_tmp_17_2_3_fu_918_p2.read());
}

void IsLocalExtremum::thread_tmp_100_fu_1487_p2() {
    tmp_100_fu_1487_p2 = (tmp_19_3_4_reg_2870.read() | tmp_99_fu_1482_p2.read());
}

void IsLocalExtremum::thread_tmp_101_fu_1496_p2() {
    tmp_101_fu_1496_p2 = (tmp_100_fu_1487_p2.read() & not_tmp_20_4_fu_1492_p2.read());
}

void IsLocalExtremum::thread_tmp_102_fu_1502_p2() {
    tmp_102_fu_1502_p2 = (tmp_19_4_fu_1438_p2.read() | tmp_101_fu_1496_p2.read());
}

void IsLocalExtremum::thread_tmp_103_fu_1613_p2() {
    tmp_103_fu_1613_p2 = (ap_pipeline_reg_pp0_iter22_tmp_102_reg_2942.read() & ap_pipeline_reg_pp0_iter22_not_tmp_20_4_1_reg_2978.read());
}

void IsLocalExtremum::thread_tmp_104_fu_1617_p2() {
    tmp_104_fu_1617_p2 = (ap_pipeline_reg_pp0_iter22_tmp_19_4_1_reg_2960.read() | tmp_103_fu_1613_p2.read());
}

void IsLocalExtremum::thread_tmp_105_fu_1622_p2() {
    tmp_105_fu_1622_p2 = (tmp_104_fu_1617_p2.read() & not_tmp_20_4_2_reg_3016.read());
}

void IsLocalExtremum::thread_tmp_106_fu_1627_p2() {
    tmp_106_fu_1627_p2 = (tmp_19_4_2_reg_2997.read() | tmp_105_fu_1622_p2.read());
}

void IsLocalExtremum::thread_tmp_107_fu_1636_p2() {
    tmp_107_fu_1636_p2 = (tmp_106_fu_1627_p2.read() & not_tmp_20_4_3_fu_1632_p2.read());
}

void IsLocalExtremum::thread_tmp_108_fu_1685_p2() {
    tmp_108_fu_1685_p2 = (tmp_19_4_3_reg_3033.read() | tmp_107_reg_3050.read());
}

void IsLocalExtremum::thread_tmp_109_fu_1693_p2() {
    tmp_109_fu_1693_p2 = (tmp_108_fu_1685_p2.read() & not_tmp_20_4_4_fu_1689_p2.read());
}

void IsLocalExtremum::thread_tmp_10_fu_430_p2() {
    tmp_10_fu_430_p2 = (ap_pipeline_reg_pp0_iter4_tmp_17_0_3_reg_2233.read() | ap_pipeline_reg_pp0_iter4_tmp_9_reg_2259.read());
}

void IsLocalExtremum::thread_tmp_110_fu_216_p1() {
    tmp_110_fu_216_p1 = Window_0_0_V_read.read().range(15-1, 0);
}

void IsLocalExtremum::thread_tmp_11_fu_434_p2() {
    tmp_11_fu_434_p2 = (tmp_10_fu_430_p2.read() & not_tmp_18_0_4_reg_2295.read());
}

void IsLocalExtremum::thread_tmp_12_fu_439_p2() {
    tmp_12_fu_439_p2 = (tmp_17_0_4_reg_2269.read() | tmp_11_fu_434_p2.read());
}

void IsLocalExtremum::thread_tmp_13_fu_448_p2() {
    tmp_13_fu_448_p2 = (tmp_12_fu_439_p2.read() & not_tmp_18_1_fu_444_p2.read());
}

void IsLocalExtremum::thread_tmp_14_fu_454_p2() {
    tmp_14_fu_454_p2 = (tmp_17_1_fu_410_p2.read() | tmp_13_fu_448_p2.read());
}

void IsLocalExtremum::thread_tmp_15_fu_640_p2() {
    tmp_15_fu_640_p2 = (ap_pipeline_reg_pp0_iter7_tmp_14_reg_2329.read() & ap_pipeline_reg_pp0_iter7_not_tmp_18_1_1_reg_2365.read());
}

void IsLocalExtremum::thread_tmp_16_fu_644_p2() {
    tmp_16_fu_644_p2 = (ap_pipeline_reg_pp0_iter7_tmp_17_1_1_reg_2339.read() | tmp_15_fu_640_p2.read());
}

void IsLocalExtremum::thread_tmp_17_0_1_fu_245_p2() {
    tmp_17_0_1_fu_245_p2 = (!p_0_2_cast_fu_242_p1.read().is_01() || !Window_0_1_V_read_1_reg_2131.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_cast_fu_242_p1.read()) < sc_bigint<16>(Window_0_1_V_read_1_reg_2131.read()));
}

void IsLocalExtremum::thread_tmp_17_0_2_fu_276_p2() {
    tmp_17_0_2_fu_276_p2 = (!p_0_2_0_1_reg_2168.read().is_01() || !ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_0_1_reg_2168.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read()));
}

void IsLocalExtremum::thread_tmp_17_0_3_fu_304_p2() {
    tmp_17_0_3_fu_304_p2 = (!p_0_2_0_2_reg_2203.read().is_01() || !ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_0_2_reg_2203.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read()));
}

void IsLocalExtremum::thread_tmp_17_0_4_fu_382_p2() {
    tmp_17_0_4_fu_382_p2 = (!p_0_2_0_3_reg_2239.read().is_01() || !ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_0_3_reg_2239.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read()));
}

void IsLocalExtremum::thread_tmp_17_1_1_fu_490_p2() {
    tmp_17_1_1_fu_490_p2 = (!p_0_2_1_reg_2310.read().is_01() || !ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_1_reg_2310.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read()));
}

void IsLocalExtremum::thread_tmp_17_1_2_fu_518_p2() {
    tmp_17_1_2_fu_518_p2 = (!p_0_2_1_1_reg_2345.read().is_01() || !ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_1_1_reg_2345.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read()));
}

void IsLocalExtremum::thread_tmp_17_1_3_fu_620_p2() {
    tmp_17_1_3_fu_620_p2 = (!p_0_2_1_2_reg_2380.read().is_01() || !ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_1_2_reg_2380.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read()));
}

void IsLocalExtremum::thread_tmp_17_1_4_fu_698_p2() {
    tmp_17_1_4_fu_698_p2 = (!p_0_2_1_3_reg_2425.read().is_01() || !ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_1_3_reg_2425.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read()));
}

void IsLocalExtremum::thread_tmp_17_1_fu_410_p2() {
    tmp_17_1_fu_410_p2 = (!p_0_2_0_4_reg_2275.read().is_01() || !ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_0_4_reg_2275.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read()));
}

void IsLocalExtremum::thread_tmp_17_2_1_fu_806_p2() {
    tmp_17_2_1_fu_806_p2 = (!p_0_2_2_reg_2496.read().is_01() || !ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_2_reg_2496.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read()));
}

void IsLocalExtremum::thread_tmp_17_2_2_fu_834_p2() {
    tmp_17_2_2_fu_834_p2 = (!p_0_2_2_1_reg_2531.read().is_01() || !ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_2_1_reg_2531.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read()));
}

void IsLocalExtremum::thread_tmp_17_2_3_fu_862_p2() {
    tmp_17_2_3_fu_862_p2 = (!p_0_2_2_2_reg_2568.read().is_01() || !ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_2_2_reg_2568.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read()));
}

void IsLocalExtremum::thread_tmp_17_2_4_fu_1036_p2() {
    tmp_17_2_4_fu_1036_p2 = (!p_0_2_2_3_reg_2605.read().is_01() || !ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_2_3_reg_2605.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read()));
}

void IsLocalExtremum::thread_tmp_17_2_fu_726_p2() {
    tmp_17_2_fu_726_p2 = (!p_0_2_1_4_reg_2461.read().is_01() || !ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_1_4_reg_2461.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read()));
}

void IsLocalExtremum::thread_tmp_17_3_1_fu_1144_p2() {
    tmp_17_3_1_fu_1144_p2 = (!p_0_2_3_reg_2698.read().is_01() || !ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_3_reg_2698.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read()));
}

void IsLocalExtremum::thread_tmp_17_3_2_fu_1172_p2() {
    tmp_17_3_2_fu_1172_p2 = (!p_0_2_3_1_reg_2733.read().is_01() || !ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_3_1_reg_2733.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read()));
}

void IsLocalExtremum::thread_tmp_17_3_3_fu_1260_p2() {
    tmp_17_3_3_fu_1260_p2 = (!p_0_2_3_2_reg_2770.read().is_01() || !ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_3_2_reg_2770.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read()));
}

void IsLocalExtremum::thread_tmp_17_3_4_fu_1338_p2() {
    tmp_17_3_4_fu_1338_p2 = (!p_0_2_3_3_reg_2827.read().is_01() || !ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_3_3_reg_2827.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read()));
}

void IsLocalExtremum::thread_tmp_17_3_fu_1064_p2() {
    tmp_17_3_fu_1064_p2 = (!p_0_2_2_4_reg_2662.read().is_01() || !ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_2_4_reg_2662.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read()));
}

void IsLocalExtremum::thread_tmp_17_4_1_fu_1508_p2() {
    tmp_17_4_1_fu_1508_p2 = (!p_0_2_4_reg_2918.read().is_01() || !ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_4_reg_2918.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read()));
}

void IsLocalExtremum::thread_tmp_17_4_2_fu_1536_p2() {
    tmp_17_4_2_fu_1536_p2 = (!p_0_2_4_1_reg_2953.read().is_01() || !ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_4_1_reg_2953.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read()));
}

void IsLocalExtremum::thread_tmp_17_4_3_fu_1564_p2() {
    tmp_17_4_3_fu_1564_p2 = (!p_0_2_4_2_reg_2990.read().is_01() || !ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_4_2_reg_2990.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read()));
}

void IsLocalExtremum::thread_tmp_17_4_4_fu_1642_p2() {
    tmp_17_4_4_fu_1642_p2 = (!p_0_2_4_3_reg_3027.read().is_01() || !ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_4_3_reg_3027.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read()));
}

void IsLocalExtremum::thread_tmp_17_4_fu_1428_p2() {
    tmp_17_4_fu_1428_p2 = (!p_0_2_3_4_reg_2863.read().is_01() || !ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_0_2_3_4_reg_2863.read()) < sc_bigint<16>(ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read()));
}

void IsLocalExtremum::thread_tmp_17_fu_649_p2() {
    tmp_17_fu_649_p2 = (tmp_16_fu_644_p2.read() & not_tmp_18_1_2_reg_2404.read());
}

void IsLocalExtremum::thread_tmp_18_fu_654_p2() {
    tmp_18_fu_654_p2 = (tmp_17_1_2_reg_2375.read() | tmp_17_fu_649_p2.read());
}

void IsLocalExtremum::thread_tmp_19_0_1_fu_257_p2() {
    tmp_19_0_1_fu_257_p2 = (!p_038_2_reg_2156.read().is_01() || !Window_0_1_V_read_1_reg_2131.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_reg_2156.read()) > sc_bigint<16>(Window_0_1_V_read_1_reg_2131.read()));
}

void IsLocalExtremum::thread_tmp_19_0_2_fu_286_p2() {
    tmp_19_0_2_fu_286_p2 = (!p_038_2_0_1_reg_2180.read().is_01() || !ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_0_1_reg_2180.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter1_Window_0_2_V_read_1_reg_2121.read()));
}

void IsLocalExtremum::thread_tmp_19_0_3_fu_314_p2() {
    tmp_19_0_3_fu_314_p2 = (!p_038_2_0_2_reg_2216.read().is_01() || !ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_0_2_reg_2216.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter2_Window_0_3_V_read_1_reg_2111.read()));
}

void IsLocalExtremum::thread_tmp_19_0_4_fu_392_p2() {
    tmp_19_0_4_fu_392_p2 = (!p_038_2_0_3_reg_2252.read().is_01() || !ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_0_3_reg_2252.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter3_Window_0_4_V_read_1_reg_2101.read()));
}

void IsLocalExtremum::thread_tmp_19_1_1_fu_500_p2() {
    tmp_19_1_1_fu_500_p2 = (!p_038_2_1_reg_2322.read().is_01() || !ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_1_reg_2322.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter5_Window_1_1_V_read_1_reg_2081.read()));
}

void IsLocalExtremum::thread_tmp_19_1_2_fu_528_p2() {
    tmp_19_1_2_fu_528_p2 = (!p_038_2_1_1_reg_2358.read().is_01() || !ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_1_1_reg_2358.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter6_Window_1_2_V_read_1_reg_2071.read()));
}

void IsLocalExtremum::thread_tmp_19_1_3_fu_630_p2() {
    tmp_19_1_3_fu_630_p2 = (!p_038_2_1_2_reg_2392.read().is_01() || !ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_1_2_reg_2392.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter7_Window_1_3_V_read_1_reg_2061.read()));
}

void IsLocalExtremum::thread_tmp_19_1_4_fu_708_p2() {
    tmp_19_1_4_fu_708_p2 = (!p_038_2_1_3_reg_2438.read().is_01() || !ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_1_3_reg_2438.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter8_Window_1_4_V_read_1_reg_2051.read()));
}

void IsLocalExtremum::thread_tmp_19_1_fu_420_p2() {
    tmp_19_1_fu_420_p2 = (!p_038_2_0_4_reg_2288.read().is_01() || !ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_0_4_reg_2288.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter4_Window_1_0_V_read_1_reg_2091.read()));
}

void IsLocalExtremum::thread_tmp_19_2_1_fu_816_p2() {
    tmp_19_2_1_fu_816_p2 = (!p_038_2_2_reg_2508.read().is_01() || !ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_2_reg_2508.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter10_Window_2_1_V_read_1_reg_2031.read()));
}

void IsLocalExtremum::thread_tmp_19_2_2_fu_844_p2() {
    tmp_19_2_2_fu_844_p2 = (!p_038_2_2_1_reg_2544.read().is_01() || !ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_2_1_reg_2544.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter11_Window_2_2_V_read_1_reg_2021.read()));
}

void IsLocalExtremum::thread_tmp_19_2_3_fu_872_p2() {
    tmp_19_2_3_fu_872_p2 = (!p_038_2_2_2_reg_2582.read().is_01() || !ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_2_2_reg_2582.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter12_Window_2_3_V_read_1_reg_2011.read()));
}

void IsLocalExtremum::thread_tmp_19_2_4_fu_1046_p2() {
    tmp_19_2_4_fu_1046_p2 = (!p_038_2_2_3_reg_2618.read().is_01() || !ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_2_3_reg_2618.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter13_Window_2_4_V_read_1_reg_2001.read()));
}

void IsLocalExtremum::thread_tmp_19_2_fu_736_p2() {
    tmp_19_2_fu_736_p2 = (!p_038_2_1_4_reg_2474.read().is_01() || !ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_1_4_reg_2474.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter9_Window_2_0_V_read_1_reg_2041.read()));
}

void IsLocalExtremum::thread_tmp_19_3_1_fu_1154_p2() {
    tmp_19_3_1_fu_1154_p2 = (!p_038_2_3_reg_2710.read().is_01() || !ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_3_reg_2710.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter15_Window_3_1_V_read_1_reg_1981.read()));
}

void IsLocalExtremum::thread_tmp_19_3_2_fu_1182_p2() {
    tmp_19_3_2_fu_1182_p2 = (!p_038_2_3_1_reg_2746.read().is_01() || !ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_3_1_reg_2746.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter16_Window_3_2_V_read_1_reg_1971.read()));
}

void IsLocalExtremum::thread_tmp_19_3_3_fu_1270_p2() {
    tmp_19_3_3_fu_1270_p2 = (!p_038_2_3_2_reg_2784.read().is_01() || !ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_3_2_reg_2784.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter17_Window_3_3_V_read_1_reg_1961.read()));
}

void IsLocalExtremum::thread_tmp_19_3_4_fu_1348_p2() {
    tmp_19_3_4_fu_1348_p2 = (!p_038_2_3_3_reg_2840.read().is_01() || !ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_3_3_reg_2840.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter18_Window_3_4_V_read_1_reg_1951.read()));
}

void IsLocalExtremum::thread_tmp_19_3_fu_1074_p2() {
    tmp_19_3_fu_1074_p2 = (!p_038_2_2_4_reg_2676.read().is_01() || !ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_2_4_reg_2676.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter14_Window_3_0_V_read_1_reg_1991.read()));
}

void IsLocalExtremum::thread_tmp_19_4_1_fu_1518_p2() {
    tmp_19_4_1_fu_1518_p2 = (!p_038_2_4_reg_2930.read().is_01() || !ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_4_reg_2930.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter20_Window_4_1_V_read_1_reg_1931.read()));
}

void IsLocalExtremum::thread_tmp_19_4_2_fu_1546_p2() {
    tmp_19_4_2_fu_1546_p2 = (!p_038_2_4_1_reg_2966.read().is_01() || !ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_4_1_reg_2966.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter21_Window_4_2_V_read_1_reg_1921.read()));
}

void IsLocalExtremum::thread_tmp_19_4_3_fu_1574_p2() {
    tmp_19_4_3_fu_1574_p2 = (!p_038_2_4_2_reg_3004.read().is_01() || !ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_4_2_reg_3004.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter22_Window_4_3_V_read_1_reg_1911.read()));
}

void IsLocalExtremum::thread_tmp_19_4_4_fu_1646_p2() {
    tmp_19_4_4_fu_1646_p2 = (!p_038_2_4_3_reg_3039.read().is_01() || !ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_4_3_reg_3039.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter23_Window_4_4_V_read_1_reg_1903.read()));
}

void IsLocalExtremum::thread_tmp_19_4_fu_1438_p2() {
    tmp_19_4_fu_1438_p2 = (!p_038_2_3_4_reg_2876.read().is_01() || !ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read().is_01())? sc_lv<1>(): (sc_bigint<16>(p_038_2_3_4_reg_2876.read()) > sc_bigint<16>(ap_pipeline_reg_pp0_iter19_Window_4_0_V_read_1_reg_1941.read()));
}

void IsLocalExtremum::thread_tmp_19_fu_663_p2() {
    tmp_19_fu_663_p2 = (tmp_18_fu_654_p2.read() & not_tmp_18_1_3_fu_659_p2.read());
}

void IsLocalExtremum::thread_tmp_1_fu_228_p2() {
    tmp_1_fu_228_p2 = (!Window_0_0_V_read.read().is_01() || !ap_const_lv16_F100.is_01())? sc_lv<1>(): (sc_bigint<16>(Window_0_0_V_read.read()) < sc_bigint<16>(ap_const_lv16_F100));
}

void IsLocalExtremum::thread_tmp_20_fu_746_p2() {
    tmp_20_fu_746_p2 = (ap_pipeline_reg_pp0_iter9_tmp_17_1_3_reg_2419.read() | ap_pipeline_reg_pp0_iter9_tmp_19_reg_2445.read());
}

void IsLocalExtremum::thread_tmp_21_fu_750_p2() {
    tmp_21_fu_750_p2 = (tmp_20_fu_746_p2.read() & not_tmp_18_1_4_reg_2481.read());
}

void IsLocalExtremum::thread_tmp_22_fu_267_p2() {
    tmp_22_fu_267_p2 = (!p_0_2_cast_fu_242_p1.read().is_01() || !Window_0_1_V_read_1_reg_2131.read().is_01())? sc_lv<1>(): sc_lv<1>(p_0_2_cast_fu_242_p1.read() != Window_0_1_V_read_1_reg_2131.read());
}

void IsLocalExtremum::thread_tmp_23_fu_755_p2() {
    tmp_23_fu_755_p2 = (tmp_17_1_4_reg_2455.read() | tmp_21_fu_750_p2.read());
}

void IsLocalExtremum::thread_tmp_24_fu_764_p2() {
    tmp_24_fu_764_p2 = (tmp_23_fu_755_p2.read() & not_tmp_18_2_fu_760_p2.read());
}

void IsLocalExtremum::thread_tmp_25_fu_770_p2() {
    tmp_25_fu_770_p2 = (tmp_17_2_fu_726_p2.read() | tmp_24_fu_764_p2.read());
}

void IsLocalExtremum::thread_tmp_26_fu_930_p2() {
    tmp_26_fu_930_p2 = (ap_pipeline_reg_pp0_iter12_tmp_25_reg_2515.read() & ap_pipeline_reg_pp0_iter12_not_tmp_18_2_1_reg_2551.read());
}

void IsLocalExtremum::thread_tmp_27_fu_934_p2() {
    tmp_27_fu_934_p2 = (ap_pipeline_reg_pp0_iter12_tmp_17_2_1_reg_2525.read() | tmp_26_fu_930_p2.read());
}

void IsLocalExtremum::thread_tmp_28_fu_939_p2() {
    tmp_28_fu_939_p2 = (tmp_27_fu_934_p2.read() & not_tmp_18_2_2_reg_2589.read());
}

void IsLocalExtremum::thread_tmp_29_fu_944_p2() {
    tmp_29_fu_944_p2 = (tmp_17_2_2_reg_2561.read() | tmp_28_fu_939_p2.read());
}

void IsLocalExtremum::thread_tmp_2_fu_1664_p2() {
    tmp_2_fu_1664_p2 = (tmp_17_4_4_fu_1642_p2.read() | tmp_52_fu_1658_p2.read());
}

void IsLocalExtremum::thread_tmp_30_fu_953_p2() {
    tmp_30_fu_953_p2 = (tmp_29_fu_944_p2.read() & not_tmp_18_2_3_fu_949_p2.read());
}

void IsLocalExtremum::thread_tmp_31_fu_1084_p2() {
    tmp_31_fu_1084_p2 = (ap_pipeline_reg_pp0_iter14_tmp_17_2_3_reg_2599.read() | ap_pipeline_reg_pp0_iter14_tmp_30_reg_2635.read());
}

void IsLocalExtremum::thread_tmp_32_fu_1088_p2() {
    tmp_32_fu_1088_p2 = (tmp_31_fu_1084_p2.read() & not_tmp_18_2_4_reg_2683.read());
}

void IsLocalExtremum::thread_tmp_33_fu_1093_p2() {
    tmp_33_fu_1093_p2 = (tmp_17_2_4_reg_2655.read() | tmp_32_fu_1088_p2.read());
}

void IsLocalExtremum::thread_tmp_34_fu_1102_p2() {
    tmp_34_fu_1102_p2 = (tmp_33_fu_1093_p2.read() & not_tmp_18_3_fu_1098_p2.read());
}

void IsLocalExtremum::thread_tmp_35_fu_1108_p2() {
    tmp_35_fu_1108_p2 = (tmp_17_3_fu_1064_p2.read() | tmp_34_fu_1102_p2.read());
}

void IsLocalExtremum::thread_tmp_36_fu_1280_p2() {
    tmp_36_fu_1280_p2 = (ap_pipeline_reg_pp0_iter17_tmp_35_reg_2717.read() & ap_pipeline_reg_pp0_iter17_not_tmp_18_3_1_reg_2753.read());
}

void IsLocalExtremum::thread_tmp_37_fu_1284_p2() {
    tmp_37_fu_1284_p2 = (ap_pipeline_reg_pp0_iter17_tmp_17_3_1_reg_2727.read() | tmp_36_fu_1280_p2.read());
}

void IsLocalExtremum::thread_tmp_38_fu_1289_p2() {
    tmp_38_fu_1289_p2 = (tmp_37_fu_1284_p2.read() & not_tmp_18_3_2_reg_2796.read());
}

void IsLocalExtremum::thread_tmp_39_fu_1294_p2() {
    tmp_39_fu_1294_p2 = (tmp_17_3_2_reg_2763.read() | tmp_38_fu_1289_p2.read());
}

void IsLocalExtremum::thread_tmp_3_fu_1211_p2() {
    tmp_3_fu_1211_p2 = (tmp19_fu_1206_p2.read() | ap_pipeline_reg_pp0_iter16_tmp191_demorgan_reg_2625.read());
}

void IsLocalExtremum::thread_tmp_40_fu_1303_p2() {
    tmp_40_fu_1303_p2 = (tmp_39_fu_1294_p2.read() & not_tmp_18_3_3_fu_1299_p2.read());
}

void IsLocalExtremum::thread_tmp_41_fu_1448_p2() {
    tmp_41_fu_1448_p2 = (ap_pipeline_reg_pp0_iter19_tmp_17_3_3_reg_2821.read() | ap_pipeline_reg_pp0_iter19_tmp_40_reg_2847.read());
}

void IsLocalExtremum::thread_tmp_42_fu_1452_p2() {
    tmp_42_fu_1452_p2 = (tmp_41_fu_1448_p2.read() & not_tmp_18_3_4_reg_2893.read());
}

void IsLocalExtremum::thread_tmp_43_fu_1457_p2() {
    tmp_43_fu_1457_p2 = (tmp_17_3_4_reg_2857.read() | tmp_42_fu_1452_p2.read());
}

void IsLocalExtremum::thread_tmp_44_fu_1466_p2() {
    tmp_44_fu_1466_p2 = (tmp_43_fu_1457_p2.read() & not_tmp_18_4_fu_1462_p2.read());
}

void IsLocalExtremum::thread_tmp_45_fu_1472_p2() {
    tmp_45_fu_1472_p2 = (tmp_17_4_fu_1428_p2.read() | tmp_44_fu_1466_p2.read());
}

void IsLocalExtremum::thread_tmp_46_fu_1584_p2() {
    tmp_46_fu_1584_p2 = (ap_pipeline_reg_pp0_iter22_tmp_45_reg_2937.read() & ap_pipeline_reg_pp0_iter22_not_tmp_18_4_1_reg_2973.read());
}

void IsLocalExtremum::thread_tmp_47_fu_1588_p2() {
    tmp_47_fu_1588_p2 = (ap_pipeline_reg_pp0_iter22_tmp_17_4_1_reg_2947.read() | tmp_46_fu_1584_p2.read());
}

void IsLocalExtremum::thread_tmp_48_fu_1593_p2() {
    tmp_48_fu_1593_p2 = (tmp_47_fu_1588_p2.read() & not_tmp_18_4_2_reg_3011.read());
}

void IsLocalExtremum::thread_tmp_49_fu_1598_p2() {
    tmp_49_fu_1598_p2 = (tmp_17_4_2_reg_2983.read() | tmp_48_fu_1593_p2.read());
}

void IsLocalExtremum::thread_tmp_4_fu_324_p2() {
    tmp_4_fu_324_p2 = (ap_pipeline_reg_pp0_iter2_tmp_s_reg_2141.read() & ap_pipeline_reg_pp0_iter2_tmp_22_reg_2187.read());
}

void IsLocalExtremum::thread_tmp_50_fu_1607_p2() {
    tmp_50_fu_1607_p2 = (tmp_49_fu_1598_p2.read() & not_tmp_18_4_3_fu_1603_p2.read());
}

void IsLocalExtremum::thread_tmp_51_fu_1650_p2() {
    tmp_51_fu_1650_p2 = (tmp_17_4_3_reg_3021.read() | tmp_50_reg_3045.read());
}

void IsLocalExtremum::thread_tmp_52_fu_1658_p2() {
    tmp_52_fu_1658_p2 = (tmp_51_fu_1650_p2.read() & not_tmp_18_4_4_fu_1654_p2.read());
}

void IsLocalExtremum::thread_tmp_53_fu_604_p2() {
    tmp_53_fu_604_p2 = (tmp30_fu_584_p2.read() & tmp31_fu_598_p2.read());
}

void IsLocalExtremum::thread_tmp_54_fu_610_p2() {
    tmp_54_fu_610_p2 = (tmp_19_1_2_fu_528_p2.read() | tmp_53_fu_604_p2.read());
}

void IsLocalExtremum::thread_tmp_55_fu_984_p2() {
    tmp_55_fu_984_p2 = (tmp32_fu_964_p2.read() & tmp33_fu_978_p2.read());
}

void IsLocalExtremum::thread_tmp_56_fu_990_p2() {
    tmp_56_fu_990_p2 = (tmp_19_2_2_reg_2575.read() | tmp_55_fu_984_p2.read());
}

void IsLocalExtremum::thread_tmp_57_fu_1403_p2() {
    tmp_57_fu_1403_p2 = (tmp36_fu_1398_p2.read() & ap_pipeline_reg_pp0_iter18_tmp34_reg_2645.read());
}

void IsLocalExtremum::thread_tmp_58_fu_1408_p2() {
    tmp_58_fu_1408_p2 = (ap_pipeline_reg_pp0_iter18_tmp_19_3_2_reg_2777.read() | tmp_57_fu_1403_p2.read());
}

void IsLocalExtremum::thread_tmp_59_fu_1795_p2() {
    tmp_59_fu_1795_p2 = (tmp39_fu_1790_p2.read() & ap_pipeline_reg_pp0_iter24_tmp37_reg_2903.read());
}

void IsLocalExtremum::thread_tmp_5_fu_1699_p2() {
    tmp_5_fu_1699_p2 = (tmp_19_4_4_fu_1646_p2.read() | tmp_109_fu_1693_p2.read());
}

void IsLocalExtremum::thread_tmp_60_fu_1800_p2() {
    tmp_60_fu_1800_p2 = (ap_pipeline_reg_pp0_iter24_tmp_19_4_2_reg_2997.read() | tmp_59_fu_1795_p2.read());
}

void IsLocalExtremum::thread_tmp_61_fu_1245_p2() {
    tmp_61_fu_1245_p2 = (tmp41_fu_1240_p2.read() | ap_pipeline_reg_pp0_iter16_tmp217_demorgan_reg_2640.read());
}

void IsLocalExtremum::thread_tmp_62_fu_272_p2() {
    tmp_62_fu_272_p2 = (!p_038_2_reg_2156.read().is_01() || !Window_0_1_V_read_1_reg_2131.read().is_01())? sc_lv<1>(): sc_lv<1>(p_038_2_reg_2156.read() != Window_0_1_V_read_1_reg_2131.read());
}

void IsLocalExtremum::thread_tmp_63_fu_353_p2() {
    tmp_63_fu_353_p2 = (ap_pipeline_reg_pp0_iter2_tmp_1_reg_2151.read() & ap_pipeline_reg_pp0_iter2_tmp_62_reg_2192.read());
}

void IsLocalExtremum::thread_tmp_64_fu_357_p2() {
    tmp_64_fu_357_p2 = (ap_pipeline_reg_pp0_iter2_tmp_19_0_1_reg_2175.read() | tmp_63_fu_353_p2.read());
}

void IsLocalExtremum::thread_tmp_65_fu_362_p2() {
    tmp_65_fu_362_p2 = (tmp_64_fu_357_p2.read() & not_tmp_20_0_2_reg_2228.read());
}

void IsLocalExtremum::thread_tmp_66_fu_367_p2() {
    tmp_66_fu_367_p2 = (tmp_19_0_2_reg_2210.read() | tmp_65_fu_362_p2.read());
}

void IsLocalExtremum::thread_tmp_67_fu_376_p2() {
    tmp_67_fu_376_p2 = (tmp_66_fu_367_p2.read() & not_tmp_20_0_3_fu_372_p2.read());
}

void IsLocalExtremum::thread_tmp_68_fu_460_p2() {
    tmp_68_fu_460_p2 = (ap_pipeline_reg_pp0_iter4_tmp_19_0_3_reg_2246.read() | ap_pipeline_reg_pp0_iter4_tmp_67_reg_2264.read());
}

void IsLocalExtremum::thread_tmp_69_fu_464_p2() {
    tmp_69_fu_464_p2 = (tmp_68_fu_460_p2.read() & not_tmp_20_0_4_reg_2300.read());
}

void IsLocalExtremum::thread_tmp_6_fu_328_p2() {
    tmp_6_fu_328_p2 = (ap_pipeline_reg_pp0_iter2_tmp_17_0_1_reg_2163.read() | tmp_4_fu_324_p2.read());
}

void IsLocalExtremum::thread_tmp_70_fu_469_p2() {
    tmp_70_fu_469_p2 = (tmp_19_0_4_reg_2282.read() | tmp_69_fu_464_p2.read());
}

void IsLocalExtremum::thread_tmp_71_fu_478_p2() {
    tmp_71_fu_478_p2 = (tmp_70_fu_469_p2.read() & not_tmp_20_1_fu_474_p2.read());
}

void IsLocalExtremum::thread_tmp_72_fu_484_p2() {
    tmp_72_fu_484_p2 = (tmp_19_1_fu_420_p2.read() | tmp_71_fu_478_p2.read());
}

void IsLocalExtremum::thread_tmp_73_fu_669_p2() {
    tmp_73_fu_669_p2 = (ap_pipeline_reg_pp0_iter7_tmp_72_reg_2334.read() & ap_pipeline_reg_pp0_iter7_not_tmp_20_1_1_reg_2370.read());
}

void IsLocalExtremum::thread_tmp_74_fu_673_p2() {
    tmp_74_fu_673_p2 = (ap_pipeline_reg_pp0_iter7_tmp_19_1_1_reg_2352.read() | tmp_73_fu_669_p2.read());
}

void IsLocalExtremum::thread_tmp_75_fu_678_p2() {
    tmp_75_fu_678_p2 = (tmp_74_fu_673_p2.read() & not_tmp_20_1_2_reg_2414.read());
}

void IsLocalExtremum::thread_tmp_76_fu_683_p2() {
    tmp_76_fu_683_p2 = (tmp_19_1_2_reg_2387.read() | tmp_75_fu_678_p2.read());
}

void IsLocalExtremum::thread_tmp_77_fu_692_p2() {
    tmp_77_fu_692_p2 = (tmp_76_fu_683_p2.read() & not_tmp_20_1_3_fu_688_p2.read());
}

void IsLocalExtremum::thread_tmp_78_fu_776_p2() {
    tmp_78_fu_776_p2 = (ap_pipeline_reg_pp0_iter9_tmp_19_1_3_reg_2432.read() | ap_pipeline_reg_pp0_iter9_tmp_77_reg_2450.read());
}

void IsLocalExtremum::thread_tmp_79_fu_780_p2() {
    tmp_79_fu_780_p2 = (tmp_78_fu_776_p2.read() & not_tmp_20_1_4_reg_2486.read());
}

void IsLocalExtremum::thread_tmp_7_fu_333_p2() {
    tmp_7_fu_333_p2 = (tmp_6_fu_328_p2.read() & not_tmp_18_0_2_reg_2223.read());
}

void IsLocalExtremum::thread_tmp_80_fu_785_p2() {
    tmp_80_fu_785_p2 = (tmp_19_1_4_reg_2468.read() | tmp_79_fu_780_p2.read());
}

void IsLocalExtremum::thread_tmp_81_fu_794_p2() {
    tmp_81_fu_794_p2 = (tmp_80_fu_785_p2.read() & not_tmp_20_2_fu_790_p2.read());
}

void IsLocalExtremum::thread_tmp_82_fu_800_p2() {
    tmp_82_fu_800_p2 = (tmp_19_2_fu_736_p2.read() | tmp_81_fu_794_p2.read());
}

void IsLocalExtremum::thread_tmp_83_fu_1007_p2() {
    tmp_83_fu_1007_p2 = (ap_pipeline_reg_pp0_iter12_tmp_82_reg_2520.read() & ap_pipeline_reg_pp0_iter12_not_tmp_20_2_1_reg_2556.read());
}

void IsLocalExtremum::thread_tmp_84_fu_1011_p2() {
    tmp_84_fu_1011_p2 = (ap_pipeline_reg_pp0_iter12_tmp_19_2_1_reg_2538.read() | tmp_83_fu_1007_p2.read());
}

void IsLocalExtremum::thread_tmp_85_fu_1016_p2() {
    tmp_85_fu_1016_p2 = (tmp_84_fu_1011_p2.read() & not_tmp_20_2_2_reg_2594.read());
}

void IsLocalExtremum::thread_tmp_86_fu_1021_p2() {
    tmp_86_fu_1021_p2 = (tmp_19_2_2_reg_2575.read() | tmp_85_fu_1016_p2.read());
}

void IsLocalExtremum::thread_tmp_87_fu_1030_p2() {
    tmp_87_fu_1030_p2 = (tmp_86_fu_1021_p2.read() & not_tmp_20_2_3_fu_1026_p2.read());
}

void IsLocalExtremum::thread_tmp_88_fu_1114_p2() {
    tmp_88_fu_1114_p2 = (ap_pipeline_reg_pp0_iter14_tmp_19_2_3_reg_2612.read() | ap_pipeline_reg_pp0_iter14_tmp_87_reg_2650.read());
}

void IsLocalExtremum::thread_tmp_89_fu_1118_p2() {
    tmp_89_fu_1118_p2 = (tmp_88_fu_1114_p2.read() & not_tmp_20_2_4_reg_2688.read());
}

void IsLocalExtremum::thread_tmp_8_fu_338_p2() {
    tmp_8_fu_338_p2 = (tmp_17_0_2_reg_2197.read() | tmp_7_fu_333_p2.read());
}

void IsLocalExtremum::thread_tmp_90_fu_1123_p2() {
    tmp_90_fu_1123_p2 = (tmp_19_2_4_reg_2669.read() | tmp_89_fu_1118_p2.read());
}

void IsLocalExtremum::thread_tmp_91_fu_1132_p2() {
    tmp_91_fu_1132_p2 = (tmp_90_fu_1123_p2.read() & not_tmp_20_3_fu_1128_p2.read());
}

void IsLocalExtremum::thread_tmp_92_fu_1138_p2() {
    tmp_92_fu_1138_p2 = (tmp_19_3_fu_1074_p2.read() | tmp_91_fu_1132_p2.read());
}

void IsLocalExtremum::thread_tmp_93_fu_1309_p2() {
    tmp_93_fu_1309_p2 = (ap_pipeline_reg_pp0_iter17_tmp_92_reg_2722.read() & ap_pipeline_reg_pp0_iter17_not_tmp_20_3_1_reg_2758.read());
}

void IsLocalExtremum::thread_tmp_94_fu_1313_p2() {
    tmp_94_fu_1313_p2 = (ap_pipeline_reg_pp0_iter17_tmp_19_3_1_reg_2740.read() | tmp_93_fu_1309_p2.read());
}

void IsLocalExtremum::thread_tmp_95_fu_1318_p2() {
    tmp_95_fu_1318_p2 = (tmp_94_fu_1313_p2.read() & not_tmp_20_3_2_reg_2811.read());
}

void IsLocalExtremum::thread_tmp_96_fu_1323_p2() {
    tmp_96_fu_1323_p2 = (tmp_19_3_2_reg_2777.read() | tmp_95_fu_1318_p2.read());
}

void IsLocalExtremum::thread_tmp_97_fu_1332_p2() {
    tmp_97_fu_1332_p2 = (tmp_96_fu_1323_p2.read() & not_tmp_20_3_3_fu_1328_p2.read());
}

void IsLocalExtremum::thread_tmp_98_fu_1478_p2() {
    tmp_98_fu_1478_p2 = (ap_pipeline_reg_pp0_iter19_tmp_19_3_3_reg_2834.read() | ap_pipeline_reg_pp0_iter19_tmp_97_reg_2852.read());
}

void IsLocalExtremum::thread_tmp_99_fu_1482_p2() {
    tmp_99_fu_1482_p2 = (tmp_98_fu_1478_p2.read() & not_tmp_20_3_4_reg_2908.read());
}

void IsLocalExtremum::thread_tmp_9_fu_347_p2() {
    tmp_9_fu_347_p2 = (tmp_8_fu_338_p2.read() & not_tmp_18_0_3_fu_343_p2.read());
}

void IsLocalExtremum::thread_tmp_fu_543_p2() {
    tmp_fu_543_p2 = (ap_pipeline_reg_pp0_iter6_tmp_17_0_2_reg_2197.read() & not_tmp_17_0_3_fu_538_p2.read());
}

void IsLocalExtremum::thread_tmp_s_fu_210_p2() {
    tmp_s_fu_210_p2 = (!Window_0_0_V_read.read().is_01() || !ap_const_lv16_F00.is_01())? sc_lv<1>(): (sc_bigint<16>(Window_0_0_V_read.read()) > sc_bigint<16>(ap_const_lv16_F00));
}

}

