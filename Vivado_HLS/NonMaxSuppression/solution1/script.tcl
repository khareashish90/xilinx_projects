############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project NonMaxSuppression
set_top SuppressNonMax
add_files NonMaxSuppression/SuppressNonMax.cpp
add_files NonMaxSuppression/SuppressNonMax.hpp
add_files -tb NonMaxSuppression/SuppressNonMax_TB.cpp
add_files -tb NonMaxSuppression/SuppressNonMax_TB.hpp
add_files -tb NonMaxSuppression/TestImage.h
open_solution "solution1"
set_part {xc7z030sbg485-1} -tool vivado
create_clock -period 5 -name default
#source "./NonMaxSuppression/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -flow syn -rtl verilog -format ip_catalog -vendor "Trimble"
