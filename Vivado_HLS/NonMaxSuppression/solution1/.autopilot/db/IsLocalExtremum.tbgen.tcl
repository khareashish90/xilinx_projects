set moduleName IsLocalExtremum
set isCombinational 0
set isDatapathOnly 1
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set C_modelName {IsLocalExtremum}
set C_modelType { int 1 }
set C_modelArgList {
	{ Window_0_0_V_read int 16 regular  }
	{ Window_0_1_V_read int 16 regular  }
	{ Window_0_2_V_read int 16 regular  }
	{ Window_0_3_V_read int 16 regular  }
	{ Window_0_4_V_read int 16 regular  }
	{ Window_1_0_V_read int 16 regular  }
	{ Window_1_1_V_read int 16 regular  }
	{ Window_1_2_V_read int 16 regular  }
	{ Window_1_3_V_read int 16 regular  }
	{ Window_1_4_V_read int 16 regular  }
	{ Window_2_0_V_read int 16 regular  }
	{ Window_2_1_V_read int 16 regular  }
	{ Window_2_2_V_read int 16 regular  }
	{ Window_2_3_V_read int 16 regular  }
	{ Window_2_4_V_read int 16 regular  }
	{ Window_3_0_V_read int 16 regular  }
	{ Window_3_1_V_read int 16 regular  }
	{ Window_3_2_V_read int 16 regular  }
	{ Window_3_3_V_read int 16 regular  }
	{ Window_3_4_V_read int 16 regular  }
	{ Window_4_0_V_read int 16 regular  }
	{ Window_4_1_V_read int 16 regular  }
	{ Window_4_2_V_read int 16 regular  }
	{ Window_4_3_V_read int 16 regular  }
	{ Window_4_4_V_read int 16 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "Window_0_0_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_0_1_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_0_2_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_0_3_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_0_4_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_1_0_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_1_1_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_1_2_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_1_3_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_1_4_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_2_0_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_2_1_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_2_2_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_2_3_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_2_4_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_3_0_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_3_1_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_3_2_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_3_3_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_3_4_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_4_0_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_4_1_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_4_2_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_4_3_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "Window_4_4_V_read", "interface" : "wire", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "ap_return", "interface" : "wire", "bitwidth" : 1} ]}
# RTL Port declarations: 
set portNum 29
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ Window_0_0_V_read sc_in sc_lv 16 signal 0 } 
	{ Window_0_1_V_read sc_in sc_lv 16 signal 1 } 
	{ Window_0_2_V_read sc_in sc_lv 16 signal 2 } 
	{ Window_0_3_V_read sc_in sc_lv 16 signal 3 } 
	{ Window_0_4_V_read sc_in sc_lv 16 signal 4 } 
	{ Window_1_0_V_read sc_in sc_lv 16 signal 5 } 
	{ Window_1_1_V_read sc_in sc_lv 16 signal 6 } 
	{ Window_1_2_V_read sc_in sc_lv 16 signal 7 } 
	{ Window_1_3_V_read sc_in sc_lv 16 signal 8 } 
	{ Window_1_4_V_read sc_in sc_lv 16 signal 9 } 
	{ Window_2_0_V_read sc_in sc_lv 16 signal 10 } 
	{ Window_2_1_V_read sc_in sc_lv 16 signal 11 } 
	{ Window_2_2_V_read sc_in sc_lv 16 signal 12 } 
	{ Window_2_3_V_read sc_in sc_lv 16 signal 13 } 
	{ Window_2_4_V_read sc_in sc_lv 16 signal 14 } 
	{ Window_3_0_V_read sc_in sc_lv 16 signal 15 } 
	{ Window_3_1_V_read sc_in sc_lv 16 signal 16 } 
	{ Window_3_2_V_read sc_in sc_lv 16 signal 17 } 
	{ Window_3_3_V_read sc_in sc_lv 16 signal 18 } 
	{ Window_3_4_V_read sc_in sc_lv 16 signal 19 } 
	{ Window_4_0_V_read sc_in sc_lv 16 signal 20 } 
	{ Window_4_1_V_read sc_in sc_lv 16 signal 21 } 
	{ Window_4_2_V_read sc_in sc_lv 16 signal 22 } 
	{ Window_4_3_V_read sc_in sc_lv 16 signal 23 } 
	{ Window_4_4_V_read sc_in sc_lv 16 signal 24 } 
	{ ap_return sc_out sc_lv 1 signal -1 } 
	{ ap_ce sc_in sc_logic 1 ce -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "Window_0_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_0_0_V_read", "role": "default" }} , 
 	{ "name": "Window_0_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_0_1_V_read", "role": "default" }} , 
 	{ "name": "Window_0_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_0_2_V_read", "role": "default" }} , 
 	{ "name": "Window_0_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_0_3_V_read", "role": "default" }} , 
 	{ "name": "Window_0_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_0_4_V_read", "role": "default" }} , 
 	{ "name": "Window_1_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_1_0_V_read", "role": "default" }} , 
 	{ "name": "Window_1_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_1_1_V_read", "role": "default" }} , 
 	{ "name": "Window_1_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_1_2_V_read", "role": "default" }} , 
 	{ "name": "Window_1_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_1_3_V_read", "role": "default" }} , 
 	{ "name": "Window_1_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_1_4_V_read", "role": "default" }} , 
 	{ "name": "Window_2_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_2_0_V_read", "role": "default" }} , 
 	{ "name": "Window_2_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_2_1_V_read", "role": "default" }} , 
 	{ "name": "Window_2_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_2_2_V_read", "role": "default" }} , 
 	{ "name": "Window_2_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_2_3_V_read", "role": "default" }} , 
 	{ "name": "Window_2_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_2_4_V_read", "role": "default" }} , 
 	{ "name": "Window_3_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_3_0_V_read", "role": "default" }} , 
 	{ "name": "Window_3_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_3_1_V_read", "role": "default" }} , 
 	{ "name": "Window_3_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_3_2_V_read", "role": "default" }} , 
 	{ "name": "Window_3_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_3_3_V_read", "role": "default" }} , 
 	{ "name": "Window_3_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_3_4_V_read", "role": "default" }} , 
 	{ "name": "Window_4_0_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_4_0_V_read", "role": "default" }} , 
 	{ "name": "Window_4_1_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_4_1_V_read", "role": "default" }} , 
 	{ "name": "Window_4_2_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_4_2_V_read", "role": "default" }} , 
 	{ "name": "Window_4_3_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_4_3_V_read", "role": "default" }} , 
 	{ "name": "Window_4_4_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "Window_4_4_V_read", "role": "default" }} , 
 	{ "name": "ap_return", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "ap_return", "role": "default" }} , 
 	{ "name": "ap_ce", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "ce", "bundle":{"name": "ap_ce", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "IsLocalExtremum",
		"VariableLatency" : "0",
		"AlignedPipeline" : "1",
		"UnalignedPipeline" : "0",
		"ProcessNetwork" : "0",
		"Combinational" : "0",
		"ControlExist" : "0",
		"Port" : [
		{"Name" : "Window_0_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_4_V_read", "Type" : "None", "Direction" : "I"}]}]}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "26", "Max" : "26"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set Spec2ImplPortList { 
	Window_0_0_V_read { ap_none {  { Window_0_0_V_read in_data 0 16 } } }
	Window_0_1_V_read { ap_none {  { Window_0_1_V_read in_data 0 16 } } }
	Window_0_2_V_read { ap_none {  { Window_0_2_V_read in_data 0 16 } } }
	Window_0_3_V_read { ap_none {  { Window_0_3_V_read in_data 0 16 } } }
	Window_0_4_V_read { ap_none {  { Window_0_4_V_read in_data 0 16 } } }
	Window_1_0_V_read { ap_none {  { Window_1_0_V_read in_data 0 16 } } }
	Window_1_1_V_read { ap_none {  { Window_1_1_V_read in_data 0 16 } } }
	Window_1_2_V_read { ap_none {  { Window_1_2_V_read in_data 0 16 } } }
	Window_1_3_V_read { ap_none {  { Window_1_3_V_read in_data 0 16 } } }
	Window_1_4_V_read { ap_none {  { Window_1_4_V_read in_data 0 16 } } }
	Window_2_0_V_read { ap_none {  { Window_2_0_V_read in_data 0 16 } } }
	Window_2_1_V_read { ap_none {  { Window_2_1_V_read in_data 0 16 } } }
	Window_2_2_V_read { ap_none {  { Window_2_2_V_read in_data 0 16 } } }
	Window_2_3_V_read { ap_none {  { Window_2_3_V_read in_data 0 16 } } }
	Window_2_4_V_read { ap_none {  { Window_2_4_V_read in_data 0 16 } } }
	Window_3_0_V_read { ap_none {  { Window_3_0_V_read in_data 0 16 } } }
	Window_3_1_V_read { ap_none {  { Window_3_1_V_read in_data 0 16 } } }
	Window_3_2_V_read { ap_none {  { Window_3_2_V_read in_data 0 16 } } }
	Window_3_3_V_read { ap_none {  { Window_3_3_V_read in_data 0 16 } } }
	Window_3_4_V_read { ap_none {  { Window_3_4_V_read in_data 0 16 } } }
	Window_4_0_V_read { ap_none {  { Window_4_0_V_read in_data 0 16 } } }
	Window_4_1_V_read { ap_none {  { Window_4_1_V_read in_data 0 16 } } }
	Window_4_2_V_read { ap_none {  { Window_4_2_V_read in_data 0 16 } } }
	Window_4_3_V_read { ap_none {  { Window_4_3_V_read in_data 0 16 } } }
	Window_4_4_V_read { ap_none {  { Window_4_4_V_read in_data 0 16 } } }
}
