set C_TypeInfoList {{ 
"SuppressNonMax" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"responseStream": [[], {"reference": "0"}] }, {"keypointStream": [[], {"reference": "1"}] }, {"nKPCount": [[],{ "pointer":  {"scalar": "int"}}] }],[],""], 
"1": [ "stream<ap_axiu<32, 1, 1, 1> >", {"hls_type": {"stream": [[[[],"2"]],"3"]}}], 
"2": [ "ap_axiu<32, 1, 1, 1>", {"struct": [[],[{"D":[[], {"scalar": { "int": 32}}]},{"U":[[], {"scalar": { "int": 1}}]},{"TI":[[], {"scalar": { "int": 1}}]},{"TD":[[], {"scalar": { "int": 1}}]}],[{ "data": [[], "4"]},{ "keep": [[], "5"]},{ "strb": [[], "5"]},{ "user": [[], "6"]},{ "last": [[], "6"]},{ "id": [[], "6"]},{ "dest": [[], "6"]}],""]}], 
"4": [ "ap_uint<32>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 32}}]],""]}}], 
"6": [ "ap_uint<1>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 1}}]],""]}}], 
"0": [ "stream<Response_And_Size>", {"hls_type": {"stream": [[[[],"7"]],"3"]}}], 
"7": [ "Response_And_Size", {"struct": [[],[],[{ "resp": [[16], "8"]},{ "size": [[16], "9"]}],""]}], 
"5": [ "ap_uint<4>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 4}}]],""]}}], 
"9": [ "Index_t", {"typedef": [[[],"10"],""]}], 
"10": [ "ap_uint<12>", {"hls_type": {"ap_uint": [[[[], {"scalar": { "int": 12}}]],""]}}], 
"8": [ "Response_t", {"typedef": [[[],"11"],""]}], 
"11": [ "ap_fixed<16, 9, 1, 3, 0>", {"hls_type": {"ap_fixed": [[[[], {"scalar": { "int": 16}}],[[], {"scalar": { "int": 9}}],[[], {"scalar": { "12": 1}}],[[], {"scalar": { "13": 3}}],[[], {"scalar": { "int": 0}}]],""]}}], 
"13": [ "ap_o_mode", {"enum": [[],[],[{"SC_SAT":  {"scalar": "__integer__"}},{"SC_SAT_ZERO":  {"scalar": "__integer__"}},{"SC_SAT_SYM":  {"scalar": "__integer__"}},{"SC_WRAP":  {"scalar": "__integer__"}},{"SC_WRAP_SM":  {"scalar": "__integer__"}}],""]}], 
"12": [ "ap_q_mode", {"enum": [[],[],[{"SC_RND":  {"scalar": "__integer__"}},{"SC_RND_ZERO":  {"scalar": "__integer__"}},{"SC_RND_MIN_INF":  {"scalar": "__integer__"}},{"SC_RND_INF":  {"scalar": "__integer__"}},{"SC_RND_CONV":  {"scalar": "__integer__"}},{"SC_TRN":  {"scalar": "__integer__"}},{"SC_TRN_ZERO":  {"scalar": "__integer__"}}],""]}],
"3": ["hls", ""]
}}
set moduleName SuppressNonMax
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set C_modelName {SuppressNonMax}
set C_modelType { void 0 }
set C_modelArgList {
	{ responseStream_V_resp_V int 16 regular {axi_s 0 volatile  { responseStream_V_resp_V Data } }  }
	{ responseStream_V_size_V int 16 regular {axi_s 0 volatile  { responseStream_V_size_V Data } }  }
	{ keypointStream_V_data_V int 32 regular {axi_s 1 volatile  { keypointStream Data } }  }
	{ keypointStream_V_keep_V int 4 regular {axi_s 1 volatile  { keypointStream Keep } }  }
	{ keypointStream_V_strb_V int 4 regular {axi_s 1 volatile  { keypointStream Strb } }  }
	{ keypointStream_V_user_V int 1 regular {axi_s 1 volatile  { keypointStream User } }  }
	{ keypointStream_V_last_V int 1 regular {axi_s 1 volatile  { keypointStream Last } }  }
	{ keypointStream_V_id_V int 1 regular {axi_s 1 volatile  { keypointStream ID } }  }
	{ keypointStream_V_dest_V int 1 regular {axi_s 1 volatile  { keypointStream Dest } }  }
	{ nKPCount int 32 regular {pointer 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "responseStream_V_resp_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "responseStream.V.resp.V","cData": "int16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "responseStream_V_size_V", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":11,"cElement": [{"cName": "responseStream.V.size.V","cData": "uint12","bit_use": { "low": 0,"up": 11},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_data_V", "interface" : "axis", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "keypointStream.V.data.V","cData": "uint32","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_keep_V", "interface" : "axis", "bitwidth" : 4, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":3,"cElement": [{"cName": "keypointStream.V.keep.V","cData": "uint4","bit_use": { "low": 0,"up": 3},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_strb_V", "interface" : "axis", "bitwidth" : 4, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":3,"cElement": [{"cName": "keypointStream.V.strb.V","cData": "uint4","bit_use": { "low": 0,"up": 3},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_user_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "keypointStream.V.user.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_last_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "keypointStream.V.last.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_id_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "keypointStream.V.id.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "keypointStream_V_dest_V", "interface" : "axis", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "keypointStream.V.dest.V","cData": "uint1","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "nKPCount", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "nKPCount","cData": "int","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 36
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst_n sc_in sc_logic 1 reset -1 active_low_sync } 
	{ responseStream_V_resp_V_TDATA sc_in sc_lv 16 signal 0 } 
	{ responseStream_V_resp_V_TVALID sc_in sc_logic 1 invld 0 } 
	{ responseStream_V_resp_V_TREADY sc_out sc_logic 1 inacc 0 } 
	{ responseStream_V_size_V_TDATA sc_in sc_lv 16 signal 1 } 
	{ responseStream_V_size_V_TVALID sc_in sc_logic 1 invld 1 } 
	{ responseStream_V_size_V_TREADY sc_out sc_logic 1 inacc 1 } 
	{ keypointStream_TDATA sc_out sc_lv 32 signal 2 } 
	{ keypointStream_TVALID sc_out sc_logic 1 outvld 8 } 
	{ keypointStream_TREADY sc_in sc_logic 1 outacc 8 } 
	{ keypointStream_TKEEP sc_out sc_lv 4 signal 3 } 
	{ keypointStream_TSTRB sc_out sc_lv 4 signal 4 } 
	{ keypointStream_TUSER sc_out sc_lv 1 signal 5 } 
	{ keypointStream_TLAST sc_out sc_lv 1 signal 6 } 
	{ keypointStream_TID sc_out sc_lv 1 signal 7 } 
	{ keypointStream_TDEST sc_out sc_lv 1 signal 8 } 
	{ nKPCount sc_out sc_lv 32 signal 9 } 
	{ s_axi_control_AWVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_AWREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_AWADDR sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_WVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_WREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_WDATA sc_in sc_lv 32 signal -1 } 
	{ s_axi_control_WSTRB sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_ARVALID sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_ARREADY sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_ARADDR sc_in sc_lv 4 signal -1 } 
	{ s_axi_control_RVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_RREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_RDATA sc_out sc_lv 32 signal -1 } 
	{ s_axi_control_RRESP sc_out sc_lv 2 signal -1 } 
	{ s_axi_control_BVALID sc_out sc_logic 1 signal -1 } 
	{ s_axi_control_BREADY sc_in sc_logic 1 signal -1 } 
	{ s_axi_control_BRESP sc_out sc_lv 2 signal -1 } 
	{ interrupt sc_out sc_logic 1 signal -1 } 
}
set NewPortList {[ 
	{ "name": "s_axi_control_AWADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "AWADDR" },"address":[{"name":"SuppressNonMax","role":"start","value":"0","valid_bit":"0"},{"name":"SuppressNonMax","role":"continue","value":"0","valid_bit":"4"},{"name":"SuppressNonMax","role":"auto_start","value":"0","valid_bit":"7"}] },
	{ "name": "s_axi_control_AWVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWVALID" } },
	{ "name": "s_axi_control_AWREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "AWREADY" } },
	{ "name": "s_axi_control_WVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WVALID" } },
	{ "name": "s_axi_control_WREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "WREADY" } },
	{ "name": "s_axi_control_WDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "WDATA" } },
	{ "name": "s_axi_control_WSTRB", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "WSTRB" } },
	{ "name": "s_axi_control_ARADDR", "direction": "in", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "control", "role": "ARADDR" },"address":[{"name":"SuppressNonMax","role":"start","value":"0","valid_bit":"0"},{"name":"SuppressNonMax","role":"done","value":"0","valid_bit":"1"},{"name":"SuppressNonMax","role":"idle","value":"0","valid_bit":"2"},{"name":"SuppressNonMax","role":"ready","value":"0","valid_bit":"3"},{"name":"SuppressNonMax","role":"auto_start","value":"0","valid_bit":"7"}] },
	{ "name": "s_axi_control_ARVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARVALID" } },
	{ "name": "s_axi_control_ARREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "ARREADY" } },
	{ "name": "s_axi_control_RVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RVALID" } },
	{ "name": "s_axi_control_RREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "RREADY" } },
	{ "name": "s_axi_control_RDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "control", "role": "RDATA" } },
	{ "name": "s_axi_control_RRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "RRESP" } },
	{ "name": "s_axi_control_BVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BVALID" } },
	{ "name": "s_axi_control_BREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "BREADY" } },
	{ "name": "s_axi_control_BRESP", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "control", "role": "BRESP" } },
	{ "name": "interrupt", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "control", "role": "interrupt" } }, 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst_n", "role": "default" }} , 
 	{ "name": "responseStream_V_resp_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "responseStream_V_resp_V", "role": "TDATA" }} , 
 	{ "name": "responseStream_V_resp_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "responseStream_V_resp_V", "role": "TVALID" }} , 
 	{ "name": "responseStream_V_resp_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "responseStream_V_resp_V", "role": "TREADY" }} , 
 	{ "name": "responseStream_V_size_V_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "responseStream_V_size_V", "role": "TDATA" }} , 
 	{ "name": "responseStream_V_size_V_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "responseStream_V_size_V", "role": "TVALID" }} , 
 	{ "name": "responseStream_V_size_V_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "responseStream_V_size_V", "role": "TREADY" }} , 
 	{ "name": "keypointStream_TDATA", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "keypointStream_V_data_V", "role": "default" }} , 
 	{ "name": "keypointStream_TVALID", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "keypointStream_V_dest_V", "role": "default" }} , 
 	{ "name": "keypointStream_TREADY", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "outacc", "bundle":{"name": "keypointStream_V_dest_V", "role": "default" }} , 
 	{ "name": "keypointStream_TKEEP", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "keypointStream_V_keep_V", "role": "default" }} , 
 	{ "name": "keypointStream_TSTRB", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "keypointStream_V_strb_V", "role": "default" }} , 
 	{ "name": "keypointStream_TUSER", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "keypointStream_V_user_V", "role": "default" }} , 
 	{ "name": "keypointStream_TLAST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "keypointStream_V_last_V", "role": "default" }} , 
 	{ "name": "keypointStream_TID", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "keypointStream_V_id_V", "role": "default" }} , 
 	{ "name": "keypointStream_TDEST", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "keypointStream_V_dest_V", "role": "default" }} , 
 	{ "name": "nKPCount", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "nKPCount", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6", "7"],
		"CDFG" : "SuppressNonMax",
		"VariableLatency" : "1",
		"AlignedPipeline" : "0",
		"UnalignedPipeline" : "0",
		"ProcessNetwork" : "0",
		"Combinational" : "0",
		"ControlExist" : "1",
		"Port" : [
		{"Name" : "responseStream_V_resp_V", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_resp_V_TDATA_blk_n", "Type" : "RtlSignal"}],
			"SubConnect" : [
			{"ID" : "7", "SubInstance" : "grp_ReadFromStream_fu_420", "Port" : "responseStream_V_res"}]},
		{"Name" : "responseStream_V_size_V", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_size_V_TDATA_blk_n", "Type" : "RtlSignal"}],
			"SubConnect" : [
			{"ID" : "7", "SubInstance" : "grp_ReadFromStream_fu_420", "Port" : "responseStream_V_siz"}]},
		{"Name" : "keypointStream_V_data_V", "Type" : "Axis", "Direction" : "O",
			"BlockSignal" : [
			{"Name" : "keypointStream_TDATA_blk_n", "Type" : "RtlSignal"}]},
		{"Name" : "keypointStream_V_keep_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "keypointStream_V_strb_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "keypointStream_V_user_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "keypointStream_V_last_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "keypointStream_V_id_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "keypointStream_V_dest_V", "Type" : "Axis", "Direction" : "O"},
		{"Name" : "nKPCount", "Type" : "None", "Direction" : "O"}],
		"SubInstanceBlock" : [
		{"SubInstance" : "grp_ReadFromStream_fu_420", "SubBlockPort" : ["responseStream_V_res_TDATA_blk_n", "responseStream_V_siz_TDATA_blk_n"]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.SuppressNonMax_control_s_axi_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ResponseBuf_1_V_U", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ResponseBuf_2_V_U", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ResponseBuf_3_V_U", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.ResponseBuf_4_V_U", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_IsLocalExtremum_fu_386", "Parent" : "0",
		"CDFG" : "IsLocalExtremum",
		"VariableLatency" : "0",
		"AlignedPipeline" : "1",
		"UnalignedPipeline" : "0",
		"ProcessNetwork" : "0",
		"Combinational" : "0",
		"ControlExist" : "0",
		"Port" : [
		{"Name" : "Window_0_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_0_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_1_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_2_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_3_4_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_0_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_1_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_2_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_3_V_read", "Type" : "None", "Direction" : "I"},
		{"Name" : "Window_4_4_V_read", "Type" : "None", "Direction" : "I"}]},
	{"ID" : "7", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_ReadFromStream_fu_420", "Parent" : "0",
		"CDFG" : "ReadFromStream",
		"VariableLatency" : "0",
		"AlignedPipeline" : "1",
		"UnalignedPipeline" : "0",
		"ProcessNetwork" : "0",
		"Combinational" : "0",
		"ControlExist" : "1",
		"Port" : [
		{"Name" : "responseStream_V_res", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_res_TDATA_blk_n", "Type" : "RtlPort"}]},
		{"Name" : "responseStream_V_siz", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_siz_TDATA_blk_n", "Type" : "RtlPort"}]},
		{"Name" : "iRow", "Type" : "None", "Direction" : "I"},
		{"Name" : "iCol", "Type" : "None", "Direction" : "I"}]}]}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "2115312", "Max" : "2115312"}
	, {"Name" : "Interval", "Min" : "2115313", "Max" : "2115313"}
]}

set Spec2ImplPortList { 
	responseStream_V_resp_V { axis {  { responseStream_V_resp_V_TDATA in_data 0 16 }  { responseStream_V_resp_V_TVALID in_vld 0 1 }  { responseStream_V_resp_V_TREADY in_acc 1 1 } } }
	responseStream_V_size_V { axis {  { responseStream_V_size_V_TDATA in_data 0 16 }  { responseStream_V_size_V_TVALID in_vld 0 1 }  { responseStream_V_size_V_TREADY in_acc 1 1 } } }
	keypointStream_V_data_V { axis {  { keypointStream_TDATA out_data 1 32 } } }
	keypointStream_V_keep_V { axis {  { keypointStream_TKEEP out_data 1 4 } } }
	keypointStream_V_strb_V { axis {  { keypointStream_TSTRB out_data 1 4 } } }
	keypointStream_V_user_V { axis {  { keypointStream_TUSER out_data 1 1 } } }
	keypointStream_V_last_V { axis {  { keypointStream_TLAST out_data 1 1 } } }
	keypointStream_V_id_V { axis {  { keypointStream_TID out_data 1 1 } } }
	keypointStream_V_dest_V { axis {  { keypointStream_TVALID out_vld 1 1 }  { keypointStream_TREADY out_acc 0 1 }  { keypointStream_TDEST out_data 1 1 } } }
	nKPCount { ap_none {  { nKPCount out_data 1 32 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
