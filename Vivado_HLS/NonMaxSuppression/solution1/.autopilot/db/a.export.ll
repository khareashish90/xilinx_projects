; ModuleID = 'D:/Documents/akhare/Xilinx_Projects/Vivado_HLS/NonMaxSuppression/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-f80:128:128-v64:64:64-v128:128:128-a0:0:64-f80:32:32-n8:16:32-S32"
target triple = "i686-pc-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@SuppressNonMax_str = internal unnamed_addr constant [15 x i8] c"SuppressNonMax\00"
@p_str8 = private unnamed_addr constant [3 x i8] c"L2\00", align 1
@p_str7 = private unnamed_addr constant [3 x i8] c"L1\00", align 1
@p_str5 = private unnamed_addr constant [5 x i8] c"both\00", align 1
@p_str4 = private unnamed_addr constant [5 x i8] c"axis\00", align 1
@p_str3 = private unnamed_addr constant [8 x i8] c"control\00", align 1
@p_str2 = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str = private unnamed_addr constant [8 x i8] c"ap_none\00", align 1

declare i11 @llvm.part.select.i11(i11, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define weak void @_ssdm_op_Write.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32*, i4*, i4*, i1*, i1*, i1*, i1*, i32, i4, i4, i1, i1, i1, i1) {
entry:
  store i32 %7, i32* %0
  store i4 %8, i4* %1
  store i4 %9, i4* %2
  store i1 %10, i1* %3
  store i1 %11, i1* %4
  store i1 %12, i1* %5
  store i1 %13, i1* %6
  ret void
}

define weak void @_ssdm_op_Write.ap_none.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecLatency(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak { i16, i16 } @_ssdm_op_Read.axis.volatile.i16P.i16P(i16*, i16*) {
entry:
  %empty = load i16* %0
  %empty_5 = load i16* %1
  %mrv_0 = insertvalue { i16, i16 } undef, i16 %empty, 0
  %mrv1 = insertvalue { i16, i16 } %mrv_0, i16 %empty_5, 1
  ret { i16, i16 } %mrv1
}

define weak i16 @_ssdm_op_Read.ap_auto.i16(i16) {
entry:
  ret i16 %0
}

define weak i11 @_ssdm_op_Read.ap_auto.i11(i11) {
entry:
  ret i11 %0
}

declare i15 @_ssdm_op_PartSelect.i15.i16.i32.i32(i16, i32, i32) nounwind readnone

define weak i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11, i32, i32) nounwind readnone {
entry:
  %empty = call i11 @llvm.part.select.i11(i11 %0, i32 %1, i32 %2)
  %empty_6 = trunc i11 %empty to i10
  ret i10 %empty_6
}

define weak i28 @_ssdm_op_BitConcatenate.i28.i12.i16(i12, i16) nounwind readnone {
entry:
  %empty = zext i12 %0 to i28
  %empty_7 = zext i16 %1 to i28
  %empty_8 = shl i28 %empty, 16
  %empty_9 = or i28 %empty_8, %empty_7
  ret i28 %empty_9
}

declare void @_GLOBAL__I_a() nounwind

define void @SuppressNonMax(i16* %responseStream_V_resp_V, i16* %responseStream_V_size_V, i32* %keypointStream_V_data_V, i4* %keypointStream_V_keep_V, i4* %keypointStream_V_strb_V, i1* %keypointStream_V_user_V, i1* %keypointStream_V_last_V, i1* %keypointStream_V_id_V, i1* %keypointStream_V_dest_V, i32* %nKPCount) {
  %nKeypointsDetected = alloca i32
  %Window_4_3_V = alloca i16
  %Window_0_0_V = alloca i16
  %Window_0_1_V = alloca i16
  %Window_0_2_V = alloca i16
  %Window_0_3_V = alloca i16
  %Window_4_2_V = alloca i16
  %Window_1_0_V = alloca i16
  %Window_1_1_V = alloca i16
  %Window_1_2_V = alloca i16
  %Window_1_3_V = alloca i16
  %Window_4_1_V = alloca i16
  %Window_2_0_V = alloca i16
  %Window_2_1_V = alloca i16
  %Window_2_2_V = alloca i16
  %Window_2_3_V = alloca i16
  %Window_4_0_V = alloca i16
  %Window_3_0_V = alloca i16
  %Window_3_1_V = alloca i16
  %Window_3_2_V = alloca i16
  %Window_3_3_V = alloca i16
  call void (...)* @_ssdm_op_SpecBitsMap(i16* %responseStream_V_resp_V), !map !76
  call void (...)* @_ssdm_op_SpecBitsMap(i16* %responseStream_V_size_V), !map !80
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %keypointStream_V_data_V), !map !84
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %keypointStream_V_keep_V), !map !88
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %keypointStream_V_strb_V), !map !92
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %keypointStream_V_user_V), !map !96
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %keypointStream_V_last_V), !map !100
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %keypointStream_V_id_V), !map !104
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %keypointStream_V_dest_V), !map !108
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %nKPCount), !map !112
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @SuppressNonMax_str) nounwind
  %ResponseBuf_1_V = alloca [1920 x i16], align 2
  %ResponseBuf_2_V = alloca [1920 x i16], align 2
  %ResponseBuf_3_V = alloca [1920 x i16], align 2
  %ResponseBuf_4_V = alloca [1920 x i16], align 2
  call void (...)* @_ssdm_op_SpecInterface(i32* %nKPCount, [8 x i8]* @p_str, i32 1, i32 1, [1 x i8]* @p_str1, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str2, i32 0, i32 0, [1 x i8]* @p_str1, i32 0, i32 0, [8 x i8]* @p_str3, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i16* %responseStream_V_resp_V, i16* %responseStream_V_size_V, [5 x i8]* @p_str4, i32 1, i32 1, [5 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1)
  call void (...)* @_ssdm_op_SpecInterface(i32* %keypointStream_V_data_V, i4* %keypointStream_V_keep_V, i4* %keypointStream_V_strb_V, i1* %keypointStream_V_user_V, i1* %keypointStream_V_last_V, i1* %keypointStream_V_id_V, i1* %keypointStream_V_dest_V, [5 x i8]* @p_str4, i32 1, i32 1, [5 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  store i32 0, i32* %nKeypointsDetected
  br label %1

; <label>:1                                       ; preds = %6, %0
  %iRow = phi i11 [ 0, %0 ], [ %iRow_1, %6 ]
  %exitcond1 = icmp eq i11 %iRow, -966
  %iRow_1 = add i11 %iRow, 1
  br i1 %exitcond1, label %7, label %2

; <label>:2                                       ; preds = %1
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 1082, i64 1082, i64 1082)
  call void (...)* @_ssdm_op_SpecLoopName([3 x i8]* @p_str7) nounwind
  %tmp_6 = call i32 (...)* @_ssdm_op_SpecRegionBegin([3 x i8]* @p_str7)
  call void (...)* @_ssdm_op_SpecLatency(i32 0, i32 65535, [1 x i8]* @p_str1) nounwind
  %tmp = call i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11 %iRow, i32 1, i32 10)
  %icmp = icmp ne i10 %tmp, 0
  %tmp_1_cast = zext i11 %iRow to i12
  %keyPointOut_Y_V = add i12 %tmp_1_cast, -2
  %tmp_2 = icmp eq i11 %iRow, 2
  %tmp_3 = icmp eq i11 %iRow, -967
  br label %3

; <label>:3                                       ; preds = %.loopexit._crit_edge, %2
  %iCol = phi i11 [ 0, %2 ], [ %iCol_1, %.loopexit._crit_edge ]
  %Window_0_1_V_1 = load i16* %Window_0_1_V
  %Window_0_2_V_1 = load i16* %Window_0_2_V
  %Window_0_3_V_1 = load i16* %Window_0_3_V
  %Window_1_1_V_1 = load i16* %Window_1_1_V
  %Window_1_2_V_1 = load i16* %Window_1_2_V
  %Window_1_3_V_1 = load i16* %Window_1_3_V
  %Window_2_1_V_1 = load i16* %Window_2_1_V
  %Window_2_2_V_1 = load i16* %Window_2_2_V
  %Window_2_3_V_1 = load i16* %Window_2_3_V
  %Window_3_1_V_1 = load i16* %Window_3_1_V
  %Window_3_2_V_1 = load i16* %Window_3_2_V
  %Window_3_3_V_1 = load i16* %Window_3_3_V
  %exitcond2 = icmp eq i11 %iCol, -126
  %iCol_1 = add i11 %iCol, 1
  br i1 %exitcond2, label %6, label %4

; <label>:4                                       ; preds = %3
  %iCol_cast6 = zext i11 %iCol to i32
  %empty_10 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 1922, i64 1922, i64 1922)
  call void (...)* @_ssdm_op_SpecLoopName([3 x i8]* @p_str8) nounwind
  %tmp_7 = call i32 (...)* @_ssdm_op_SpecRegionBegin([3 x i8]* @p_str8)
  call void (...)* @_ssdm_op_SpecPipeline(i32 -1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %pixelIn_V = call fastcc i16 @ReadFromStream(i16* %responseStream_V_resp_V, i16* %responseStream_V_size_V, i11 %iRow, i11 %iCol)
  %tmp_5 = icmp ult i11 %iCol, -128
  br i1 %tmp_5, label %.preheader152.0, label %.loopexit

.preheader152.0:                                  ; preds = %4
  %ResponseBuf_1_V_add = getelementptr [1920 x i16]* %ResponseBuf_1_V, i32 0, i32 %iCol_cast6
  %Window_0_4_V = load i16* %ResponseBuf_1_V_add, align 2
  %ResponseBuf_2_V_add = getelementptr [1920 x i16]* %ResponseBuf_2_V, i32 0, i32 %iCol_cast6
  %Window_1_4_V = load i16* %ResponseBuf_2_V_add, align 2
  store i16 %Window_1_4_V, i16* %ResponseBuf_1_V_add, align 2
  %ResponseBuf_3_V_add = getelementptr [1920 x i16]* %ResponseBuf_3_V, i32 0, i32 %iCol_cast6
  %Window_2_4_V = load i16* %ResponseBuf_3_V_add, align 2
  store i16 %Window_2_4_V, i16* %ResponseBuf_2_V_add, align 2
  %ResponseBuf_4_V_add = getelementptr [1920 x i16]* %ResponseBuf_4_V, i32 0, i32 %iCol_cast6
  %Window_3_4_V = load i16* %ResponseBuf_4_V_add, align 2
  store i16 %Window_3_4_V, i16* %ResponseBuf_3_V_add, align 2
  store i16 %pixelIn_V, i16* %ResponseBuf_4_V_add, align 2
  br label %.loopexit

.loopexit:                                        ; preds = %.preheader152.0, %4
  %Window_V_3_4_2 = phi i16 [ %Window_3_4_V, %.preheader152.0 ], [ 0, %4 ]
  %Window_V_2_4_2 = phi i16 [ %Window_2_4_V, %.preheader152.0 ], [ 0, %4 ]
  %Window_V_1_4_2 = phi i16 [ %Window_1_4_V, %.preheader152.0 ], [ 0, %4 ]
  %Window_V_0_4_2 = phi i16 [ %Window_0_4_V, %.preheader152.0 ], [ 0, %4 ]
  %Window_4_4_V = phi i16 [ %pixelIn_V, %.preheader152.0 ], [ 0, %4 ]
  %tmp_9 = call i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11 %iCol, i32 1, i32 10)
  %icmp3 = icmp ne i10 %tmp_9, 0
  %or_cond = and i1 %icmp, %icmp3
  br i1 %or_cond, label %5, label %.loopexit._crit_edge

; <label>:5                                       ; preds = %.loopexit
  %Window_4_3_V_load = load i16* %Window_4_3_V
  %Window_0_0_V_load = load i16* %Window_0_0_V
  %Window_4_2_V_load = load i16* %Window_4_2_V
  %Window_1_0_V_load = load i16* %Window_1_0_V
  %Window_4_1_V_load = load i16* %Window_4_1_V
  %Window_2_0_V_load = load i16* %Window_2_0_V
  %Window_4_0_V_load = load i16* %Window_4_0_V
  %Window_3_0_V_load = load i16* %Window_3_0_V
  %bKeypointDetected = call fastcc i1 @IsLocalExtremum(i16 %Window_0_0_V_load, i16 %Window_0_1_V_1, i16 %Window_0_2_V_1, i16 %Window_0_3_V_1, i16 %Window_V_0_4_2, i16 %Window_1_0_V_load, i16 %Window_1_1_V_1, i16 %Window_1_2_V_1, i16 %Window_1_3_V_1, i16 %Window_V_1_4_2, i16 %Window_2_0_V_load, i16 %Window_2_1_V_1, i16 %Window_2_2_V_1, i16 %Window_2_3_V_1, i16 %Window_V_2_4_2, i16 %Window_3_0_V_load, i16 %Window_3_1_V_1, i16 %Window_3_2_V_1, i16 %Window_3_3_V_1, i16 %Window_V_3_4_2, i16 %Window_4_0_V_load, i16 %Window_4_1_V_load, i16 %Window_4_2_V_load, i16 %Window_4_3_V_load, i16 %Window_4_4_V)
  %tmp_4 = icmp eq i11 %iCol, -127
  %tmp_last_V = and i1 %tmp_3, %tmp_4
  %or_cond2 = or i1 %bKeypointDetected, %tmp_last_V
  br i1 %or_cond2, label %._crit_edge156, label %.loopexit._crit_edge

._crit_edge156:                                   ; preds = %5
  %nKeypointsDetected_l = load i32* %nKeypointsDetected
  %keyPointOut_X_V = add i11 %iCol, -2
  %keyPointOut_X_V_cast = zext i11 %keyPointOut_X_V to i16
  %nKeypointsDetected_1 = add nsw i32 %nKeypointsDetected_l, 1
  %tmp_1 = call i28 @_ssdm_op_BitConcatenate.i28.i12.i16(i12 %keyPointOut_Y_V, i16 %keyPointOut_X_V_cast)
  %p_Result_s = sext i28 %tmp_1 to i32
  %tmp_8 = icmp eq i11 %iCol, 2
  %tmp_user_V = and i1 %tmp_2, %tmp_8
  call void @_ssdm_op_Write.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32* %keypointStream_V_data_V, i4* %keypointStream_V_keep_V, i4* %keypointStream_V_strb_V, i1* %keypointStream_V_user_V, i1* %keypointStream_V_last_V, i1* %keypointStream_V_id_V, i1* %keypointStream_V_dest_V, i32 %p_Result_s, i4 -1, i4 -1, i1 %tmp_user_V, i1 %tmp_last_V, i1 false, i1 false)
  store i32 %nKeypointsDetected_1, i32* %nKeypointsDetected
  br label %.loopexit._crit_edge

.loopexit._crit_edge:                             ; preds = %._crit_edge156, %5, %.loopexit
  %Window_4_3_V_load_1 = load i16* %Window_4_3_V
  %Window_4_2_V_load_1 = load i16* %Window_4_2_V
  %Window_4_1_V_load_1 = load i16* %Window_4_1_V
  %empty_11 = call i32 (...)* @_ssdm_op_SpecRegionEnd([3 x i8]* @p_str8, i32 %tmp_7)
  store i16 %Window_V_3_4_2, i16* %Window_3_3_V
  store i16 %Window_3_3_V_1, i16* %Window_3_2_V
  store i16 %Window_3_2_V_1, i16* %Window_3_1_V
  store i16 %Window_3_1_V_1, i16* %Window_3_0_V
  store i16 %Window_4_1_V_load_1, i16* %Window_4_0_V
  store i16 %Window_V_2_4_2, i16* %Window_2_3_V
  store i16 %Window_2_3_V_1, i16* %Window_2_2_V
  store i16 %Window_2_2_V_1, i16* %Window_2_1_V
  store i16 %Window_2_1_V_1, i16* %Window_2_0_V
  store i16 %Window_4_2_V_load_1, i16* %Window_4_1_V
  store i16 %Window_V_1_4_2, i16* %Window_1_3_V
  store i16 %Window_1_3_V_1, i16* %Window_1_2_V
  store i16 %Window_1_2_V_1, i16* %Window_1_1_V
  store i16 %Window_1_1_V_1, i16* %Window_1_0_V
  store i16 %Window_4_3_V_load_1, i16* %Window_4_2_V
  store i16 %Window_V_0_4_2, i16* %Window_0_3_V
  store i16 %Window_0_3_V_1, i16* %Window_0_2_V
  store i16 %Window_0_2_V_1, i16* %Window_0_1_V
  store i16 %Window_0_1_V_1, i16* %Window_0_0_V
  store i16 %Window_4_4_V, i16* %Window_4_3_V
  br label %3

; <label>:6                                       ; preds = %3
  %empty_12 = call i32 (...)* @_ssdm_op_SpecRegionEnd([3 x i8]* @p_str7, i32 %tmp_6)
  br label %1

; <label>:7                                       ; preds = %1
  %nKeypointsDetected_l_1 = load i32* %nKeypointsDetected
  call void @_ssdm_op_Write.ap_none.i32P(i32* %nKPCount, i32 %nKeypointsDetected_l_1)
  ret void
}

define internal fastcc i16 @ReadFromStream(i16* %responseStream_V_res, i16* %responseStream_V_siz, i11 %iRow, i11 %iCol) {
  %iCol_read = call i11 @_ssdm_op_Read.ap_auto.i11(i11 %iCol)
  %iRow_read = call i11 @_ssdm_op_Read.ap_auto.i11(i11 %iRow)
  call void (...)* @_ssdm_op_SpecInterface(i16* %responseStream_V_res, i16* %responseStream_V_siz, [5 x i8]* @p_str4, i32 0, i32 0, [5 x i8]* @p_str5, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1)
  %tmp = icmp ult i11 %iRow_read, -968
  %tmp_s = icmp ult i11 %iCol_read, -128
  %or_cond = and i1 %tmp, %tmp_s
  br i1 %or_cond, label %1, label %2

; <label>:1                                       ; preds = %0
  %empty = call { i16, i16 } @_ssdm_op_Read.axis.volatile.i16P.i16P(i16* %responseStream_V_res, i16* %responseStream_V_siz)
  %tmp_resp_V = extractvalue { i16, i16 } %empty, 0
  br label %2

; <label>:2                                       ; preds = %1, %0
  %pixelIn_V = phi i16 [ %tmp_resp_V, %1 ], [ 0, %0 ]
  ret i16 %pixelIn_V
}

define internal fastcc i1 @IsLocalExtremum(i16 %Window_0_0_V_read, i16 %Window_0_1_V_read, i16 %Window_0_2_V_read, i16 %Window_0_3_V_read, i16 %Window_0_4_V_read, i16 %Window_1_0_V_read, i16 %Window_1_1_V_read, i16 %Window_1_2_V_read, i16 %Window_1_3_V_read, i16 %Window_1_4_V_read, i16 %Window_2_0_V_read, i16 %Window_2_1_V_read, i16 %Window_2_2_V_read, i16 %Window_2_3_V_read, i16 %Window_2_4_V_read, i16 %Window_3_0_V_read, i16 %Window_3_1_V_read, i16 %Window_3_2_V_read, i16 %Window_3_3_V_read, i16 %Window_3_4_V_read, i16 %Window_4_0_V_read, i16 %Window_4_1_V_read, i16 %Window_4_2_V_read, i16 %Window_4_3_V_read, i16 %Window_4_4_V_read) readnone {
.preheader.preheader.0_ifconv:
  %Window_4_4_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_4_4_V_read)
  %Window_4_3_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_4_3_V_read)
  %Window_4_2_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_4_2_V_read)
  %Window_4_1_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_4_1_V_read)
  %Window_4_0_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_4_0_V_read)
  %Window_3_4_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_3_4_V_read)
  %Window_3_3_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_3_3_V_read)
  %Window_3_2_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_3_2_V_read)
  %Window_3_1_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_3_1_V_read)
  %Window_3_0_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_3_0_V_read)
  %Window_2_4_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_2_4_V_read)
  %Window_2_3_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_2_3_V_read)
  %Window_2_2_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_2_2_V_read)
  %Window_2_1_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_2_1_V_read)
  %Window_2_0_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_2_0_V_read)
  %Window_1_4_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_1_4_V_read)
  %Window_1_3_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_1_3_V_read)
  %Window_1_2_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_1_2_V_read)
  %Window_1_1_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_1_1_V_read)
  %Window_1_0_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_1_0_V_read)
  %Window_0_4_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_0_4_V_read)
  %Window_0_3_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_0_3_V_read)
  %Window_0_2_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_0_2_V_read)
  %Window_0_1_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_0_1_V_read)
  %Window_0_0_V_read_1 = call i16 @_ssdm_op_Read.ap_auto.i16(i16 %Window_0_0_V_read)
  %tmp_s = icmp sgt i16 %Window_0_0_V_read_1, 3840
  %tmp_110 = trunc i16 %Window_0_0_V_read_1 to i15
  %p_0_2 = select i1 %tmp_s, i15 %tmp_110, i15 3840
  %p_0_2_cast = zext i15 %p_0_2 to i16
  %tmp_1 = icmp slt i16 %Window_0_0_V_read_1, -3840
  %p_038_2 = select i1 %tmp_1, i16 %Window_0_0_V_read_1, i16 -3840
  %tmp_17_0_1 = icmp slt i16 %p_0_2_cast, %Window_0_1_V_read_1
  %p_0_2_0_1 = select i1 %tmp_17_0_1, i16 %Window_0_1_V_read_1, i16 %p_0_2_cast
  %tmp_19_0_1 = icmp sgt i16 %p_038_2, %Window_0_1_V_read_1
  %p_038_2_0_1 = select i1 %tmp_19_0_1, i16 %Window_0_1_V_read_1, i16 %p_038_2
  %tmp_17_0_2 = icmp slt i16 %p_0_2_0_1, %Window_0_2_V_read_1
  %p_0_2_0_2 = select i1 %tmp_17_0_2, i16 %Window_0_2_V_read_1, i16 %p_0_2_0_1
  %tmp_19_0_2 = icmp sgt i16 %p_038_2_0_1, %Window_0_2_V_read_1
  %p_038_2_0_2 = select i1 %tmp_19_0_2, i16 %Window_0_2_V_read_1, i16 %p_038_2_0_1
  %tmp_17_0_3 = icmp slt i16 %p_0_2_0_2, %Window_0_3_V_read_1
  %p_0_2_0_3 = select i1 %tmp_17_0_3, i16 %Window_0_3_V_read_1, i16 %p_0_2_0_2
  %tmp_19_0_3 = icmp sgt i16 %p_038_2_0_2, %Window_0_3_V_read_1
  %p_038_2_0_3 = select i1 %tmp_19_0_3, i16 %Window_0_3_V_read_1, i16 %p_038_2_0_2
  %tmp_17_0_4 = icmp slt i16 %p_0_2_0_3, %Window_0_4_V_read_1
  %p_0_2_0_4 = select i1 %tmp_17_0_4, i16 %Window_0_4_V_read_1, i16 %p_0_2_0_3
  %tmp_19_0_4 = icmp sgt i16 %p_038_2_0_3, %Window_0_4_V_read_1
  %p_038_2_0_4 = select i1 %tmp_19_0_4, i16 %Window_0_4_V_read_1, i16 %p_038_2_0_3
  %tmp_17_1 = icmp slt i16 %p_0_2_0_4, %Window_1_0_V_read_1
  %p_0_2_1 = select i1 %tmp_17_1, i16 %Window_1_0_V_read_1, i16 %p_0_2_0_4
  %tmp_19_1 = icmp sgt i16 %p_038_2_0_4, %Window_1_0_V_read_1
  %p_038_2_1 = select i1 %tmp_19_1, i16 %Window_1_0_V_read_1, i16 %p_038_2_0_4
  %tmp_17_1_1 = icmp slt i16 %p_0_2_1, %Window_1_1_V_read_1
  %p_0_2_1_1 = select i1 %tmp_17_1_1, i16 %Window_1_1_V_read_1, i16 %p_0_2_1
  %tmp_19_1_1 = icmp sgt i16 %p_038_2_1, %Window_1_1_V_read_1
  %p_038_2_1_1 = select i1 %tmp_19_1_1, i16 %Window_1_1_V_read_1, i16 %p_038_2_1
  %tmp_17_1_2 = icmp slt i16 %p_0_2_1_1, %Window_1_2_V_read_1
  %p_0_2_1_2 = select i1 %tmp_17_1_2, i16 %Window_1_2_V_read_1, i16 %p_0_2_1_1
  %tmp_19_1_2 = icmp sgt i16 %p_038_2_1_1, %Window_1_2_V_read_1
  %p_038_2_1_2 = select i1 %tmp_19_1_2, i16 %Window_1_2_V_read_1, i16 %p_038_2_1_1
  %tmp_17_1_3 = icmp slt i16 %p_0_2_1_2, %Window_1_3_V_read_1
  %p_0_2_1_3 = select i1 %tmp_17_1_3, i16 %Window_1_3_V_read_1, i16 %p_0_2_1_2
  %tmp_19_1_3 = icmp sgt i16 %p_038_2_1_2, %Window_1_3_V_read_1
  %p_038_2_1_3 = select i1 %tmp_19_1_3, i16 %Window_1_3_V_read_1, i16 %p_038_2_1_2
  %tmp_17_1_4 = icmp slt i16 %p_0_2_1_3, %Window_1_4_V_read_1
  %p_0_2_1_4 = select i1 %tmp_17_1_4, i16 %Window_1_4_V_read_1, i16 %p_0_2_1_3
  %tmp_19_1_4 = icmp sgt i16 %p_038_2_1_3, %Window_1_4_V_read_1
  %p_038_2_1_4 = select i1 %tmp_19_1_4, i16 %Window_1_4_V_read_1, i16 %p_038_2_1_3
  %tmp_17_2 = icmp slt i16 %p_0_2_1_4, %Window_2_0_V_read_1
  %p_0_2_2 = select i1 %tmp_17_2, i16 %Window_2_0_V_read_1, i16 %p_0_2_1_4
  %tmp_19_2 = icmp sgt i16 %p_038_2_1_4, %Window_2_0_V_read_1
  %p_038_2_2 = select i1 %tmp_19_2, i16 %Window_2_0_V_read_1, i16 %p_038_2_1_4
  %tmp_17_2_1 = icmp slt i16 %p_0_2_2, %Window_2_1_V_read_1
  %p_0_2_2_1 = select i1 %tmp_17_2_1, i16 %Window_2_1_V_read_1, i16 %p_0_2_2
  %tmp_19_2_1 = icmp sgt i16 %p_038_2_2, %Window_2_1_V_read_1
  %p_038_2_2_1 = select i1 %tmp_19_2_1, i16 %Window_2_1_V_read_1, i16 %p_038_2_2
  %tmp_17_2_2 = icmp slt i16 %p_0_2_2_1, %Window_2_2_V_read_1
  %p_0_2_2_2 = select i1 %tmp_17_2_2, i16 %Window_2_2_V_read_1, i16 %p_0_2_2_1
  %tmp_19_2_2 = icmp sgt i16 %p_038_2_2_1, %Window_2_2_V_read_1
  %p_038_2_2_2 = select i1 %tmp_19_2_2, i16 %Window_2_2_V_read_1, i16 %p_038_2_2_1
  %tmp_17_2_3 = icmp slt i16 %p_0_2_2_2, %Window_2_3_V_read_1
  %p_0_2_2_3 = select i1 %tmp_17_2_3, i16 %Window_2_3_V_read_1, i16 %p_0_2_2_2
  %tmp_19_2_3 = icmp sgt i16 %p_038_2_2_2, %Window_2_3_V_read_1
  %p_038_2_2_3 = select i1 %tmp_19_2_3, i16 %Window_2_3_V_read_1, i16 %p_038_2_2_2
  %tmp_17_2_4 = icmp slt i16 %p_0_2_2_3, %Window_2_4_V_read_1
  %p_0_2_2_4 = select i1 %tmp_17_2_4, i16 %Window_2_4_V_read_1, i16 %p_0_2_2_3
  %tmp_19_2_4 = icmp sgt i16 %p_038_2_2_3, %Window_2_4_V_read_1
  %p_038_2_2_4 = select i1 %tmp_19_2_4, i16 %Window_2_4_V_read_1, i16 %p_038_2_2_3
  %tmp_17_3 = icmp slt i16 %p_0_2_2_4, %Window_3_0_V_read_1
  %p_0_2_3 = select i1 %tmp_17_3, i16 %Window_3_0_V_read_1, i16 %p_0_2_2_4
  %tmp_19_3 = icmp sgt i16 %p_038_2_2_4, %Window_3_0_V_read_1
  %p_038_2_3 = select i1 %tmp_19_3, i16 %Window_3_0_V_read_1, i16 %p_038_2_2_4
  %tmp_17_3_1 = icmp slt i16 %p_0_2_3, %Window_3_1_V_read_1
  %p_0_2_3_1 = select i1 %tmp_17_3_1, i16 %Window_3_1_V_read_1, i16 %p_0_2_3
  %tmp_19_3_1 = icmp sgt i16 %p_038_2_3, %Window_3_1_V_read_1
  %p_038_2_3_1 = select i1 %tmp_19_3_1, i16 %Window_3_1_V_read_1, i16 %p_038_2_3
  %tmp_17_3_2 = icmp slt i16 %p_0_2_3_1, %Window_3_2_V_read_1
  %p_0_2_3_2 = select i1 %tmp_17_3_2, i16 %Window_3_2_V_read_1, i16 %p_0_2_3_1
  %tmp_19_3_2 = icmp sgt i16 %p_038_2_3_1, %Window_3_2_V_read_1
  %p_038_2_3_2 = select i1 %tmp_19_3_2, i16 %Window_3_2_V_read_1, i16 %p_038_2_3_1
  %tmp_17_3_3 = icmp slt i16 %p_0_2_3_2, %Window_3_3_V_read_1
  %p_0_2_3_3 = select i1 %tmp_17_3_3, i16 %Window_3_3_V_read_1, i16 %p_0_2_3_2
  %tmp_19_3_3 = icmp sgt i16 %p_038_2_3_2, %Window_3_3_V_read_1
  %p_038_2_3_3 = select i1 %tmp_19_3_3, i16 %Window_3_3_V_read_1, i16 %p_038_2_3_2
  %tmp_17_3_4 = icmp slt i16 %p_0_2_3_3, %Window_3_4_V_read_1
  %p_0_2_3_4 = select i1 %tmp_17_3_4, i16 %Window_3_4_V_read_1, i16 %p_0_2_3_3
  %tmp_19_3_4 = icmp sgt i16 %p_038_2_3_3, %Window_3_4_V_read_1
  %p_038_2_3_4 = select i1 %tmp_19_3_4, i16 %Window_3_4_V_read_1, i16 %p_038_2_3_3
  %tmp_17_4 = icmp slt i16 %p_0_2_3_4, %Window_4_0_V_read_1
  %p_0_2_4 = select i1 %tmp_17_4, i16 %Window_4_0_V_read_1, i16 %p_0_2_3_4
  %tmp_19_4 = icmp sgt i16 %p_038_2_3_4, %Window_4_0_V_read_1
  %p_038_2_4 = select i1 %tmp_19_4, i16 %Window_4_0_V_read_1, i16 %p_038_2_3_4
  %tmp_17_4_1 = icmp slt i16 %p_0_2_4, %Window_4_1_V_read_1
  %p_0_2_4_1 = select i1 %tmp_17_4_1, i16 %Window_4_1_V_read_1, i16 %p_0_2_4
  %tmp_19_4_1 = icmp sgt i16 %p_038_2_4, %Window_4_1_V_read_1
  %p_038_2_4_1 = select i1 %tmp_19_4_1, i16 %Window_4_1_V_read_1, i16 %p_038_2_4
  %tmp_17_4_2 = icmp slt i16 %p_0_2_4_1, %Window_4_2_V_read_1
  %p_0_2_4_2 = select i1 %tmp_17_4_2, i16 %Window_4_2_V_read_1, i16 %p_0_2_4_1
  %tmp_19_4_2 = icmp sgt i16 %p_038_2_4_1, %Window_4_2_V_read_1
  %p_038_2_4_2 = select i1 %tmp_19_4_2, i16 %Window_4_2_V_read_1, i16 %p_038_2_4_1
  %tmp_17_4_3 = icmp slt i16 %p_0_2_4_2, %Window_4_3_V_read_1
  %p_0_2_4_3 = select i1 %tmp_17_4_3, i16 %Window_4_3_V_read_1, i16 %p_0_2_4_2
  %tmp_19_4_3 = icmp sgt i16 %p_038_2_4_2, %Window_4_3_V_read_1
  %p_038_2_4_3 = select i1 %tmp_19_4_3, i16 %Window_4_3_V_read_1, i16 %p_038_2_4_2
  %tmp_17_4_4 = icmp slt i16 %p_0_2_4_3, %Window_4_4_V_read_1
  %tmp_19_4_4 = icmp sgt i16 %p_038_2_4_3, %Window_4_4_V_read_1
  %not_tmp_17_0_3 = xor i1 %tmp_17_0_3, true
  %tmp = and i1 %tmp_17_0_2, %not_tmp_17_0_3
  %tmp188_demorgan = or i1 %tmp_17_1, %tmp_17_1_1
  %tmp187_demorgan = or i1 %tmp188_demorgan, %tmp_17_0_4
  %tmp2 = xor i1 %tmp187_demorgan, true
  %tmp3 = and i1 %tmp, %tmp2
  %tmp4 = or i1 %tmp_17_1_2, %tmp3
  %not_tmp_17_1_3 = xor i1 %tmp_17_1_3, true
  %tmp5 = and i1 %tmp4, %not_tmp_17_1_3
  %tmp191_demorgan = or i1 %tmp_17_2, %tmp_17_2_1
  %tmp190_demorgan = or i1 %tmp191_demorgan, %tmp_17_1_4
  %tmp6 = xor i1 %tmp190_demorgan, true
  %tmp7 = and i1 %tmp5, %tmp6
  %tmp8 = or i1 %tmp_17_2_2, %tmp7
  %not_tmp_17_2_3 = xor i1 %tmp_17_2_3, true
  %not_tmp_17_2_4 = xor i1 %tmp_17_2_4, true
  %tmp9 = and i1 %tmp8, %not_tmp_17_2_3
  %tmp10_demorgan = or i1 %tmp_17_3, %tmp_17_3_1
  %tmp1 = xor i1 %tmp10_demorgan, true
  %tmp10 = and i1 %tmp1, %not_tmp_17_2_4
  %tmp11 = and i1 %tmp10, %tmp9
  %tmp13 = or i1 %tmp_17_3_2, %tmp11
  %not_tmp_17_3_3 = xor i1 %tmp_17_3_3, true
  %not_tmp_17_3_4 = xor i1 %tmp_17_3_4, true
  %not_tmp_17_4 = xor i1 %tmp_17_4, true
  %not_tmp_17_4_1 = xor i1 %tmp_17_4_1, true
  %tmp14 = and i1 %tmp13, %not_tmp_17_3_3
  %tmp15 = and i1 %not_tmp_17_4, %not_tmp_17_4_1
  %tmp16 = and i1 %tmp15, %not_tmp_17_3_4
  %tmp17 = and i1 %tmp16, %tmp14
  %tmp12 = or i1 %tmp_17_4_2, %tmp17
  %tmp18 = or i1 %tmp_17_2_3, %tmp_17_2_4
  %tmp19 = or i1 %tmp18, %tmp_17_2_2
  %tmp_3 = or i1 %tmp19, %tmp191_demorgan
  %not_tmp_17_3_2 = xor i1 %tmp_17_3_2, true
  %not_tmp_17_4_2 = xor i1 %tmp_17_4_2, true
  %tmp_22 = icmp ne i16 %p_0_2_cast, %Window_0_1_V_read_1
  %tmp_4 = and i1 %tmp_s, %tmp_22
  %tmp_6 = or i1 %tmp_17_0_1, %tmp_4
  %not_tmp_18_0_2 = icmp ne i16 %p_0_2_0_1, %Window_0_2_V_read_1
  %tmp_7 = and i1 %tmp_6, %not_tmp_18_0_2
  %tmp_8 = or i1 %tmp_17_0_2, %tmp_7
  %not_tmp_18_0_3 = icmp ne i16 %p_0_2_0_2, %Window_0_3_V_read_1
  %tmp_9 = and i1 %tmp_8, %not_tmp_18_0_3
  %tmp_10 = or i1 %tmp_17_0_3, %tmp_9
  %not_tmp_18_0_4 = icmp ne i16 %p_0_2_0_3, %Window_0_4_V_read_1
  %tmp_11 = and i1 %tmp_10, %not_tmp_18_0_4
  %tmp_12 = or i1 %tmp_17_0_4, %tmp_11
  %not_tmp_18_1 = icmp ne i16 %p_0_2_0_4, %Window_1_0_V_read_1
  %tmp_13 = and i1 %tmp_12, %not_tmp_18_1
  %tmp_14 = or i1 %tmp_17_1, %tmp_13
  %not_tmp_18_1_1 = icmp ne i16 %p_0_2_1, %Window_1_1_V_read_1
  %tmp_15 = and i1 %tmp_14, %not_tmp_18_1_1
  %tmp_16 = or i1 %tmp_17_1_1, %tmp_15
  %not_tmp_18_1_2 = icmp ne i16 %p_0_2_1_1, %Window_1_2_V_read_1
  %tmp_17 = and i1 %tmp_16, %not_tmp_18_1_2
  %tmp_18 = or i1 %tmp_17_1_2, %tmp_17
  %not_tmp_18_1_3 = icmp ne i16 %p_0_2_1_2, %Window_1_3_V_read_1
  %tmp_19 = and i1 %tmp_18, %not_tmp_18_1_3
  %tmp_20 = or i1 %tmp_17_1_3, %tmp_19
  %not_tmp_18_1_4 = icmp ne i16 %p_0_2_1_3, %Window_1_4_V_read_1
  %tmp_21 = and i1 %tmp_20, %not_tmp_18_1_4
  %tmp_23 = or i1 %tmp_17_1_4, %tmp_21
  %not_tmp_18_2 = icmp ne i16 %p_0_2_1_4, %Window_2_0_V_read_1
  %tmp_24 = and i1 %tmp_23, %not_tmp_18_2
  %tmp_25 = or i1 %tmp_17_2, %tmp_24
  %not_tmp_18_2_1 = icmp ne i16 %p_0_2_2, %Window_2_1_V_read_1
  %tmp_26 = and i1 %tmp_25, %not_tmp_18_2_1
  %tmp_27 = or i1 %tmp_17_2_1, %tmp_26
  %not_tmp_18_2_2 = icmp ne i16 %p_0_2_2_1, %Window_2_2_V_read_1
  %tmp_28 = and i1 %tmp_27, %not_tmp_18_2_2
  %tmp_29 = or i1 %tmp_17_2_2, %tmp_28
  %not_tmp_18_2_3 = icmp ne i16 %p_0_2_2_2, %Window_2_3_V_read_1
  %tmp_30 = and i1 %tmp_29, %not_tmp_18_2_3
  %tmp_31 = or i1 %tmp_17_2_3, %tmp_30
  %not_tmp_18_2_4 = icmp ne i16 %p_0_2_2_3, %Window_2_4_V_read_1
  %tmp_32 = and i1 %tmp_31, %not_tmp_18_2_4
  %tmp_33 = or i1 %tmp_17_2_4, %tmp_32
  %not_tmp_18_3 = icmp ne i16 %p_0_2_2_4, %Window_3_0_V_read_1
  %tmp_34 = and i1 %tmp_33, %not_tmp_18_3
  %tmp_35 = or i1 %tmp_17_3, %tmp_34
  %not_tmp_18_3_1 = icmp ne i16 %p_0_2_3, %Window_3_1_V_read_1
  %tmp_36 = and i1 %tmp_35, %not_tmp_18_3_1
  %tmp_37 = or i1 %tmp_17_3_1, %tmp_36
  %not_tmp_18_3_2 = icmp ne i16 %p_0_2_3_1, %Window_3_2_V_read_1
  %tmp_38 = and i1 %tmp_37, %not_tmp_18_3_2
  %tmp_39 = or i1 %tmp_17_3_2, %tmp_38
  %not_tmp_18_3_3 = icmp ne i16 %p_0_2_3_2, %Window_3_3_V_read_1
  %tmp_40 = and i1 %tmp_39, %not_tmp_18_3_3
  %tmp_41 = or i1 %tmp_17_3_3, %tmp_40
  %not_tmp_18_3_4 = icmp ne i16 %p_0_2_3_3, %Window_3_4_V_read_1
  %tmp_42 = and i1 %tmp_41, %not_tmp_18_3_4
  %tmp_43 = or i1 %tmp_17_3_4, %tmp_42
  %not_tmp_18_4 = icmp ne i16 %p_0_2_3_4, %Window_4_0_V_read_1
  %tmp_44 = and i1 %tmp_43, %not_tmp_18_4
  %tmp_45 = or i1 %tmp_17_4, %tmp_44
  %not_tmp_18_4_1 = icmp ne i16 %p_0_2_4, %Window_4_1_V_read_1
  %tmp_46 = and i1 %tmp_45, %not_tmp_18_4_1
  %tmp_47 = or i1 %tmp_17_4_1, %tmp_46
  %not_tmp_18_4_2 = icmp ne i16 %p_0_2_4_1, %Window_4_2_V_read_1
  %tmp_48 = and i1 %tmp_47, %not_tmp_18_4_2
  %tmp_49 = or i1 %tmp_17_4_2, %tmp_48
  %not_tmp_18_4_3 = icmp ne i16 %p_0_2_4_2, %Window_4_3_V_read_1
  %tmp_50 = and i1 %tmp_49, %not_tmp_18_4_3
  %tmp_51 = or i1 %tmp_17_4_3, %tmp_50
  %not_tmp_18_4_4 = icmp ne i16 %p_0_2_4_3, %Window_4_4_V_read_1
  %tmp_52 = and i1 %tmp_51, %not_tmp_18_4_4
  %tmp_2 = or i1 %tmp_17_4_4, %tmp_52
  %tmp20 = and i1 %tmp_3, %tmp1
  %tmp21 = and i1 %not_tmp_17_3_3, %not_tmp_17_3_4
  %tmp22 = and i1 %tmp21, %not_tmp_17_3_2
  %tmp23 = and i1 %tmp22, %tmp20
  %tmp24 = and i1 %not_tmp_17_4_1, %not_tmp_17_4_2
  %tmp25 = and i1 %tmp24, %not_tmp_17_4
  %tmp210_demorgan = or i1 %tmp_17_4_3, %tmp_17_4_4
  %tmp26 = xor i1 %tmp210_demorgan, true
  %tmp27 = and i1 %tmp12, %tmp_2
  %tmp28 = and i1 %tmp27, %tmp26
  %tmp29 = and i1 %tmp28, %tmp25
  %or_cond2 = and i1 %tmp29, %tmp23
  %not_tmp_19_0_3 = xor i1 %tmp_19_0_3, true
  %tmp30 = and i1 %tmp_19_0_2, %not_tmp_19_0_3
  %tmp214_demorgan = or i1 %tmp_19_1, %tmp_19_1_1
  %tmp213_demorgan = or i1 %tmp214_demorgan, %tmp_19_0_4
  %tmp31 = xor i1 %tmp213_demorgan, true
  %tmp_53 = and i1 %tmp30, %tmp31
  %tmp_54 = or i1 %tmp_19_1_2, %tmp_53
  %not_tmp_19_1_3 = xor i1 %tmp_19_1_3, true
  %tmp32 = and i1 %tmp_54, %not_tmp_19_1_3
  %tmp217_demorgan = or i1 %tmp_19_2, %tmp_19_2_1
  %tmp216_demorgan = or i1 %tmp217_demorgan, %tmp_19_1_4
  %tmp33 = xor i1 %tmp216_demorgan, true
  %tmp_55 = and i1 %tmp32, %tmp33
  %tmp_56 = or i1 %tmp_19_2_2, %tmp_55
  %not_tmp_19_2_3 = xor i1 %tmp_19_2_3, true
  %not_tmp_19_2_4 = xor i1 %tmp_19_2_4, true
  %tmp34 = and i1 %tmp_56, %not_tmp_19_2_3
  %tmp39_demorgan = or i1 %tmp_19_3, %tmp_19_3_1
  %tmp35 = xor i1 %tmp39_demorgan, true
  %tmp36 = and i1 %tmp35, %not_tmp_19_2_4
  %tmp_57 = and i1 %tmp36, %tmp34
  %tmp_58 = or i1 %tmp_19_3_2, %tmp_57
  %not_tmp_19_3_3 = xor i1 %tmp_19_3_3, true
  %not_tmp_19_3_4 = xor i1 %tmp_19_3_4, true
  %not_tmp_19_4 = xor i1 %tmp_19_4, true
  %not_tmp_19_4_1 = xor i1 %tmp_19_4_1, true
  %tmp37 = and i1 %tmp_58, %not_tmp_19_3_3
  %tmp38 = and i1 %not_tmp_19_4, %not_tmp_19_4_1
  %tmp39 = and i1 %tmp38, %not_tmp_19_3_4
  %tmp_59 = and i1 %tmp39, %tmp37
  %tmp_60 = or i1 %tmp_19_4_2, %tmp_59
  %tmp40 = or i1 %tmp_19_2_3, %tmp_19_2_4
  %tmp41 = or i1 %tmp40, %tmp_19_2_2
  %tmp_61 = or i1 %tmp41, %tmp217_demorgan
  %not_tmp_19_3_2 = xor i1 %tmp_19_3_2, true
  %not_tmp_19_4_2 = xor i1 %tmp_19_4_2, true
  %tmp_62 = icmp ne i16 %p_038_2, %Window_0_1_V_read_1
  %tmp_63 = and i1 %tmp_1, %tmp_62
  %tmp_64 = or i1 %tmp_19_0_1, %tmp_63
  %not_tmp_20_0_2 = icmp ne i16 %p_038_2_0_1, %Window_0_2_V_read_1
  %tmp_65 = and i1 %tmp_64, %not_tmp_20_0_2
  %tmp_66 = or i1 %tmp_19_0_2, %tmp_65
  %not_tmp_20_0_3 = icmp ne i16 %p_038_2_0_2, %Window_0_3_V_read_1
  %tmp_67 = and i1 %tmp_66, %not_tmp_20_0_3
  %tmp_68 = or i1 %tmp_19_0_3, %tmp_67
  %not_tmp_20_0_4 = icmp ne i16 %p_038_2_0_3, %Window_0_4_V_read_1
  %tmp_69 = and i1 %tmp_68, %not_tmp_20_0_4
  %tmp_70 = or i1 %tmp_19_0_4, %tmp_69
  %not_tmp_20_1 = icmp ne i16 %p_038_2_0_4, %Window_1_0_V_read_1
  %tmp_71 = and i1 %tmp_70, %not_tmp_20_1
  %tmp_72 = or i1 %tmp_19_1, %tmp_71
  %not_tmp_20_1_1 = icmp ne i16 %p_038_2_1, %Window_1_1_V_read_1
  %tmp_73 = and i1 %tmp_72, %not_tmp_20_1_1
  %tmp_74 = or i1 %tmp_19_1_1, %tmp_73
  %not_tmp_20_1_2 = icmp ne i16 %p_038_2_1_1, %Window_1_2_V_read_1
  %tmp_75 = and i1 %tmp_74, %not_tmp_20_1_2
  %tmp_76 = or i1 %tmp_19_1_2, %tmp_75
  %not_tmp_20_1_3 = icmp ne i16 %p_038_2_1_2, %Window_1_3_V_read_1
  %tmp_77 = and i1 %tmp_76, %not_tmp_20_1_3
  %tmp_78 = or i1 %tmp_19_1_3, %tmp_77
  %not_tmp_20_1_4 = icmp ne i16 %p_038_2_1_3, %Window_1_4_V_read_1
  %tmp_79 = and i1 %tmp_78, %not_tmp_20_1_4
  %tmp_80 = or i1 %tmp_19_1_4, %tmp_79
  %not_tmp_20_2 = icmp ne i16 %p_038_2_1_4, %Window_2_0_V_read_1
  %tmp_81 = and i1 %tmp_80, %not_tmp_20_2
  %tmp_82 = or i1 %tmp_19_2, %tmp_81
  %not_tmp_20_2_1 = icmp ne i16 %p_038_2_2, %Window_2_1_V_read_1
  %tmp_83 = and i1 %tmp_82, %not_tmp_20_2_1
  %tmp_84 = or i1 %tmp_19_2_1, %tmp_83
  %not_tmp_20_2_2 = icmp ne i16 %p_038_2_2_1, %Window_2_2_V_read_1
  %tmp_85 = and i1 %tmp_84, %not_tmp_20_2_2
  %tmp_86 = or i1 %tmp_19_2_2, %tmp_85
  %not_tmp_20_2_3 = icmp ne i16 %p_038_2_2_2, %Window_2_3_V_read_1
  %tmp_87 = and i1 %tmp_86, %not_tmp_20_2_3
  %tmp_88 = or i1 %tmp_19_2_3, %tmp_87
  %not_tmp_20_2_4 = icmp ne i16 %p_038_2_2_3, %Window_2_4_V_read_1
  %tmp_89 = and i1 %tmp_88, %not_tmp_20_2_4
  %tmp_90 = or i1 %tmp_19_2_4, %tmp_89
  %not_tmp_20_3 = icmp ne i16 %p_038_2_2_4, %Window_3_0_V_read_1
  %tmp_91 = and i1 %tmp_90, %not_tmp_20_3
  %tmp_92 = or i1 %tmp_19_3, %tmp_91
  %not_tmp_20_3_1 = icmp ne i16 %p_038_2_3, %Window_3_1_V_read_1
  %tmp_93 = and i1 %tmp_92, %not_tmp_20_3_1
  %tmp_94 = or i1 %tmp_19_3_1, %tmp_93
  %not_tmp_20_3_2 = icmp ne i16 %p_038_2_3_1, %Window_3_2_V_read_1
  %tmp_95 = and i1 %tmp_94, %not_tmp_20_3_2
  %tmp_96 = or i1 %tmp_19_3_2, %tmp_95
  %not_tmp_20_3_3 = icmp ne i16 %p_038_2_3_2, %Window_3_3_V_read_1
  %tmp_97 = and i1 %tmp_96, %not_tmp_20_3_3
  %tmp_98 = or i1 %tmp_19_3_3, %tmp_97
  %not_tmp_20_3_4 = icmp ne i16 %p_038_2_3_3, %Window_3_4_V_read_1
  %tmp_99 = and i1 %tmp_98, %not_tmp_20_3_4
  %tmp_100 = or i1 %tmp_19_3_4, %tmp_99
  %not_tmp_20_4 = icmp ne i16 %p_038_2_3_4, %Window_4_0_V_read_1
  %tmp_101 = and i1 %tmp_100, %not_tmp_20_4
  %tmp_102 = or i1 %tmp_19_4, %tmp_101
  %not_tmp_20_4_1 = icmp ne i16 %p_038_2_4, %Window_4_1_V_read_1
  %tmp_103 = and i1 %tmp_102, %not_tmp_20_4_1
  %tmp_104 = or i1 %tmp_19_4_1, %tmp_103
  %not_tmp_20_4_2 = icmp ne i16 %p_038_2_4_1, %Window_4_2_V_read_1
  %tmp_105 = and i1 %tmp_104, %not_tmp_20_4_2
  %tmp_106 = or i1 %tmp_19_4_2, %tmp_105
  %not_tmp_20_4_3 = icmp ne i16 %p_038_2_4_2, %Window_4_3_V_read_1
  %tmp_107 = and i1 %tmp_106, %not_tmp_20_4_3
  %tmp_108 = or i1 %tmp_19_4_3, %tmp_107
  %not_tmp_20_4_4 = icmp ne i16 %p_038_2_4_3, %Window_4_4_V_read_1
  %tmp_109 = and i1 %tmp_108, %not_tmp_20_4_4
  %tmp_5 = or i1 %tmp_19_4_4, %tmp_109
  %tmp42 = and i1 %tmp_61, %tmp35
  %tmp43 = and i1 %not_tmp_19_3_3, %not_tmp_19_3_4
  %tmp44 = and i1 %tmp43, %not_tmp_19_3_2
  %tmp45 = and i1 %tmp44, %tmp42
  %tmp46 = and i1 %not_tmp_19_4_1, %not_tmp_19_4_2
  %tmp47 = and i1 %tmp46, %not_tmp_19_4
  %tmp236_demorgan = or i1 %tmp_19_4_3, %tmp_19_4_4
  %tmp48 = xor i1 %tmp236_demorgan, true
  %tmp49 = and i1 %tmp_60, %tmp_5
  %tmp50 = and i1 %tmp49, %tmp48
  %tmp51 = and i1 %tmp50, %tmp47
  %or_cond4 = and i1 %tmp51, %tmp45
  %UnifiedRetVal = or i1 %or_cond2, %or_cond4
  ret i1 %UnifiedRetVal
}

!opencl.kernels = !{!0, !7, !11, !17, !17, !17, !21, !27, !30, !30, !21, !21, !33, !30, !30, !21, !21, !35, !37, !37, !37, !21, !21, !40, !40, !21, !42, !44, !47, !47, !30, !30, !21, !21, !53, !53, !55, !57, !57, !21, !59, !59, !61, !21, !62, !21, !21, !21, !64, !67, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21, !21}
!hls.encrypted.func = !{}
!llvm.map.gv = !{!69}

!0 = metadata !{null, metadata !1, metadata !2, metadata !3, metadata !4, metadata !5, metadata !6}
!1 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 1}
!2 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none"}
!3 = metadata !{metadata !"kernel_arg_type", metadata !"hls::stream<Response_And_Size> &", metadata !"hls::stream<axi_32_side_channel> &", metadata !"int*"}
!4 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !""}
!5 = metadata !{metadata !"kernel_arg_name", metadata !"responseStream", metadata !"keypointStream", metadata !"nKPCount"}
!6 = metadata !{metadata !"reqd_work_group_size", i32 1, i32 1, i32 1}
!7 = metadata !{null, metadata !8, metadata !2, metadata !9, metadata !4, metadata !10, metadata !6}
!8 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 0}
!9 = metadata !{metadata !"kernel_arg_type", metadata !"hls::stream<Response_And_Size> &", metadata !"int", metadata !"int"}
!10 = metadata !{metadata !"kernel_arg_name", metadata !"responseStream", metadata !"iRow", metadata !"iCol"}
!11 = metadata !{null, metadata !12, metadata !13, metadata !14, metadata !15, metadata !16, metadata !6}
!12 = metadata !{metadata !"kernel_arg_addr_space", i32 1}
!13 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none"}
!14 = metadata !{metadata !"kernel_arg_type", metadata !"Response_t [5]*"}
!15 = metadata !{metadata !"kernel_arg_type_qual", metadata !""}
!16 = metadata !{metadata !"kernel_arg_name", metadata !"Window"}
!17 = metadata !{null, metadata !18, metadata !13, metadata !19, metadata !15, metadata !20, metadata !6}
!18 = metadata !{metadata !"kernel_arg_addr_space", i32 0}
!19 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_fixed_base<16, 9, true, (enum ap_q_mode)1, (enum ap_o_mode)3, 0> &"}
!20 = metadata !{metadata !"kernel_arg_name", metadata !"op2"}
!21 = metadata !{null, metadata !22, metadata !23, metadata !24, metadata !25, metadata !26, metadata !6}
!22 = metadata !{metadata !"kernel_arg_addr_space"}
!23 = metadata !{metadata !"kernel_arg_access_qual"}
!24 = metadata !{metadata !"kernel_arg_type"}
!25 = metadata !{metadata !"kernel_arg_type_qual"}
!26 = metadata !{metadata !"kernel_arg_name"}
!27 = metadata !{null, metadata !18, metadata !13, metadata !28, metadata !15, metadata !29, metadata !6}
!28 = metadata !{metadata !"kernel_arg_type", metadata !"const struct ap_axiu<32, 1, 1, 1> &"}
!29 = metadata !{metadata !"kernel_arg_name", metadata !"din"}
!30 = metadata !{null, metadata !18, metadata !13, metadata !31, metadata !15, metadata !32, metadata !6}
!31 = metadata !{metadata !"kernel_arg_type", metadata !"int"}
!32 = metadata !{metadata !"kernel_arg_name", metadata !"val"}
!33 = metadata !{null, metadata !18, metadata !13, metadata !34, metadata !15, metadata !20, metadata !6}
!34 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<4> &"}
!35 = metadata !{null, metadata !18, metadata !13, metadata !36, metadata !15, metadata !20, metadata !6}
!36 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<1> &"}
!37 = metadata !{null, metadata !18, metadata !13, metadata !38, metadata !15, metadata !39, metadata !6}
!38 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_concat_ref<16, struct ap_int_base<16, false, true>, 16, struct ap_int_base<16, false, true> > &"}
!39 = metadata !{metadata !"kernel_arg_name", metadata !"ref"}
!40 = metadata !{null, metadata !18, metadata !13, metadata !31, metadata !15, metadata !41, metadata !6}
!41 = metadata !{metadata !"kernel_arg_name", metadata !"op"}
!42 = metadata !{null, metadata !18, metadata !13, metadata !43, metadata !15, metadata !20, metadata !6}
!43 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<32> &"}
!44 = metadata !{null, metadata !18, metadata !13, metadata !45, metadata !15, metadata !46, metadata !6}
!45 = metadata !{metadata !"kernel_arg_type", metadata !"ap_int_base<16, false> &"}
!46 = metadata !{metadata !"kernel_arg_name", metadata !"a2"}
!47 = metadata !{null, metadata !48, metadata !49, metadata !50, metadata !51, metadata !52, metadata !6}
!48 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0}
!49 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none"}
!50 = metadata !{metadata !"kernel_arg_type", metadata !"struct ap_int_base<16, false, true> &", metadata !"struct ap_int_base<16, false, true> &"}
!51 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !""}
!52 = metadata !{metadata !"kernel_arg_name", metadata !"bv1", metadata !"bv2"}
!53 = metadata !{null, metadata !18, metadata !13, metadata !31, metadata !15, metadata !54, metadata !6}
!54 = metadata !{metadata !"kernel_arg_name", metadata !"v"}
!55 = metadata !{null, metadata !18, metadata !13, metadata !31, metadata !15, metadata !56, metadata !6}
!56 = metadata !{metadata !"kernel_arg_name", metadata !"b"}
!57 = metadata !{null, metadata !18, metadata !13, metadata !31, metadata !15, metadata !58, metadata !6}
!58 = metadata !{metadata !"kernel_arg_name", metadata !"i_op"}
!59 = metadata !{null, metadata !18, metadata !13, metadata !60, metadata !15, metadata !41, metadata !6}
!60 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_fixed_base<32, 32, true, (enum ap_q_mode)5, (enum ap_o_mode)3, 0> &"}
!61 = metadata !{null, metadata !18, metadata !13, metadata !19, metadata !15, metadata !41, metadata !6}
!62 = metadata !{null, metadata !18, metadata !13, metadata !63, metadata !15, metadata !41, metadata !6}
!63 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_fixed<16, 9, (enum ap_q_mode)1, (enum ap_o_mode)3, 0> &"}
!64 = metadata !{null, metadata !18, metadata !13, metadata !65, metadata !15, metadata !66, metadata !6}
!65 = metadata !{metadata !"kernel_arg_type", metadata !"const struct Response_And_Size &"}
!66 = metadata !{metadata !"kernel_arg_name", metadata !""}
!67 = metadata !{null, metadata !18, metadata !13, metadata !68, metadata !15, metadata !20, metadata !6}
!68 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<12> &"}
!69 = metadata !{metadata !70, [1 x i32]* @llvm_global_ctors_0}
!70 = metadata !{metadata !71}
!71 = metadata !{i32 0, i32 31, metadata !72}
!72 = metadata !{metadata !73}
!73 = metadata !{metadata !"llvm.global_ctors.0", metadata !74, metadata !"", i32 0, i32 31}
!74 = metadata !{metadata !75}
!75 = metadata !{i32 0, i32 0, i32 1}
!76 = metadata !{metadata !77}
!77 = metadata !{i32 0, i32 15, metadata !78}
!78 = metadata !{metadata !79}
!79 = metadata !{metadata !"responseStream.V.resp.V", metadata !74, metadata !"int16", i32 0, i32 15}
!80 = metadata !{metadata !81}
!81 = metadata !{i32 0, i32 11, metadata !82}
!82 = metadata !{metadata !83}
!83 = metadata !{metadata !"responseStream.V.size.V", metadata !74, metadata !"uint12", i32 0, i32 11}
!84 = metadata !{metadata !85}
!85 = metadata !{i32 0, i32 31, metadata !86}
!86 = metadata !{metadata !87}
!87 = metadata !{metadata !"keypointStream.V.data.V", metadata !74, metadata !"uint32", i32 0, i32 31}
!88 = metadata !{metadata !89}
!89 = metadata !{i32 0, i32 3, metadata !90}
!90 = metadata !{metadata !91}
!91 = metadata !{metadata !"keypointStream.V.keep.V", metadata !74, metadata !"uint4", i32 0, i32 3}
!92 = metadata !{metadata !93}
!93 = metadata !{i32 0, i32 3, metadata !94}
!94 = metadata !{metadata !95}
!95 = metadata !{metadata !"keypointStream.V.strb.V", metadata !74, metadata !"uint4", i32 0, i32 3}
!96 = metadata !{metadata !97}
!97 = metadata !{i32 0, i32 0, metadata !98}
!98 = metadata !{metadata !99}
!99 = metadata !{metadata !"keypointStream.V.user.V", metadata !74, metadata !"uint1", i32 0, i32 0}
!100 = metadata !{metadata !101}
!101 = metadata !{i32 0, i32 0, metadata !102}
!102 = metadata !{metadata !103}
!103 = metadata !{metadata !"keypointStream.V.last.V", metadata !74, metadata !"uint1", i32 0, i32 0}
!104 = metadata !{metadata !105}
!105 = metadata !{i32 0, i32 0, metadata !106}
!106 = metadata !{metadata !107}
!107 = metadata !{metadata !"keypointStream.V.id.V", metadata !74, metadata !"uint1", i32 0, i32 0}
!108 = metadata !{metadata !109}
!109 = metadata !{i32 0, i32 0, metadata !110}
!110 = metadata !{metadata !111}
!111 = metadata !{metadata !"keypointStream.V.dest.V", metadata !74, metadata !"uint1", i32 0, i32 0}
!112 = metadata !{metadata !113}
!113 = metadata !{i32 0, i32 31, metadata !114}
!114 = metadata !{metadata !115}
!115 = metadata !{metadata !"nKPCount", metadata !74, metadata !"int", i32 0, i32 31}
