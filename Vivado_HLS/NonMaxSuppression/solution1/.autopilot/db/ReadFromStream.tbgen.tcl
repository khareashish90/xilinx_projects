set moduleName ReadFromStream
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set C_modelName {ReadFromStream}
set C_modelType { int 16 }
set C_modelArgList {
	{ responseStream_V_res int 16 regular {axi_s 0 volatile  { responseStream_V_res Data } }  }
	{ responseStream_V_siz int 16 regular {axi_s 0 volatile  { responseStream_V_siz Data } }  }
	{ iRow int 11 regular  }
	{ iCol int 11 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "responseStream_V_res", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "responseStream_V_siz", "interface" : "axis", "bitwidth" : 16, "direction" : "READONLY"} , 
 	{ "Name" : "iRow", "interface" : "wire", "bitwidth" : 11, "direction" : "READONLY"} , 
 	{ "Name" : "iCol", "interface" : "wire", "bitwidth" : 11, "direction" : "READONLY"} , 
 	{ "Name" : "ap_return", "interface" : "wire", "bitwidth" : 16} ]}
# RTL Port declarations: 
set portNum 18
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ responseStream_V_res_TVALID sc_in sc_logic 1 invld 0 } 
	{ ap_ce sc_in sc_logic 1 ce -1 } 
	{ responseStream_V_res_TDATA sc_in sc_lv 16 signal 0 } 
	{ responseStream_V_res_TREADY sc_out sc_logic 1 inacc 0 } 
	{ responseStream_V_siz_TDATA sc_in sc_lv 16 signal 1 } 
	{ responseStream_V_siz_TVALID sc_in sc_logic 1 invld 1 } 
	{ responseStream_V_siz_TREADY sc_out sc_logic 1 inacc 1 } 
	{ iRow sc_in sc_lv 11 signal 2 } 
	{ iCol sc_in sc_lv 11 signal 3 } 
	{ ap_return sc_out sc_lv 16 signal -1 } 
	{ responseStream_V_res_TDATA_blk_n sc_out sc_logic 1 signal -1 } 
	{ responseStream_V_siz_TDATA_blk_n sc_out sc_logic 1 signal -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "responseStream_V_res_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "responseStream_V_res", "role": "TVALID" }} , 
 	{ "name": "ap_ce", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "ce", "bundle":{"name": "ap_ce", "role": "default" }} , 
 	{ "name": "responseStream_V_res_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "responseStream_V_res", "role": "TDATA" }} , 
 	{ "name": "responseStream_V_res_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "responseStream_V_res", "role": "TREADY" }} , 
 	{ "name": "responseStream_V_siz_TDATA", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "responseStream_V_siz", "role": "TDATA" }} , 
 	{ "name": "responseStream_V_siz_TVALID", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "invld", "bundle":{"name": "responseStream_V_siz", "role": "TVALID" }} , 
 	{ "name": "responseStream_V_siz_TREADY", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "inacc", "bundle":{"name": "responseStream_V_siz", "role": "TREADY" }} , 
 	{ "name": "iRow", "direction": "in", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "iRow", "role": "default" }} , 
 	{ "name": "iCol", "direction": "in", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "iCol", "role": "default" }} , 
 	{ "name": "ap_return", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "ap_return", "role": "default" }} , 
 	{ "name": "responseStream_V_res_TDATA_blk_n", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "responseStream_V_res_TDATA_blk_n", "role": "default" }} , 
 	{ "name": "responseStream_V_siz_TDATA_blk_n", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "responseStream_V_siz_TDATA_blk_n", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "ReadFromStream",
		"VariableLatency" : "0",
		"AlignedPipeline" : "1",
		"UnalignedPipeline" : "0",
		"ProcessNetwork" : "0",
		"Combinational" : "0",
		"ControlExist" : "1",
		"Port" : [
		{"Name" : "responseStream_V_res", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_res_TDATA_blk_n", "Type" : "RtlPort"}]},
		{"Name" : "responseStream_V_siz", "Type" : "Axis", "Direction" : "I",
			"BlockSignal" : [
			{"Name" : "responseStream_V_siz_TDATA_blk_n", "Type" : "RtlPort"}]},
		{"Name" : "iRow", "Type" : "None", "Direction" : "I"},
		{"Name" : "iCol", "Type" : "None", "Direction" : "I"}]}]}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "1", "Max" : "1"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set Spec2ImplPortList { 
	responseStream_V_res { axis {  { responseStream_V_res_TVALID in_vld 0 1 }  { responseStream_V_res_TDATA in_data 0 16 }  { responseStream_V_res_TREADY in_acc 1 1 } } }
	responseStream_V_siz { axis {  { responseStream_V_siz_TDATA in_data 0 16 }  { responseStream_V_siz_TVALID in_vld 0 1 }  { responseStream_V_siz_TREADY in_acc 1 1 } } }
	iRow { ap_none {  { iRow in_data 0 11 } } }
	iCol { ap_none {  { iCol in_data 0 11 } } }
}
