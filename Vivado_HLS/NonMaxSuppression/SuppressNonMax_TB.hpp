/*

 * File:	SuppressNonMax.hpp
 * Date:	October 25, 2017
 * By:		Ashish Khare

 * Description:

 */



#ifndef _SUPPRESS_NONMAX_HPP_
#define _SUPPRESS_NONMAX_HPP_

#include "SuppressNonMax.hpp"


#define NQUADS 			15
#define NPAIRS			10

#define QUADS_SIZES		{1,2,3,4,6,8,11,12,16,22,23,32,45,46,64}
#define QUADS_MSIZE		64
#define SIZES_0			{{64,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{46,1,0},{45,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{32,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{23,1,0},{22,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{16,1,0},{0,0,0},{0,0,0},{0,0,0},{12,1,0},{11,1,0},{0,0,0},{0,0,0},{8,1,0},{0,0,0},{6,1,0},{0,0,0},{4,1,0},{3,1,0},{2,1,0},{1,1,0},{0,0,0},{1,1,0},{2,1,0},{3,1,0},{4,1,0},{0,0,0},{6,1,0},{0,0,0},{8,1,0},{0,0,0},{0,0,0},{11,1,0},{12,1,0},{0,0,0},{0,0,0},{0,0,0},{16,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{22,1,0},{23,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{32,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{45,1,0},{46,1,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{64,1,0},{0,0,1}}

#define PAIRS_QIDS		{{1,0},{3,1},{4,2},{5,3},{7,4},{8,5},{9,6},{11,8},{13,10},{14,11}}


#define PAIRS_COEF		{{1.f/16,1.f/9},{1.f/56,1.f/25},{1.f/120,1.f/49},{1.f/208,1.f/81},\
						{1.f/456,1.f/169},{1.f/800,1.f/289},{1.f/1496,1.f/529},{1.f/3136,1.f/1089},\
						{1.f/6440,1.f/2209},{1.f/12416,1.f/4225}}


#define A_OFS(qid)		  ImageCols*quadSizes[k] + quadSizes[k]
#define B_OFS(qid)		  ImageCols*quadSizes[k] - quadSizes[k] -1
#define C_OFS(qid)		 -ImageCols*(quadSizes[k]+1) + quadSizes[k]
#define D_OFS(qid)		 -ImageCols*(quadSizes[k]+1) - quadSizes[k] -1

typedef ap_uint<4> Size_id_t;
typedef ap_fixed<18,0,AP_RND_ZERO> Inv_size_t;

typedef ap_uint<8> int_8_Pixel;
typedef ap_uint<32> int_32_Pixel;

void ComputeIntegralImage(int_8_Pixel srcImage[ImageRows*ImageCols], int_32_Pixel integralImage[ImageRows*ImageCols]);
void ComputeResponse(int_32_Pixel in[ImageRows*ImageCols], Response_And_Size_SideChannel out[ImageRows*ImageCols]);
#endif
