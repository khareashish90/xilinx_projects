/*
 * File:	SuppressNonMax.cpp
 * Date:	October 22, 2017
 * By:		Ashish Khare

 * Description:	This file contains implementations of methods
 * defined in which is defined in file SuppressNonMax.hpp

*/

#include "SuppressNonMax.hpp"


void SuppressNonMax(hls::stream<Response_And_Size_SideChannel> &responseStream, hls::stream<Keypoint_SideChannel> &keypointStream, int* nKPCount  )
{

#pragma HLS INTERFACE ap_none register port=nKPCount
#pragma HLS INTERFACE s_axilite port=return bundle=control
#pragma HLS INTERFACE axis port=responseStream
#pragma HLS INTERFACE axis port=keypointStream


		Response_And_Size Window[Kernel_Size][Kernel_Size];// Sliding window
#pragma HLS ARRAY_PARTITION variable=Window complete dim=0

		Response_And_Size ResponseBuf[Kernel_Size][ImageCols];
#pragma HLS ARRAY_PARTITION variable = ResponseBuf complete dim=1

		Response_And_Size rightCol[Kernel_Size];
#pragma HLS ARRAY_PARTITION variable=rightCol dim=0
 // right-most, incoming column

		int nKeypointsDetected = 0 ;

	L1:	for(int iRow = 0 ; iRow < ImageRows +Filter_Offs; iRow++ )
		{
#pragma HLS LATENCY min=0

		// HLS Latency pragma helps Vivado HLS make better timing estimates

		L2:	for(int iCol = 0 ; iCol < ImageCols+Filter_Offs; iCol++ )
			{
#pragma HLS PIPELINE

			Response_And_Size  pixelIn;
			Response_And_Size_SideChannel pixelInSideChannel;

			if(iRow<ImageRows && iCol < ImageCols)
			{
				pixelInSideChannel = responseStream.read();
				pixelIn = pixelInSideChannel.data;
			}
			else
			{
				pixelIn.resp = 0;
				pixelIn.size = 0;
				pixelIn.null = 0 ;
			}


			ShiftImageBuffer(ResponseBuf, pixelIn, rightCol, iRow, iCol);


			// Horizontal Sliding Window:
			for(unsigned char ii = 0; ii < Kernel_Size; ii++)
				for(unsigned char jj = 0; jj < Kernel_Size-1; jj++)
					Window[ii][jj] = Window[ii][jj+1];

			for(unsigned char ii = 0; ii < Kernel_Size; ii++)
				Window[ii][Kernel_Size-1] = rightCol[ii];


			// Suppress non-max responses and put keypoints on output stream //
			if((iRow >= Filter_Offs && iCol >= Filter_Offs ))
			{
				Keypoint_SideChannel keypointSideChannel ;	//Keypoint with side channel information
				bool bKeypointDetected  ;

				Response_t respKeypoint = Window[Kernel_Size/2][Kernel_Size/2].resp;
				uint16_t sizeKeypoint = (uint16_t)Window[Kernel_Size/2][Kernel_Size/2].size ;

				bKeypointDetected = IsLocalExtremum(Window);

				if( (bKeypointDetected || ((iRow == ImageRows +Filter_Offs-1)&&(iCol == ImageCols+Filter_Offs-1))))
				{
					ap_uint<16> keyPointOut_Y = (iRow - Filter_Offs);
					ap_uint<16> keyPointOut_X = (iCol - Filter_Offs);
					nKeypointsDetected++ ;

					keypointSideChannel.data.x = keyPointOut_X;
					keypointSideChannel.data.y = keyPointOut_Y ;
					keypointSideChannel.data.resp = respKeypoint;
					keypointSideChannel.data.size = sizeKeypoint;
					keypointSideChannel.last = ((iRow == ImageRows +Filter_Offs-1)&&(iCol == ImageCols+Filter_Offs-1))?1:0 ;
					keypointSideChannel.dest = 0 ;
					keypointSideChannel.id = 0;
					keypointSideChannel.keep = -1;
					keypointSideChannel.strb = -1;

					keypointStream.write(keypointSideChannel);
				}
			}
		}//end of L2
	}// end of L1

	*nKPCount = nKeypointsDetected-1;
}




void ShiftImageBuffer( Response_And_Size ResponseBuf[Kernel_Size][ImageCols], Response_And_Size  pix, Response_And_Size rightCol[Kernel_Size],int row, int col)
{

	if(col < ImageCols)
	{
		for(unsigned char ii = 0; ii < Kernel_Size-1; ii++)
		{
			rightCol[ii]=ResponseBuf[ii][col]=ResponseBuf[ii+1][col];
		}
		rightCol[Kernel_Size-1] = ResponseBuf[Kernel_Size-1][col] = pix;
	}
	else
	{
		for(unsigned char ii = 0; ii < Kernel_Size; ii++)
		{
			rightCol[ii].resp = 0;
			rightCol[ii].size = 0;
			rightCol[ii].null = 0;
		}
	}
}

bool IsLocalExtremum(Response_And_Size Window[Kernel_Size][Kernel_Size])
{
	int iMaxPtRow, iMaxPtCol ;
	int iMinPtRow, iMinPtCol ;
	Response_t respMax = Response_Threshold;
	Response_t respMin = -Response_Threshold;
	int nMaxCount, nMinCount ;
	Index_t Size ;

	Size = Window[Filter_Offs][Filter_Offs].size;

	for(int iWinRow = 0 ; iWinRow < Kernel_Size; iWinRow++)
	{
		for(int iWinCol = 0 ; iWinCol < Kernel_Size ; iWinCol++)
		{
//Parent loop is pipelined. So we don't need pipeline pragma here

			Response_t respValue =  Window[iWinRow][iWinCol].resp;

			if( respValue  > respMax )
			{
				respMax = respValue;
				iMaxPtRow = iWinRow;
				iMaxPtCol = iWinCol;
				nMaxCount = 0 ;
			}
			else if(respValue == respMax)
			{
				nMaxCount=1;
			}

			if(respValue < respMin)
			{
				respMin = respValue;
				iMinPtCol = iWinCol;
				iMinPtRow = iWinRow;
				nMinCount=0 ;
			}
			else if(respValue == respMin)
			{
				nMinCount=1 ;
			}
		}
	}

	//Rejecting very small or very large features
	if(Size <= Min_Size )
		return false;
	if(iMaxPtCol == Kernel_Size/2 && iMaxPtRow ==Kernel_Size/2 && nMaxCount==0)
		return true ;
	else if (iMinPtCol == Kernel_Size/2 && iMinPtRow ==	Kernel_Size/2 && nMinCount==0)
		return true ;
	else
		return false ;


}





