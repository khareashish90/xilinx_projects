#include "ComputeResopnse.hpp"
#include "ComputeResopnse_Parameters.hpp"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ap_fixed.h>

#define ACURENCY_REQUIERED	0.05

template <typename T>
T abs(T a){
#pragma HLS INLINE
	if(a>=0)	return a;
	else		return -a;
}

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////
/// BRAM Logic - Line CLASS
///////////////////////////////////////////////////////////////////////////////////////////

inline void Line::new_pixel(void){
#pragma HLS INLINE
	pLeft++;
	pRight++;
	pStore++;
}

inline void Line::new_line(Line_values_t size){
#pragma HLS INLINE
	pLeft = -size.size;
	pRight = size.size+1;
	pStore = 0;
	readEnable = size.readEnable;
	writeEnable = size.writeEnable;
}

inline void Line::process(Integral_image_pixel& dLeft, Integral_image_pixel& dRight, Integral_image_pixel dIn){
#pragma HLS INLINE
	if(readEnable){
		dLeft = line[pLeft];
		dRight = line[pRight];
	}else if(writeEnable){
		line[pStore] = dIn;
		dLeft = 0;
		dRight = 0;
	}else{
		dLeft = 0;
		dRight = 0;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
/// Arithmetic Logic
///////////////////////////////////////////////////////////////////////////////////////////

inline void compute_quads(Substracted_integral_image_pixels iiValues[NQUADS][2], Integral_image_pixel responses[NQUADS]){
#pragma HLS INLINE
	for(Index_t k = 0; k<NQUADS; k++){
#pragma HLS UNROLL
		Substracted_integral_image_pixels a = iiValues[k][1];
		Substracted_integral_image_pixels b = iiValues[k][0];

		responses[k] = (a - b);
	}
}

inline void compute_pairs(const Size_id_t pairs[NPAIRS][2], const Inv_size_t invSizes[NPAIRS][2],
		Integral_image_pixel vals[NQUADS], Response_t pairResponses[NPAIRS]){
#pragma HLS INLINE
	for(Index_t i=0; i<NPAIRS; i++){
#pragma HLS UNROLL
		Integral_image_pixel inner_sum = vals[pairs[i][1]];
		Integral_image_pixel outer_sum = vals[pairs[i][0]] - inner_sum;

		Response_t inner = inner_sum*invSizes[i][1];
		Response_t outer = outer_sum*invSizes[i][0];

		pairResponses[i] = inner - outer;
	}
}

void get_max(Response_t responsesIn[NPAIRS], const Size_id_t pairs[NPAIRS][2],
					Response_t& maxResponse, Size_id_t& maxSizeId){

#pragma HLS LATENCY min=20
	//Response_t uresp[GETM_NSTAGES+1][NPAIRS]; // absolute value of response
	Response_t sresp[GETM_NSTAGES+1][NPAIRS]; // arithmetic value (signed)
	Size_t sizes[GETM_NSTAGES+1][NPAIRS]; // size


	for(Index_t k=0; k<NPAIRS; k++){
		//uresp[0][k] = tabs<Response_t>(responsesIn[k]); // DO NOT USE hls::math::fabsf()
		sresp[0][k] = responsesIn[k];
		sizes[0][k] = pairs[k][0];
	}

	for(Index_t i=0; i<GETM_NSTAGES; i++){
		for(Index_t j=0; j<NPAIRS; j+=(2<<i)){
#pragma HLS UNROLL
#pragma HLS LATENCY min=27
			Index_t off = j+(1<<i);
			if(off >= NPAIRS){
				//uresp[i+1][j] = uresp[i][j];
				sresp[i+1][j] = sresp[i][j];
				sizes[i+1][j] = sizes[i][j];
			}else{
				//Response_t diff = abs<Response_t>(sresp[i][off]) - abs<Response_t>(sresp[i][j]);
				//if(diff >= 0){
				if(abs<Response_t>(sresp[i][off]) >= abs<Response_t>(sresp[i][j])){
					//uresp[i+1][j] = uresp[i][off];
					sresp[i+1][j] = sresp[i][off];
					sizes[i+1][j] = sizes[i][off];
				}else{
					//uresp[i+1][j] = uresp[i][j];
					sresp[i+1][j] = sresp[i][j];
					sizes[i+1][j] = sizes[i][j];

				}
			}
		}
	}
	maxResponse = sresp[GETM_NSTAGES][0];
	maxSizeId = sizes[GETM_NSTAGES][0];
}


///////////////////////////////////////////////////////////////////////////////////////////
/// BRAM Logic - Input
///////////////////////////////////////////////////////////////////////////////////////////


inline void shift_sizes(Line_values_t sizes[NLINES]){
#pragma HLS INLINE
	Line_values_t tmp;
	tmp.size = sizes[NLINES-1].size;
	tmp.readEnable = sizes[NLINES-1].readEnable;
	tmp.writeEnable = sizes[NLINES-1].writeEnable;

	for(Index_t i =(NLINES-1); i != 0; i--){
#pragma HLS UNROLL
		sizes[i].size = sizes[i-1].size;
		sizes[i].readEnable = sizes[i-1].readEnable;
		sizes[i].writeEnable = sizes[i-1].writeEnable;
	}
	sizes[0].size = tmp.size;
	sizes[0].readEnable = tmp.readEnable;
	sizes[0].writeEnable = tmp.writeEnable;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// BRAM Logic - Output
///////////////////////////////////////////////////////////////////////////////////////////

inline void substract_data_in_lines(Integral_image_pixel dataIn[2*NLINES],
			Substracted_integral_image_pixels dataOut[NLINES]){
#pragma HLS INLINE
	for(int k=0; k<NLINES; k++){
#pragma HLS UNROLL
		Integral_image_pixel a = dataIn[2*k+1];
		Integral_image_pixel b = dataIn[2*k+0];
		Integral_image_pixel tmp = dataIn[2*k+1] - dataIn[2*k+0];
		Substracted_integral_image_pixels tmp2 = tmp;
		dataOut[k] = tmp;
		/*if((dataIn[2*k+1] - dataIn[2*k+0]) !=  dataOut[k]){
			printf("err");
		}*/
	}
}


inline void lines_to_quads(Index_t stage, Substracted_integral_image_pixels dataIn[NLINES],
		Substracted_integral_image_pixels dataOut[NQUADS][2]){
#pragma HLS PIPELINE II=1
	Integral_image_pixel dataTmp[L2Q_NSTAGES+1][NLINES];
#pragma HLS ARRAY_PARTITION variable=dataTmp complete dim=1
	Integral_image_pixel shiftTmp[L2Q_NSTAGES][L2Q_SHIFT_OVERLAP_SIZE];
#pragma HLS ARRAY_PARTITION variable=shiftTmp complete dim=1
	Index_t stageId, stageMask;

	// Move input data to the main buffer (to be used in the main loop)
	for(int i=0; i<NLINES; i++){
		dataTmp[0][i] = dataIn[i];
	}

	// A Stage is a circular multiplexer. 1 input 2 outputs same command
	// We can do a power of two shifting or no shifting.
	// The power of two depends on the index of the loop.
	// Power of two :  stageMask, Index id stageId
	for(stageId=0, stageMask=1; stageId<L2Q_NSTAGES; stageId++, stageMask<<=1){
		if(stage & stageMask){
			// Save data which will be overlapped
			for(int i=0; i<(stageMask); i++){
				shiftTmp[stageId][i] = dataTmp[stageId][i];
			}

			// Shift the data - normal case
			for(int i=0; i<(NLINES-stageMask); i++){
				dataTmp[stageId+1][i] = dataTmp[stageId][i+stageMask];
			}

			// Overlapped data
			for(int i=0; i<stageMask; i++){
				dataTmp[stageId+1][NLINES-stageMask+i] = shiftTmp[stageId][i];
			}
		}else{
			for(int i=0; i<NLINES; i++){
				dataTmp[stageId+1][i] = dataTmp[stageId][i];
			}
		}
	}

	Index_t lines0ToQuads[2*NQUADS] = LINES0_QUADS;

	//Last Stage
	for(int i=0; i<NQUADS; i++){
		dataOut[i][0] = dataTmp[L2Q_NSTAGES][lines0ToQuads[2*i+0]];
		dataOut[i][1] = dataTmp[L2Q_NSTAGES][lines0ToQuads[2*i+1]];
	}


	/*for(int i=0; i<2*NLINES; i++){
		std::cout << dataTmp[stageId][i] << "\t";
	}std::cout << "\n";*/

}


///////////////////////////////////////////////////////////////////////////////////////////
/// Debug functions
///////////////////////////////////////////////////////////////////////////////////////////

void display_lines(Line lines[NLINES]){
	for(Index_t k=0; k<NLINES; k++){
		for(Index_t l=0; l<WIDTH; l++){
			cout << "\t" << lines[k].line[l];
		}
		cout << endl;
	}
	cout << endl;
}

void display_sizes(Index_t sizes[NLINES]){
	for(Index_t k=0; k<NLINES; k++){
		cout << "\t" << sizes[k];
	}
	cout << endl;
}
/*
void display_quads(Integral_image_pixel iiValues[4*NQUADS]){
	for(Index_t k=0; k<NQUADS; k++){
		cout << iiValues[4*k+1] << "\t"
			 << iiValues[4*k+0] << "\t"
			 << iiValues[4*k+3] << "\t"
			 << iiValues[4*k+2] << "\t" << endl;
	}
}*/

void display_chosen_values(int i, Index_t maxSize, Response_t maxResponse){
	if(((i%WIDTH) >= 2)&&((i%WIDTH) < (WIDTH-2)))
		cout << i << "\t" << maxSize << "\t:" << maxResponse << endl;
}

void debug__reset_lines(Line lines[NLINES]){
	for(int j=0; j<NLINES; j++){
		for(int i=0; i<WIDTH; i++){
			lines[j].line[i] = 0;//i + j*WIDTH;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
/// Top functions
///////////////////////////////////////////////////////////////////////////////////////////

inline void new_lines(Line lines[NLINES], Line_values_t sizes[NLINES]){
#pragma HLS INLINE
	for(Index_t j=0; j<NLINES; j++){
#pragma HLS UNROLL
		lines[j].new_line(sizes[j]);
	}
}

inline void new_pixels(Line lines[NLINES]){
#pragma HLS INLINE
	for(Index_t k=0; k<NLINES; k++)
#pragma HLS UNROLL
		lines[k].new_pixel();
}

inline void read_write_lines(Line lines[NLINES], Integral_image_pixel incommingData,
		Integral_image_pixel dataFromLines[2*NLINES]){
#pragma HLS INLINE
	bram_loop: for(Index_t k=0, l=0; k<NLINES; k++, l+=2){
	#pragma HLS UNROLL
		lines[k].process(dataFromLines[l], dataFromLines[l+1], incommingData);
	}
}

#define INC_LINEID(lineId)	 if(lineId==(NLINES-1)) lineId=0; else lineId++;




void ComputeResponse(Input_stream& iiInput, Output_stream& respAndSizeOutput){
//#pragma HLS INTERFACE s_axilite port=return //clock=control name=control
#pragma HLS INTERFACE s_axilite port=return bundle=control
#pragma HLS INTERFACE axis register both port=respAndSizeOutput name=Response_And_Size
#pragma HLS INTERFACE axis register both port=iiInput name=Integral_Image

	int mode = ATOMIC_MODE;										//Block mode


	Line lines[NLINES];
											//BRAM + logic
#pragma HLS ARRAY_PARTITION variable=lines complete dim=1

	Line_values_t sizes[NLINES] = SIZES_0;						//Line shifting values (size, WE, RE)
#pragma HLS ARRAY_PARTITION variable=sizes complete dim=1


//Wires
	Integral_image_pixel iiValuesFromLines[2*NLINES];
			//Wires: II values from lines/BRAMs
#pragma HLS ARRAY_PARTITION variable=iiValuesFromLines complete dim=1

	Substracted_integral_image_pixels iiLineDifferenceFromLines[NLINES];
#pragma HLS ARRAY_PARTITION variable=iiLineDifferenceFromLines complete dim=1

	Substracted_integral_image_pixels iiLineDifferenceToQuads[NQUADS][2];
#pragma HLS ARRAY_PARTITION variable=iiLineDifferenceToQuads complete dim=1

	Integral_image_pixel quadResponses[NQUADS];							//Wires: Quad responses (A-B-C+D)
#pragma HLS ARRAY_PARTITION variable=quadResponses complete dim=1

	Response_t pairResponses[NPAIRS];							//Wires: Pair responses (Q*C-q*c)
#pragma HLS ARRAY_PARTITION variable=pairResponses complete dim=1

	Size_t maxSize;
	Size_id_t maxSizeId;
	Response_t maxResponse;


//Input / Output wires
	Integral_image_pixel dIn;
	Response_And_Size dOut;
	AXI_Response_And_Size axistream;
	axistream.data.null = 0;


//Constants
	const Size_t quadSizes[NQUADS] = QUADS_SIZES;						//Quad Sizes
#pragma HLS ARRAY_PARTITION variable=quadSizes complete dim=1

	const Inv_size_t pairsCoef[NPAIRS][2] = PAIRS_COEF;					//Inv area (for pairs)
#pragma HLS ARRAY_PARTITION variable=pairsCoef complete dim=1

	const Size_id_t pairs[NPAIRS][2] = PAIRS_QIDS;						//Pairs Quad ID
#pragma HLS ARRAY_PARTITION variable=pairs complete dim=1


	Index_t lineId = 0;

	//debug__reset_lines(lines);

	row_loop: for(Index_t j=0; j<(HEIGHT+((mode == ATOMIC_MODE)?(QUADS_MSIZE+1):0)); j++){
#pragma HLS LOOP_FLATTEN off

		//display_sizes(sizes);
		new_lines(lines, sizes);

		col_loop: for(Index_t i=0; i<WIDTH; i++){
#pragma HLS PIPELINE II=1

			//read value from the input axistream
			if(!((mode == ATOMIC_MODE)&&(j>(HEIGHT-1)))){
				iiInput >> dIn;
			}else{//if last line, process without reading
				dIn = 0; //optional
			}


			//BRAM Logic
			read_write_lines(lines, dIn, iiValuesFromLines);
			substract_data_in_lines(iiValuesFromLines, iiLineDifferenceFromLines);
			lines_to_quads(lineId, iiLineDifferenceFromLines, iiLineDifferenceToQuads);

			//Arithmetic Logic
			compute_quads(iiLineDifferenceToQuads, quadResponses);
			compute_pairs(pairs, pairsCoef, quadResponses, pairResponses);
			get_max(pairResponses, pairs, maxResponse, maxSizeId);

			//display_chosen_values(j*WIDTH + i, maxSize, maxResponse);


			//write value to the output axistream
			if(j<(QUADS_MSIZE+1)){
				//Skip sending over the first lines iterations.
			}
			else if( (i<QUADS_MSIZE) || (i>(WIDTH-QUADS_MSIZE-1)) 	/* left || right border*/
					|| (j<(2*QUADS_MSIZE+1)) 					  	/* top case */
					|| (j>(HEIGHT))){ 								/* bottom  case */
				//Borders, send zeros
				axistream.user = (i==0)?1:0;
				axistream.last = ((i==(WIDTH-1))&&(j==(HEIGHT+QUADS_MSIZE+1-1)))?1:0;
				axistream.data.resp = 0;
				axistream.data.size = 0;
				respAndSizeOutput << axistream;
			}else{
				//if(maxSize != 1) printf("Yolo\n");
				maxSize = quadSizes[maxSizeId];
				axistream.user = 0;
				axistream.last = 0;
				axistream.data.resp = maxResponse;
				axistream.data.size = maxSize;
				respAndSizeOutput << axistream;
			}

			new_pixels(lines);
		}

		INC_LINEID(lineId);
		shift_sizes(sizes);

	}

	return;

}
