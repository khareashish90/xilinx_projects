#include "ComputeResponse.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void ComputeResponse::thread_hdltv_gen() {
    const char* dump_tv = std::getenv("AP_WRITE_TV");
    if (!(dump_tv && string(dump_tv) == "on")) return;

    wait();

    mHdltvinHandle << "[ " << endl;
    mHdltvoutHandle << "[ " << endl;
    int ap_cycleNo = 0;
    while (1) {
        wait();
        const char* mComma = ap_cycleNo == 0 ? " " : ", " ;
        mHdltvinHandle << mComma << "{"  <<  " \"ap_rst_n\" :  \"" << ap_rst_n.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"Integral_Image_V_V_TDATA\" :  \"" << Integral_Image_V_V_TDATA.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"Integral_Image_V_V_TVALID\" :  \"" << Integral_Image_V_V_TVALID.read() << "\" ";
        mHdltvoutHandle << mComma << "{"  <<  " \"Integral_Image_V_V_TREADY\" :  \"" << Integral_Image_V_V_TREADY.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TDATA\" :  \"" << Response_And_Size_TDATA.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TVALID\" :  \"" << Response_And_Size_TVALID.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"Response_And_Size_TREADY\" :  \"" << Response_And_Size_TREADY.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TKEEP\" :  \"" << Response_And_Size_TKEEP.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TSTRB\" :  \"" << Response_And_Size_TSTRB.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TUSER\" :  \"" << Response_And_Size_TUSER.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TLAST\" :  \"" << Response_And_Size_TLAST.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TID\" :  \"" << Response_And_Size_TID.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"Response_And_Size_TDEST\" :  \"" << Response_And_Size_TDEST.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_AWVALID\" :  \"" << s_axi_control_AWVALID.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_AWREADY\" :  \"" << s_axi_control_AWREADY.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_AWADDR\" :  \"" << s_axi_control_AWADDR.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_WVALID\" :  \"" << s_axi_control_WVALID.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_WREADY\" :  \"" << s_axi_control_WREADY.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_WDATA\" :  \"" << s_axi_control_WDATA.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_WSTRB\" :  \"" << s_axi_control_WSTRB.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_ARVALID\" :  \"" << s_axi_control_ARVALID.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_ARREADY\" :  \"" << s_axi_control_ARREADY.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_ARADDR\" :  \"" << s_axi_control_ARADDR.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_RVALID\" :  \"" << s_axi_control_RVALID.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_RREADY\" :  \"" << s_axi_control_RREADY.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_RDATA\" :  \"" << s_axi_control_RDATA.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_RRESP\" :  \"" << s_axi_control_RRESP.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_BVALID\" :  \"" << s_axi_control_BVALID.read() << "\" ";
        mHdltvinHandle << " , " <<  " \"s_axi_control_BREADY\" :  \"" << s_axi_control_BREADY.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"s_axi_control_BRESP\" :  \"" << s_axi_control_BRESP.read() << "\" ";
        mHdltvoutHandle << " , " <<  " \"interrupt\" :  \"" << interrupt.read() << "\" ";
        mHdltvinHandle << "}" << std::endl;
        mHdltvoutHandle << "}" << std::endl;
        ap_cycleNo++;
    }
}

}

