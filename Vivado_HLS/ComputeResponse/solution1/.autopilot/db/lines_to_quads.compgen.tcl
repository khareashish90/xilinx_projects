# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 0 \
    name stage_V \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_stage_V \
    op interface \
    ports { stage_V { I 12 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 1 \
    name dataIn_0_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_0_V_read \
    op interface \
    ports { dataIn_0_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 2 \
    name dataIn_1_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_1_V_read \
    op interface \
    ports { dataIn_1_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 3 \
    name dataIn_2_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_2_V_read \
    op interface \
    ports { dataIn_2_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 4 \
    name dataIn_3_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_3_V_read \
    op interface \
    ports { dataIn_3_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 5 \
    name dataIn_4_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_4_V_read \
    op interface \
    ports { dataIn_4_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 6 \
    name dataIn_5_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_5_V_read \
    op interface \
    ports { dataIn_5_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 7 \
    name dataIn_6_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_6_V_read \
    op interface \
    ports { dataIn_6_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 8 \
    name dataIn_7_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_7_V_read \
    op interface \
    ports { dataIn_7_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 9 \
    name dataIn_8_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_8_V_read \
    op interface \
    ports { dataIn_8_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 10 \
    name dataIn_9_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_9_V_read \
    op interface \
    ports { dataIn_9_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 11 \
    name dataIn_10_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_10_V_read \
    op interface \
    ports { dataIn_10_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 12 \
    name dataIn_11_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_11_V_read \
    op interface \
    ports { dataIn_11_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 13 \
    name dataIn_12_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_12_V_read \
    op interface \
    ports { dataIn_12_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 14 \
    name dataIn_13_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_13_V_read \
    op interface \
    ports { dataIn_13_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 15 \
    name dataIn_14_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_14_V_read \
    op interface \
    ports { dataIn_14_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 16 \
    name dataIn_15_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_15_V_read \
    op interface \
    ports { dataIn_15_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 17 \
    name dataIn_16_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_16_V_read \
    op interface \
    ports { dataIn_16_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 18 \
    name dataIn_17_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_17_V_read \
    op interface \
    ports { dataIn_17_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 19 \
    name dataIn_18_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_18_V_read \
    op interface \
    ports { dataIn_18_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 20 \
    name dataIn_19_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_19_V_read \
    op interface \
    ports { dataIn_19_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 21 \
    name dataIn_20_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_20_V_read \
    op interface \
    ports { dataIn_20_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 22 \
    name dataIn_21_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_21_V_read \
    op interface \
    ports { dataIn_21_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 23 \
    name dataIn_22_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_22_V_read \
    op interface \
    ports { dataIn_22_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 24 \
    name dataIn_23_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_23_V_read \
    op interface \
    ports { dataIn_23_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 25 \
    name dataIn_24_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_24_V_read \
    op interface \
    ports { dataIn_24_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 26 \
    name dataIn_25_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_25_V_read \
    op interface \
    ports { dataIn_25_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 27 \
    name dataIn_26_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_26_V_read \
    op interface \
    ports { dataIn_26_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 28 \
    name dataIn_27_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_27_V_read \
    op interface \
    ports { dataIn_27_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 29 \
    name dataIn_28_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_28_V_read \
    op interface \
    ports { dataIn_28_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 30 \
    name dataIn_29_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_29_V_read \
    op interface \
    ports { dataIn_29_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 31 \
    name dataIn_30_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_30_V_read \
    op interface \
    ports { dataIn_30_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 32 \
    name dataIn_31_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_31_V_read \
    op interface \
    ports { dataIn_31_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 33 \
    name dataIn_32_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_32_V_read \
    op interface \
    ports { dataIn_32_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 34 \
    name dataIn_33_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_33_V_read \
    op interface \
    ports { dataIn_33_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 35 \
    name dataIn_34_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_34_V_read \
    op interface \
    ports { dataIn_34_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 36 \
    name dataIn_35_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_35_V_read \
    op interface \
    ports { dataIn_35_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 37 \
    name dataIn_36_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_36_V_read \
    op interface \
    ports { dataIn_36_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 38 \
    name dataIn_37_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_37_V_read \
    op interface \
    ports { dataIn_37_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 39 \
    name dataIn_38_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_38_V_read \
    op interface \
    ports { dataIn_38_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 40 \
    name dataIn_39_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_39_V_read \
    op interface \
    ports { dataIn_39_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 41 \
    name dataIn_40_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_40_V_read \
    op interface \
    ports { dataIn_40_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 42 \
    name dataIn_41_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_41_V_read \
    op interface \
    ports { dataIn_41_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 43 \
    name dataIn_42_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_42_V_read \
    op interface \
    ports { dataIn_42_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 44 \
    name dataIn_43_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_43_V_read \
    op interface \
    ports { dataIn_43_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 45 \
    name dataIn_44_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_44_V_read \
    op interface \
    ports { dataIn_44_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 46 \
    name dataIn_45_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_45_V_read \
    op interface \
    ports { dataIn_45_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 47 \
    name dataIn_46_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_46_V_read \
    op interface \
    ports { dataIn_46_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 48 \
    name dataIn_47_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_47_V_read \
    op interface \
    ports { dataIn_47_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 49 \
    name dataIn_48_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_48_V_read \
    op interface \
    ports { dataIn_48_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 50 \
    name dataIn_49_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_49_V_read \
    op interface \
    ports { dataIn_49_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 51 \
    name dataIn_50_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_50_V_read \
    op interface \
    ports { dataIn_50_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 52 \
    name dataIn_51_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_51_V_read \
    op interface \
    ports { dataIn_51_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 53 \
    name dataIn_52_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_52_V_read \
    op interface \
    ports { dataIn_52_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 54 \
    name dataIn_53_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_53_V_read \
    op interface \
    ports { dataIn_53_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 55 \
    name dataIn_54_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_54_V_read \
    op interface \
    ports { dataIn_54_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 56 \
    name dataIn_55_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_55_V_read \
    op interface \
    ports { dataIn_55_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 57 \
    name dataIn_56_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_56_V_read \
    op interface \
    ports { dataIn_56_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 58 \
    name dataIn_57_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_57_V_read \
    op interface \
    ports { dataIn_57_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 59 \
    name dataIn_58_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_58_V_read \
    op interface \
    ports { dataIn_58_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 60 \
    name dataIn_59_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_59_V_read \
    op interface \
    ports { dataIn_59_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 61 \
    name dataIn_60_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_60_V_read \
    op interface \
    ports { dataIn_60_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 62 \
    name dataIn_61_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_61_V_read \
    op interface \
    ports { dataIn_61_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 63 \
    name dataIn_62_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_62_V_read \
    op interface \
    ports { dataIn_62_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 64 \
    name dataIn_63_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_63_V_read \
    op interface \
    ports { dataIn_63_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 65 \
    name dataIn_64_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_64_V_read \
    op interface \
    ports { dataIn_64_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 66 \
    name dataIn_65_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_65_V_read \
    op interface \
    ports { dataIn_65_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 67 \
    name dataIn_66_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_66_V_read \
    op interface \
    ports { dataIn_66_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 68 \
    name dataIn_67_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_67_V_read \
    op interface \
    ports { dataIn_67_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 69 \
    name dataIn_68_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_68_V_read \
    op interface \
    ports { dataIn_68_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 70 \
    name dataIn_69_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_69_V_read \
    op interface \
    ports { dataIn_69_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 71 \
    name dataIn_70_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_70_V_read \
    op interface \
    ports { dataIn_70_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 72 \
    name dataIn_71_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_71_V_read \
    op interface \
    ports { dataIn_71_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 73 \
    name dataIn_72_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_72_V_read \
    op interface \
    ports { dataIn_72_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 74 \
    name dataIn_73_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_73_V_read \
    op interface \
    ports { dataIn_73_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 75 \
    name dataIn_74_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_74_V_read \
    op interface \
    ports { dataIn_74_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 76 \
    name dataIn_75_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_75_V_read \
    op interface \
    ports { dataIn_75_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 77 \
    name dataIn_76_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_76_V_read \
    op interface \
    ports { dataIn_76_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 78 \
    name dataIn_77_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_77_V_read \
    op interface \
    ports { dataIn_77_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 79 \
    name dataIn_78_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_78_V_read \
    op interface \
    ports { dataIn_78_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 80 \
    name dataIn_79_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_79_V_read \
    op interface \
    ports { dataIn_79_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 81 \
    name dataIn_80_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_80_V_read \
    op interface \
    ports { dataIn_80_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 82 \
    name dataIn_81_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_81_V_read \
    op interface \
    ports { dataIn_81_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 83 \
    name dataIn_82_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_82_V_read \
    op interface \
    ports { dataIn_82_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 84 \
    name dataIn_83_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_83_V_read \
    op interface \
    ports { dataIn_83_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 85 \
    name dataIn_84_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_84_V_read \
    op interface \
    ports { dataIn_84_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 86 \
    name dataIn_85_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_85_V_read \
    op interface \
    ports { dataIn_85_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 87 \
    name dataIn_86_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_86_V_read \
    op interface \
    ports { dataIn_86_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 88 \
    name dataIn_87_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_87_V_read \
    op interface \
    ports { dataIn_87_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 89 \
    name dataIn_88_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_88_V_read \
    op interface \
    ports { dataIn_88_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 90 \
    name dataIn_89_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_89_V_read \
    op interface \
    ports { dataIn_89_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 91 \
    name dataIn_90_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_90_V_read \
    op interface \
    ports { dataIn_90_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 92 \
    name dataIn_91_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_91_V_read \
    op interface \
    ports { dataIn_91_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 93 \
    name dataIn_92_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_92_V_read \
    op interface \
    ports { dataIn_92_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 94 \
    name dataIn_93_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_93_V_read \
    op interface \
    ports { dataIn_93_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 95 \
    name dataIn_94_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_94_V_read \
    op interface \
    ports { dataIn_94_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 96 \
    name dataIn_95_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_95_V_read \
    op interface \
    ports { dataIn_95_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 97 \
    name dataIn_96_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_96_V_read \
    op interface \
    ports { dataIn_96_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 98 \
    name dataIn_97_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_97_V_read \
    op interface \
    ports { dataIn_97_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 99 \
    name dataIn_98_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_98_V_read \
    op interface \
    ports { dataIn_98_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 100 \
    name dataIn_99_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_99_V_read \
    op interface \
    ports { dataIn_99_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 101 \
    name dataIn_100_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_100_V_read \
    op interface \
    ports { dataIn_100_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 102 \
    name dataIn_101_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_101_V_read \
    op interface \
    ports { dataIn_101_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 103 \
    name dataIn_102_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_102_V_read \
    op interface \
    ports { dataIn_102_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 104 \
    name dataIn_103_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_103_V_read \
    op interface \
    ports { dataIn_103_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 105 \
    name dataIn_104_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_104_V_read \
    op interface \
    ports { dataIn_104_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 106 \
    name dataIn_105_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_105_V_read \
    op interface \
    ports { dataIn_105_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 107 \
    name dataIn_106_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_106_V_read \
    op interface \
    ports { dataIn_106_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 108 \
    name dataIn_107_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_107_V_read \
    op interface \
    ports { dataIn_107_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 109 \
    name dataIn_108_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_108_V_read \
    op interface \
    ports { dataIn_108_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 110 \
    name dataIn_109_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_109_V_read \
    op interface \
    ports { dataIn_109_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 111 \
    name dataIn_110_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_110_V_read \
    op interface \
    ports { dataIn_110_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 112 \
    name dataIn_111_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_111_V_read \
    op interface \
    ports { dataIn_111_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 113 \
    name dataIn_112_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_112_V_read \
    op interface \
    ports { dataIn_112_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 114 \
    name dataIn_113_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_113_V_read \
    op interface \
    ports { dataIn_113_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 115 \
    name dataIn_114_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_114_V_read \
    op interface \
    ports { dataIn_114_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 116 \
    name dataIn_115_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_115_V_read \
    op interface \
    ports { dataIn_115_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 117 \
    name dataIn_116_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_116_V_read \
    op interface \
    ports { dataIn_116_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 118 \
    name dataIn_117_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_117_V_read \
    op interface \
    ports { dataIn_117_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 119 \
    name dataIn_118_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_118_V_read \
    op interface \
    ports { dataIn_118_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 120 \
    name dataIn_119_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_119_V_read \
    op interface \
    ports { dataIn_119_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 121 \
    name dataIn_120_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_120_V_read \
    op interface \
    ports { dataIn_120_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 122 \
    name dataIn_121_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_121_V_read \
    op interface \
    ports { dataIn_121_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 123 \
    name dataIn_122_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_122_V_read \
    op interface \
    ports { dataIn_122_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 124 \
    name dataIn_123_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_123_V_read \
    op interface \
    ports { dataIn_123_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 125 \
    name dataIn_124_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_124_V_read \
    op interface \
    ports { dataIn_124_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 126 \
    name dataIn_125_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_125_V_read \
    op interface \
    ports { dataIn_125_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 127 \
    name dataIn_126_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_126_V_read \
    op interface \
    ports { dataIn_126_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 128 \
    name dataIn_127_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_127_V_read \
    op interface \
    ports { dataIn_127_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 129 \
    name dataIn_128_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_128_V_read \
    op interface \
    ports { dataIn_128_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 130 \
    name dataIn_129_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_129_V_read \
    op interface \
    ports { dataIn_129_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 131 \
    name dataIn_130_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_dataIn_130_V_read \
    op interface \
    ports { dataIn_130_V_read { I 26 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_return \
    type ap_return \
    reset_level 1 \
    sync_rst true \
    corename ap_return \
    op interface \
    ports { ap_return { O 1 vector } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_ce
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_ce] == "cg_default_interface_gen_ce"} {
eval "cg_default_interface_gen_ce { \
    id -4 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_ce \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


