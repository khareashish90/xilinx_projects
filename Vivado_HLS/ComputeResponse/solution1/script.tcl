############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
############################################################
open_project ComputeResponse
set_top ComputeResponse
add_files ComputeResponse/ComputeResopnse_Parameters.hpp
add_files ComputeResponse/ComputeResopnse.hpp
add_files ComputeResponse/ComputeResopnse.cpp
add_files -tb ComputeResponse/ComputeResopnse_TB.cpp
open_solution "solution1"
set_part {xc7z030sbg485-1} -tool vivado
create_clock -period 5 -name default
#source "./ComputeResponse/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog -vendor "Trimble"
