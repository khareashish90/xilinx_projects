#ifndef __COMPUTE_RESPONSE_HPP__
#define __COMPUTE_RESPONSE_HPP__

#include <hls_stream.h>
#include <ap_int.h>
#include <ap_fixed.h>
#include "ComputeResopnse_Parameters.hpp"

#define ATOMIC_MODE		1
#define FSTREAM_MODE	2

#define ABS(a)	((a>=0)?(a):(-a))

template <typename T>
T abs(T a);


/* Uncomment the following line to use the classical C type
 * The simulations are faster */
//#define USE_CTYPES

#ifndef USE_CTYPES
typedef ap_uint<29> Integral_image_pixel;
typedef ap_uint<26> Substracted_integral_image_pixels;
typedef ap_uint<12> Index_t;
typedef ap_uint<4> Size_id_t;
typedef ap_fixed<16,9,AP_RND_ZERO> Response_t;
typedef ap_fixed<18,0,AP_RND_ZERO> Inv_size_t;
typedef ap_int<12> Bram_Addr_t;
typedef ap_uint<7> Size_t;
typedef ap_uint<1> Bool_t;
#else
typedef int Integral_image_pixel;
typedef int Substracted_integral_image_pixels;
typedef int Index_t;
typedef int Size_id_t;
typedef float Response_t;
typedef float Inv_size_t;
typedef int Bram_Addr_t;
typedef int Size_t;
typedef int Bool_t;
#endif



typedef struct{
	Response_t resp;
	Index_t size;
}Response_And_Size;

/* Response output on 32 bits */
typedef struct{
	Response_t resp;
	ap_uint<4> null;
	Index_t size;
}Output_Data_t;

typedef struct{
	Output_Data_t	data;
    ap_uint<4> 	keep;
    ap_uint<4> 	strb;
    ap_uint<1>  user;
    ap_uint<1>  last;
    ap_uint<1>  id;
    ap_uint<1>  dest;
}AXI_Response_And_Size;

typedef hls::stream<AXI_Response_And_Size>	Output_stream;

typedef hls::stream<Integral_image_pixel>	Input_stream;

typedef Integral_image_pixel BRAM[WIDTH];



typedef struct{
	Bram_Addr_t size;
	Bool_t readEnable;
	Bool_t writeEnable;
}Line_values_t;


class Line{
public:
	BRAM line;
	Bram_Addr_t pLeft;
	Bram_Addr_t pRight, pStore;
	Bram_Addr_t dLoadL;
	Bram_Addr_t dLoadR, dLoadS;
	Bool_t readEnable, readEnableNext;
	Bool_t writeEnable, writeEnableNext;

	inline void new_pixel(void);
	inline void new_line(Line_values_t size);
	inline void process(Integral_image_pixel& dLeft, Integral_image_pixel& dRight,
			Integral_image_pixel dIn);

};

void ComputeResponse(Input_stream& iiInput, Output_stream& respAndSizeOutput);


#endif
