/******************************************************************************
*
* (c) Copyright 2010-2013 Xilinx, Inc. All rights reserved.
*
* This file contains confidential and proprietary information of Xilinx, Inc.
* and is protected under U.S. and international copyright and other
* intellectual property laws.
*
* DISCLAIMER
* This disclaimer is not a license and does not grant any rights to the
* materials distributed herewith. Except as otherwise provided in a valid
* license issued to you by Xilinx, and to the maximum extent permitted by
* applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL
* FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS,
* IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
* MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE;
* and (2) Xilinx shall not be liable (whether in contract or tort, including
* negligence, or under any other theory of liability) for any loss or damage
* of any kind or nature related to, arising under or in connection with these
* materials, including for any direct, or any indirect, special, incidental,
* or consequential loss or damage (including loss of data, profits, goodwill,
* or any type of loss or damage suffered as a result of any action brought by
* a third party) even if such damage or loss was reasonably foreseeable or
* Xilinx had been advised of the possibility of the same.
*
* CRITICAL APPLICATIONS
* Xilinx products are not designed or intended to be fail-safe, or for use in
* any application requiring fail-safe performance, such as life-support or
* safety devices or systems, Class III medical devices, nuclear facilities,
* applications related to the deployment of airbags, or any other applications
* that could lead to death, personal injury, or severe property or
* environmental damage (individually and collectively, "Critical
* Applications"). Customer assumes the sole risk and liability of any use of
* Xilinx products in Critical Applications, subject only to applicable laws
* and regulations governing limitations on product liability.
*
* THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE
* AT ALL TIMES.
*
******************************************************************************/
/*****************************************************************************/
/**
 *
 * @file xaxidma_example_simple_poll.c
 *
 * This file demonstrates how to use the xaxidma driver on the Xilinx AXI
 * DMA core (AXIDMA) to transfer packets in polling mode when the AXI DMA core
 * is configured in simple mode.
 *
 * This code assumes a loopback hardware widget is connected to the AXI DMA
 * core for data packet loopback.
 *
 * To see the debug print, you need a Uart16550 or uartlite in your system,
 * and please set "-DDEBUG" in your compiler options. You need to rebuild your
 * software executable.
 *
 * Make sure that MEMORY_BASE is defined properly as per the HW system. The
 * h/w system built in Area mode has a maximum DDR memory limit of 64MB. In
 * throughput mode, it is 512MB.  These limits are need to ensured for
 * proper operation of this code.
 *
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 4.00a rkv  02/22/11 New example created for simple DMA, this example is for
 *       	       simple DMA
 * 5.00a srt  03/06/12 Added Flushing and Invalidation of Caches to fix CRs
 *		       648103, 648701.
 *		       Added V7 DDR Base Address to fix CR 649405.
 * 6.00a srt  03/27/12 Changed API calls to support MCDMA driver.
 * 7.00a srt  06/18/12 API calls are reverted back for backward compatibility.
 * 7.01a srt  11/02/12 Buffer sizes (Tx and Rx) are modified to meet maximum
 *		       DDR memory limit of the h/w system built with Area mode
 * 7.02a srt  03/01/13 Updated DDR base address for IPI designs (CR 703656).
 *
 * </pre>
 *
 * ***************************************************************************

 */
/***************************** Include Files *********************************/
#include <xaxidma.h>
#include <xintegral_image_kernel.h>

#include "xparameters.h"
#include "xdebug.h"
#include "xtime_l.h"
#include"TestImage.h"
#include "stdio.h"

#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif

/******************** Constant Definitions **********************************/

/*
 * Device hardware build related constants.
 */

#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID

#ifdef XPAR_V6DDR_0_S_AXI_BASEADDR
#define DDR_BASE_ADDR		XPAR_V6DDR_0_S_AXI_BASEADDR
#elif XPAR_S6DDR_0_S0_AXI_BASEADDR
#define DDR_BASE_ADDR		XPAR_S6DDR_0_S0_AXI_BASEADDR
#elif XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#define DDR_BASE_ADDR		XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#elif XPAR_MIG7SERIES_0_BASEADDR
#define DDR_BASE_ADDR		XPAR_MIG7SERIES_0_BASEADDR
#else
#define DDR_BASE_ADDR		XPAR_PS7_DDR_0_S_AXI_BASEADDR
#endif

#ifndef DDR_BASE_ADDR
#warning CHECK FOR THE VALID DDR ADDRESS IN XPARAMETERS.H, \
		 DEFAULT SET TO 0x01000000
#define MEM_BASE_ADDR		0x01000000
#else
#define MEM_BASE_ADDR		(DDR_BASE_ADDR + 0x1000000)
#endif

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00400000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00800000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x00BFFFFF)
#define REF_BUFFER			(MEM_BASE_ADDR + 0x01000000)

#define WIDTH				1920
#define HEIGHT				1080
#define MAX_PKT_LEN			WIDTH*HEIGHT
//#define MAX_PKT_LEN			16

#define NUMBER_OF_TRANSFERS	1

/**************************** Type Definitions *******************************/


/***************** Macros (Inline Functions) Definitions *********************/


/************************** Function Prototypes ******************************/

#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

int check_data(void);
void compute_ref(void);

/************************** Variable Definitions *****************************/
/*
 * Device instance definitions
 */
XAxiDma AxiDma;
XIntegral_image_kernel IIInst;

void disp_proc(void){
	u32* p = RX_BUFFER_BASE;
	for(int i=0; i<5*4*2; i++){
		xil_printf("Data %d\t Read: %d\n\r", i, *p);
		p++;
	}
}

void clear_proc(void){
	memset(RX_BUFFER_BASE, 0, MAX_PKT_LEN*NUMBER_OF_TRANSFERS*sizeof(u32));
}

void write_proc(XAxiDma *InstancePtr){
	XAxiDma_WriteReg(InstancePtr->TxBdRing.ChanBase,
					 XAXIDMA_SRCADDR_OFFSET, TX_BUFFER_BASE);

	XAxiDma_WriteReg(InstancePtr->TxBdRing.ChanBase,
			XAXIDMA_CR_OFFSET,
			XAxiDma_ReadReg(
			InstancePtr->TxBdRing.ChanBase,
			XAXIDMA_CR_OFFSET)| XAXIDMA_CR_RUNSTOP_MASK);

	XAxiDma_WriteReg(InstancePtr->TxBdRing.ChanBase,
				XAXIDMA_BUFFLEN_OFFSET, MAX_PKT_LEN*NUMBER_OF_TRANSFERS*sizeof(u8));

}



void read_proc(XAxiDma *InstancePtr, int id){
	XAxiDma_WriteReg(InstancePtr->RxBdRing[0].ChanBase,
					 XAXIDMA_DESTADDR_OFFSET, RX_BUFFER_BASE + id*MAX_PKT_LEN*sizeof(u32));

	XAxiDma_WriteReg(InstancePtr->RxBdRing[0].ChanBase,
					XAXIDMA_CR_OFFSET,
				XAxiDma_ReadReg(InstancePtr->RxBdRing[0].ChanBase,
				XAXIDMA_CR_OFFSET)| XAXIDMA_CR_RUNSTOP_MASK);

	XAxiDma_WriteReg(InstancePtr->RxBdRing[0].ChanBase,
				XAXIDMA_BUFFLEN_OFFSET, MAX_PKT_LEN*sizeof(u32));

	//Xil_DCacheInvalidateRange(RX_BUFFER_BASE + id*MAX_PKT_LEN*sizeof(u32), MAX_PKT_LEN*sizeof(u32));
}

int is_dma_read_ready(XAxiDma *InstancePtr){
	return ((XAxiDma_ReadReg(InstancePtr->RxBdRing[0].ChanBase, XAXIDMA_SR_OFFSET) &
					XAXIDMA_IDLE_MASK) ? TRUE : FALSE);
}



int init_DMA()
{

	//DMA INIT
	XAxiDma_Config *CfgPtr;
	CfgPtr = XAxiDma_LookupConfig(0);
	XAxiDma_CfgInitialize(&AxiDma, CfgPtr);

	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS ;
}

int init_IntegralImage()
{
	int status ;
	//IP Init
	XIntegral_image_kernel_Config* IIConfig = XIntegral_image_kernel_LookupConfig(0);
	if(!IIConfig)
	{
		printf("Error loading config for Integral Image Kernel\n");
	}

	status = XIntegral_image_kernel_CfgInitialize(&IIInst, IIConfig);

	if(status != XST_SUCCESS)
	{
		printf("Error initializing Integral Image Kernel\n");
	}
	XIntegral_image_kernel_EnableAutoRestart(&IIInst);
	return status ;

}


int main()
{
	Xil_DCacheDisable();

	XTime xtimeStart;
	XTime xtimeDone;


	init_DMA();
	init_IntegralImage();


	XIntegral_image_kernel_Start(&IIInst);

	u8 *SrcImg_Buffer = TX_BUFFER_BASE;

	for(int i = 0 ; i< MAX_PKT_LEN; i++)
	{
		SrcImg_Buffer[i] = im[i];
	}

	compute_ref();
	clear_proc(); //set 0 to the read space.


	XTime_GetTime(&xtimeStart);

	write_proc(&AxiDma); //Init data transfert DDR to IP
	for(int i = 0; i < NUMBER_OF_TRANSFERS; i++){
		read_proc(&AxiDma, i);
		while(!is_dma_read_ready(&AxiDma));
	}

	XTime_GetTime(&xtimeDone);



	int nerr = check_data();
	xil_printf("Number of errors: %d\r\n", nerr);


	//ARM frequency: XPAR_CPU_CORTEXA9_CORE_CLOCK_FREQ_HZ
	xil_printf("Start: %d\r\nDone:  %d\n\rARM Ticks %d\n\r", (int)xtimeStart, (int)xtimeDone, (int)(xtimeDone - xtimeStart));
	xil_printf("Frequency: %d Hz\n\rTime (2 * ARM ticks / frequency)\n\r", XPAR_CPU_CORTEXA9_CORE_CLOCK_FREQ_HZ);

	xil_printf("--- Exiting main() ---\r\n");

	return 0;//nerr;
}


int check_data(void)
{
	int nerr = 0;

	u32* ref = REF_BUFFER;
	u32* RxPacket = RX_BUFFER_BASE;

	/* Invalidate the DestBuffer before receiving the data, in case the
	 * Data Cache is enabled
	 */
	//Xil_DCacheInvalidateRange((u32)RxPacket, MAX_PKT_LEN*NUMBER_OF_TRANSFERS);


	for (int i = 0; i < MAX_PKT_LEN*NUMBER_OF_TRANSFERS; ++i){
		if (RxPacket[i] != ref[i]){
			nerr++;
		}
		//xil_printf("Data %d\tRead %d \tExpected %d\n\r", i, RxPacket[i], ref[i]);
	}

	return nerr;
}

// Computes the integral image in software
void compute_ref(void){
	u8* refSrc = TX_BUFFER_BASE;
	u32* ref = REF_BUFFER;

	for(int l=0; l<NUMBER_OF_TRANSFERS; l++){
		ref = REF_BUFFER + WIDTH*HEIGHT*sizeof(u32)*l;
		refSrc = TX_BUFFER_BASE + WIDTH*HEIGHT*sizeof(u8)*l;
		for(int i=0; i<HEIGHT; i++){
			for(int j=0; j<WIDTH; j++){
				int k = WIDTH*i+j;
				//xil_printf("Id %d:\tSrc %d\n\r", k, (u8)refSrc[k]);
				ref[k] = refSrc[k];
				if(i!=0)
					ref[k] += ref[k-WIDTH];
				if(j!=0)
					ref[k] += ref[k-1];
				if((i!=0)&&(j!=0))
					ref[k] -= ref[k-WIDTH-1];
				//xil_printf("\tRef %d\n\r", (u32)ref[k]);
			}
		}
	}


}
